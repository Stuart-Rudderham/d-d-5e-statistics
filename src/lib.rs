#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]

#[macro_use]
pub mod flat_map_lit;

pub mod distribution;
pub mod dice;
pub mod attack;
pub mod attack_roller;
pub mod attack_sequence;
pub mod check_against_dc;
pub mod saving_throw;
pub mod group_check;
pub mod spell;
pub mod typedefs;
pub mod parsing;

#[cfg(test)]
pub mod unit_test_utils;
