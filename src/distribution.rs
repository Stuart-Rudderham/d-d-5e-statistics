use crate::typedefs::MeanValue;

use itertools::Itertools;
use more_asserts::*;
use num_integer::Integer;
use num_integer;
use num_traits::cast::ToPrimitive;
use num_traits::identities::One;
use num_traits::identities::Zero;
use num_traits;
use std::ops::Add;

pub type Count = num_bigint::BigUint;

//
// NOTE: This could go even lower and be an i16, but when that was benchmarked
// a couple of tests were much slower (10x) for some unknown reason.
//
// Possibly the smaller size is having some strange effect on the cache?
// https://www.youtube.com/watch?v=r-TLSBdHe1A&feature=youtu.be&t=598
//
pub type Value = i32;

pub type Probability = f64;

// Use FlatMap rather than BTreeMap for the following benefits:
//     - better cache usage
//     - faster iteration speed
//     - the ability to reserve memory upfront in one allocation rather than a bunch of smaller allocations
pub type       CountDistribution = flat_map::FlatMap<Value, Count      >;
pub type ProbabilityDistribution = flat_map::FlatMap<Value, Probability>;

#[must_use]
pub fn CountRatioToProbability( aNumerator : &Count, aDenominator : &Count ) -> Probability
{
    assert!( !aDenominator.is_zero() );

    // TODO: Could try reducing numbers by GCD first to help improve precision.
    aNumerator.to_f64().unwrap() / aDenominator.to_f64().unwrap()
}

#[must_use]
pub fn IsCountValid( aCount : &Count ) -> bool
{
    // Counts must be positive, otherwise there's no point in having it in the distribution.
    !aCount.is_zero()
}

#[must_use]
pub fn IsProbabilityValid( aProbability : &Probability ) -> bool
{
    (*aProbability >= 0.0) && (*aProbability <= 1.0)
}

pub fn AssertDistributionValid<T, F>( aDistribution : &flat_map::FlatMap<Value, T>, aValidationFn : F ) where F : Fn(&T)
{
    assert!( !aDistribution.is_empty() );

    for ( (v1, t1), (v2, t2) ) in aDistribution.iter().tuple_windows()
    {
        aValidationFn(t1);
        aValidationFn(t2);

        // The distribution must be sorted from low to high
        // and have no duplicate entries.
        assert_lt!(v1, v2);
    }
}

pub fn AssertCountDistributionValid( aCountDistribution : &CountDistribution )
{
    AssertDistributionValid(aCountDistribution, |count|
    {
        assert!( IsCountValid(count) );
    });
}

pub fn AssertProbabilityDistributionValid( aProbabilityDistribution : &ProbabilityDistribution )
{
    AssertDistributionValid(aProbabilityDistribution, |probability|
    {
        assert!( IsProbabilityValid(probability) );
    });
}

#[must_use]
pub fn CalculateNumPermutations( aCountDistribution : &CountDistribution ) -> Count
{
    aCountDistribution.values().sum()
}

#[must_use]
pub fn GetMin<K, V>( aDistribution : &flat_map::FlatMap<K, V> ) -> (&K, &V)
{
    aDistribution.iter().next().expect("Cannot find min value for empty distribution!")
}

#[must_use]
pub fn GetMax<K, V>( aDistribution : &flat_map::FlatMap<K, V> ) -> (&K, &V)
{
    aDistribution.iter().next_back().expect("Cannot find max value for empty distribution!")
}

#[must_use]
pub fn BuildProbabilityDistributionHelper( aCountDistribution : &CountDistribution, aNumPermutations : &Count ) -> ProbabilityDistribution
{
    aCountDistribution.iter().map(|(value, count)|
    {
        let probability = CountRatioToProbability(count, aNumPermutations);

        assert!( IsProbabilityValid(&probability) );

        (*value, probability)
    })
    .collect()
}

#[must_use]
pub fn BuildProbabilityDistribution( aCountDistribution : &CountDistribution ) -> ProbabilityDistribution
{
    let numPermutations = CalculateNumPermutations( aCountDistribution );

    BuildProbabilityDistributionHelper(aCountDistribution, &numPermutations)
}

// https://en.wikipedia.org/wiki/Cumulative_distribution_function
#[must_use]
pub fn BuildCumulativeDistribution( aCountDistribution : &CountDistribution ) -> CountDistribution
{
    let mut ret = CountDistribution::with_capacity( aCountDistribution.len() );

    let mut runningCount = Count::zero();

    for (value, count) in aCountDistribution
    {
        runningCount += count;

        ret.insert( *value, runningCount.clone() );
    }

    ret
}

// The (almost) inverse of the cumulative distribution function.
// Gives the number of ways for the distribution to be >= the given value.
// Start at NumPermutations() and goes down to (but doesn't actually hit) 0.
//
// The "standard" survival distribution gives the probability of > a given value
// but that is not as useful for the types of graphs and queries we want to make.
//
// https://en.wikipedia.org/wiki/Survival_function
#[must_use]
pub fn BuildSurvivalDistribution( aCountDistribution : &CountDistribution ) -> CountDistribution
{
    let mut ret = CountDistribution::with_capacity( aCountDistribution.len() );

    let mut numPermutations = CalculateNumPermutations( aCountDistribution );

    for (value, count) in aCountDistribution
    {
        ret.insert( *value, numPermutations.clone() );

        numPermutations -= count;
    }

    ret
}

pub fn ReduceDistribution( aCountDistribution : &mut CountDistribution )
{
    assert!( !aCountDistribution.is_empty(), "Cannot reduce an empty CountDistribution!" );

    let TryFoldFn = |acc : Count, x : &Count| -> Option<Count>
    {
        let gcd = x.gcd(&acc);

        // We're trying to reduce the counts to the lowest denominator possible.
        // If GCD == 1 then some of the counts are coprime and therefore we can't
        // reduce anything. In which case we want to break the iteration because
        // there's no point in checking any of the other numbers, the GCD will
        // never increase.
        if gcd > Count::one()
        {
            Some(gcd)
        }
        else
        {
            None
        }
    };

    // Fold from the back because a lot of the time the highest value is also the
    // least common one (e.g. rolling a crit and then rolling for max damage) and
    // actually has a Count of 1 (which means that the GCD will be 1), so we
    // increase the odds of breaking early.
    if let Some(gcd) = aCountDistribution.values().try_rfold(Count::zero(), TryFoldFn)
    {
        for count in aCountDistribution.values_mut()
        {
            assert!(count.is_multiple_of(&gcd));

            *count /= &gcd;
        }
    }
}

#[must_use]
pub fn ClampDistribution( aCountDistribution : &CountDistribution, aNewMin : Value, aNewMax : Value) -> CountDistribution
{
    assert!(aNewMin <= aNewMax, "The clamp min {} must be <= than the clamp max {}!", aNewMin, aNewMax);

    // In most cases an over-estimate for the capacity
    // but we want to make sure we don't need to grow.
    let mut clampedDistribution = CountDistribution::with_capacity( aCountDistribution.len() );

    for (value, count) in aCountDistribution.iter()
    {
        let clampedValue = num_traits::clamp(*value, aNewMin, aNewMax);

        let entry = clampedDistribution.entry(clampedValue).or_insert_with(Count::zero);
        *entry += count;
    }

    clampedDistribution
}

#[must_use]
pub fn ClipDistribution( aCountDistribution : &CountDistribution, aNewMin : Value, aNewMax : Value) -> Option<CountDistribution>
{
    // In most cases an over-estimate for the capacity
    // but we want to make sure we don't need to grow.
    let mut clippedDistribution = CountDistribution::with_capacity( aCountDistribution.len() );

    for (value, count) in aCountDistribution
    {
        if *value < aNewMin || *value > aNewMax
        {
            // Out of range, drop the value.
            continue;
        }

        let entry = clippedDistribution.entry(*value).or_insert_with(Count::zero);
        *entry += count;
    }

    if clippedDistribution.is_empty()
    {
        // We clipped away all the values, so there is no more distribution.
        None
    }
    else
    {
        Some( clippedDistribution )
    }
}

pub fn NegateDistribution<V : Clone>( aDistribution : &flat_map::FlatMap<Value, V> ) -> flat_map::FlatMap<Value, V>
{
    let mut newDistribution = flat_map::FlatMap::with_capacity( aDistribution.len() );

    for (value, x) in aDistribution.iter()
    {
        newDistribution.insert(-value, x.clone());
    }

    newDistribution
}

#[must_use]
pub fn MergeCountDistributions<F>(aLHS : &CountDistribution, aRHS : &CountDistribution, aMergeFn : F ) -> CountDistribution
    where F : Fn(Value, Value) -> Value
{
    // It is difficult pick a capacity that is best for all situations. In the worst case
    // every call to "aMergeFn" will result in a unique value, so the upper bound on the
    // size is "aLHS.len() * aRHS.len()". However, for a variety of common use cases such
    // as addition or minimum/maximum this can be a very large overestimation.
    //
    // TODO: Have the caller pass in the capacity since they know what MergeFn they are using?
    let mut newDistribution = CountDistribution::with_capacity( aLHS.len() + aRHS.len() );

    for (leftValue, leftCount) in aLHS.iter()
    {
        for (rightValue, rightCount) in aRHS.iter()
        {
            let mergedValue = aMergeFn(*leftValue, *rightValue);
            let newCount = leftCount * rightCount;

            let entry = newDistribution.entry(mergedValue).or_insert_with(Count::zero);
            *entry += newCount;
        }
    }

    newDistribution
}

// Mul.
#[must_use]
pub fn MergeCountDistributions_Mul(aCountDistribution : &CountDistribution, aMultiplier : usize) -> CountDistribution
{
    // Multiplication is just repeated addition, in the same way that
    // exponentiation is repeated multiplication. Algorithm reference
    // https://en.wikipedia.org/wiki/Exponentiation_by_squaring#Basic_method

    fn Add( a : &CountDistribution, b : &CountDistribution ) -> CountDistribution
    {
        MergeCountDistributions(a, b, Value::add)
    }

    fn Double( a : &CountDistribution ) -> CountDistribution
    {
        Add(a, a)
    }

    fn Helper( x : CountDistribution, n : usize ) -> CountDistribution
    {
        assert!(n > 0);

        if n == 1
        {
            x
        }
        else if n % 2 == 0
        {
            Helper( Double(&x), n / 2 )
        }
        else
        {
            Add( &x, &Helper(Double(&x), (n - 1) / 2) )
        }
    }

    Helper( aCountDistribution.clone(), aMultiplier )
}

#[must_use]
pub fn CalculateMean(aDistribution : &CountDistribution) -> MeanValue
{
    let mut sum         : num_bigint::BigInt = Zero::zero();
    let mut weightedSum : num_bigint::BigInt = Zero::zero();

    for (value, count) in aDistribution.iter()
    {
        let signedCount = num_bigint::BigInt::from_biguint( num_bigint::Sign::Plus, count.clone() );

        weightedSum += num_bigint::BigInt::from(*value) * &signedCount;

        sum += signedCount;
    }

    let gcd = sum.gcd(&weightedSum);

    let reducedSum         = sum         / &gcd;
    let reducedWeightedSum = weightedSum / &gcd;

    // Do the conversion to float as late as possible to help accuracy.
    MeanValue(reducedWeightedSum.to_f64().unwrap() / reducedSum.to_f64().unwrap())
}

#[cfg(test)]
mod tests
{
    use super::*;
    use itertools::zip;
    use itertools::izip;
    use num_traits::Pow;
    use proptest::prelude::*;
    use rand::distributions::Distribution;
    use rand::seq::IteratorRandom;

    fn TestArbitraryCountDistribution( aCountDistribution : &CountDistribution )
    {
        // Make sure the generated distribution is valid before checking anything else.
        AssertCountDistributionValid( aCountDistribution );

        let aCountDistributionNumPermutations = CalculateNumPermutations( aCountDistribution );

        // NumPermutations.
        assert_gt!(aCountDistributionNumPermutations, Count::zero());
        for (_, count) in aCountDistribution
        {
            assert_ge!(aCountDistributionNumPermutations, count);
        }

        let aCountDistributionMinValue = *GetMin(aCountDistribution).0;
        let aCountDistributionMaxValue = *GetMax(aCountDistribution).0;

        assert_ge!(aCountDistributionMaxValue, aCountDistributionMinValue);

        // The mean value should be within a "reasonable" range.
        let meanValue = CalculateMean( aCountDistribution );
        {
            assert_ge!( *meanValue, f64::from( aCountDistributionMinValue ) );
            assert_le!( *meanValue, f64::from( aCountDistributionMaxValue ) );
        }

        // Repeated addition should be the same as multiplication.
        for i in 1..10
        {
            let mult = MergeCountDistributions_Mul( aCountDistribution, i );

            let mut add = aCountDistribution.clone();
            for _ in 1..i
            {
                add = MergeCountDistributions( &add, aCountDistribution, Value::add );
            }

            AssertCountDistributionValid( &mult );
            AssertCountDistributionValid( &add  );

            assert_eq!(mult, add);
        }

        // Probability distribution.
        let aProbabilityDistribution = BuildProbabilityDistribution( aCountDistribution );
        {
            // Changing to a probability distribution should keep all
            // the same keys and only change the value in the map.
            assert_eq!(aCountDistribution.len(), aProbabilityDistribution.len());

            AssertProbabilityDistributionValid(&aProbabilityDistribution);

            for ((pValue, _), (cValue, _)) in zip(&aProbabilityDistribution, aCountDistribution)
            {
                // Keys are unchanged.
                assert_eq!(pValue, cValue);
            }
        }

        // Reducing.
        {
            let mut aReducedCountDistribution = aCountDistribution.clone();
            ReduceDistribution( &mut aReducedCountDistribution );

            AssertCountDistributionValid( &aReducedCountDistribution );

            assert_eq!(aCountDistribution.len(), aReducedCountDistribution.len());

            // Reducing should be a "final" operation.
            {
                let mut aDoubleReducedCountDistribution = aReducedCountDistribution.clone();
                ReduceDistribution( &mut aDoubleReducedCountDistribution );

                assert_eq!(aReducedCountDistribution, aDoubleReducedCountDistribution);
            }

            // Reducing shouldn't change the mean.
            assert_eq!( meanValue, CalculateMean(&aReducedCountDistribution) );

            // Reducing shouldn't change the final probability for any value.
            {
                let aReducedProbabilityDistribution = BuildProbabilityDistribution( &aReducedCountDistribution );

                AssertProbabilityDistributionValid(&aReducedProbabilityDistribution);

                for ((oValue, oProbability), (rValue, rProbability)) in zip(&aProbabilityDistribution, &aReducedProbabilityDistribution)
                {
                    // Keys are unchanged.
                    assert_eq!(oValue, rValue);

                    // Probabilities are unchanged.
                    assert_eq!(oProbability, rProbability);
                }
            }
        }

        // Negating.
        {
            let       aNegatedCountDistribution = NegateDistribution(         aCountDistribution );
            let aDoubleNegatedCountDistribution = NegateDistribution( &aNegatedCountDistribution );

            AssertCountDistributionValid( &      aNegatedCountDistribution );
            AssertCountDistributionValid( &aDoubleNegatedCountDistribution );

            let       aNegatedProbabilityDistribution = NegateDistribution( &       aProbabilityDistribution );
            let aDoubleNegatedProbabilityDistribution = NegateDistribution( &aNegatedProbabilityDistribution );

            AssertProbabilityDistributionValid(&      aNegatedProbabilityDistribution);
            AssertProbabilityDistributionValid(&aDoubleNegatedProbabilityDistribution);

            // Length should be unchanged.
            assert_eq!(      aCountDistribution.len(), aNegatedCountDistribution      .len());
            assert_eq!(aProbabilityDistribution.len(), aNegatedProbabilityDistribution.len());

            // Double negation should be the identity.
            assert_eq!(     *aCountDistribution, aDoubleNegatedCountDistribution      );
            assert_eq!(aProbabilityDistribution, aDoubleNegatedProbabilityDistribution);

            // Since the distribution is sorted, when we negate it we also end
            // up reversing the order, hence the ".rev()" when comparing.
            for ((oValue, oCount), (nValue, nCount)) in zip(aCountDistribution.iter().rev(), &aNegatedCountDistribution)
            {
                // Keys are negated.
                assert_eq!(*oValue, -nValue);

                // Counts are unchanged.
                assert_eq!(oCount, nCount);
            }
        }

        // Clamping and Clipping.
        {
            // We haven't actually changed anything here.
            let anUnchangedClampedDistribution = ClampDistribution( aCountDistribution, aCountDistributionMinValue, aCountDistributionMaxValue );
            let anUnchangedClippedDistribution =  ClipDistribution( aCountDistribution, aCountDistributionMinValue, aCountDistributionMaxValue ).unwrap();

            assert_eq!( anUnchangedClampedDistribution, *aCountDistribution );
            assert_eq!( anUnchangedClippedDistribution, *aCountDistribution );

            if aCountDistribution.len() >= 3
            {
                assert_ne!(aCountDistributionMinValue, aCountDistributionMaxValue);

                // Since we have at least 3 distinct values we're sure that
                // the new min/max don't overlap. They may be equal though.
                let newMinValue = aCountDistributionMinValue + 1;
                let newMaxValue = aCountDistributionMaxValue - 1;

                let aClampedDistribution = ClampDistribution( aCountDistribution, newMinValue, newMaxValue );
                let aClippedDistribution =  ClipDistribution( aCountDistribution, newMinValue, newMaxValue ).unwrap();

                AssertCountDistributionValid( &aClampedDistribution );
                AssertCountDistributionValid( &aClippedDistribution );

                // The values the distribution can have should be changed.
                assert_eq!( newMinValue, *GetMin(&aClampedDistribution).0 );
                assert_eq!( newMaxValue, *GetMax(&aClampedDistribution).0 );

                assert_le!( newMinValue, *GetMin(&aClippedDistribution).0 );
                assert_ge!( newMaxValue, *GetMax(&aClippedDistribution).0 );

                // We *should not* lose any "sides" of the dice by clamping the range it can roll.
                assert_eq!( aCountDistributionNumPermutations, CalculateNumPermutations(&aClampedDistribution) );

                // But we *should* lose "sides" of the dice by clipping the range it can roll.
                assert_gt!( aCountDistributionNumPermutations, CalculateNumPermutations(&aClippedDistribution) );
            }
        }

        // Accumulation distributions.
        {
            let   aSurvivalDistribution =   BuildSurvivalDistribution( aCountDistribution );
            let aCumulativeDistribution = BuildCumulativeDistribution( aCountDistribution );

            AssertCountDistributionValid( &  aSurvivalDistribution );
            AssertCountDistributionValid( &aCumulativeDistribution );

            assert_eq!(aCountDistribution.len(),   aSurvivalDistribution.len());
            assert_eq!(aCountDistribution.len(), aCumulativeDistribution.len());

            for ((value, count), (surValue, surCount), (cumuValue, cumuCount)) in izip!(aCountDistribution, &aSurvivalDistribution, &aCumulativeDistribution)
            {
                // Keys are unchanged.
                assert_eq!(value,  surValue);
                assert_eq!(value, cumuValue);

                //
                // The cumulative and survival distributions are complementary and together
                // they should sum to 1.
                //
                // Because our definition of the survival distribution is slightly non-standard
                // (we check >= rather than >) we end up double counting the current entry when
                // summing them together so we need to undo that with the "- count").
                //
                assert_eq!(aCountDistributionNumPermutations, surCount + cumuCount - count);
            }

            // Make sure the ends are as expected.
            assert_eq!( &aCountDistributionNumPermutations, GetMin(&  aSurvivalDistribution).1 );
            assert_eq!( &aCountDistributionNumPermutations, GetMax(&aCumulativeDistribution).1 );

            // Strictly monotonically decreasing.
            for ( (_, c1), (_, c2) ) in aSurvivalDistribution.iter().tuple_windows()
            {
                assert_gt!(c1, c2);
            }

            // Strictly monotonically increasing.
            for ( (_, c1), (_, c2) ) in aCumulativeDistribution.iter().tuple_windows()
            {
                assert_lt!(c1, c2);
            }
        }
    }

    //
    // TODO: There is a low probability of generating a random CountDistribution that has all contiguous values.
    //       This is a good test case for off-by-one errors so it would be nice to make it more common.
    //
    // This could be implemented by changing the seed values we use. If the seeds were rather:
    //
    //     1) A shuffled vector of all the possible distribution values (generated using prop_shuffle)
    //     2) A random distribution length
    //     3) A random starting index
    //     4) A max bit length for the distribution counts
    //
    // We would use the random starting index + length to extract a subslice from the shuffled distribution
    // values rather than calling choose_multiple().
    //
    // When the array is fully random this would be equivalent to pulling random values, and as the array
    // becomes more sorted we'd pull more contiguous values. The random starting index would also ensure
    // that we'd get a good sample of values (i.e. all negative, all positive, negative + positive + zero).
    //
    fn GenerateRandomCountDistributions() -> impl proptest::strategy::Strategy<Value = CountDistribution>
    {
        let distributionLengthRange             = 1..=61usize;
        let distributionCountsMaxBitLengthRange = 0..=32u32;

        // These are the params that Proptest will tweak while generating
        // test cases as well as trying to shrink a failing test case.
        let strategySeeds = (distributionLengthRange, distributionCountsMaxBitLengthRange);

        strategySeeds.prop_perturb(|(distributionLength, distributionCountsMaxBitLength), mut rng|
        {
            println!("distributionLength{}, distributionCountsMaxBitLength{}", distributionLength, distributionCountsMaxBitLength);

            // NOTE: The length of this range should be equal to distributionLengthRange.end so that we get
            //       the full range of contiguous values when trying to generate a max length distribution.
            let distributionValuesRange = -30i32..=30i32;

            // Do a random selection of *unique* values.
            let distributionValues = distributionValuesRange.choose_multiple(&mut rng, distributionLength);

            // Make sure we were actually able to get as many values as we wanted.
            // If this triggers that means that distributionValuesRange.len() < distributionLengthRange.end
            assert_eq!( distributionValues.len(), distributionLength );

            // Performance optimization, since we'll be generating a bunch of random Counts.
            // Needs to be an inclusive range to handle a max bit length of 0.
            let distributionCountsRng = rand::distributions::Uniform::new_inclusive( Count::one(), Count::from(2u32).pow(distributionCountsMaxBitLength) );

            distributionValues.iter().map( |value| (*value, distributionCountsRng.sample(&mut rng)) ).collect()
        })
    }

    // Test a bunch of randomly generated distributions.
    proptest!
    {
        #![proptest_config(ProptestConfig{ cases: 1000, verbose: 2, .. ProptestConfig::default() })]
        #[test]
        fn ProptestRandom( countDistribution in GenerateRandomCountDistributions() )
        {
            // With deoptimized code this takes >5 minutes to run.
            if crate::unit_test_utils::ShouldRunSlowUnitTests()
            {
                TestArbitraryCountDistribution( &countDistribution );
            }
        }
    }

    #[test]
    fn Hardcoded()
    {
        // Just some very basic tests with hardcoded numbers as a sanity check.
        let aCountDistribution = flatmap!
        {
                20 => Count::from(10_000u32),
               200 => Count::from( 1_000u32),
             2_000 => Count::from(   100u32),
            20_000 => Count::from(    10u32),
        };

        TestArbitraryCountDistribution( &aCountDistribution );

        // Misc.
        {
            assert_eq!( CalculateNumPermutations(&aCountDistribution), Count::from(11_110u32) );

            assert_eq!( GetMin(&aCountDistribution), (&    20, &Count::from(10_000u32)) );
            assert_eq!( GetMax(&aCountDistribution), (&20_000, &Count::from(    10u32)) );

            assert_eq!( CalculateMean(&aCountDistribution), MeanValue(800_000.0 / 11_110.0) );
        }

        // Probability.
        {
            let aProbabilityDistribution = BuildProbabilityDistribution( &aCountDistribution );

            AssertProbabilityDistributionValid(&aProbabilityDistribution);

            assert_eq!( GetMin(&aProbabilityDistribution), (&    20, &(10_000f64 / 11_110f64)) );
            assert_eq!( GetMax(&aProbabilityDistribution), (&20_000, &(    10f64 / 11_110f64)) );

            assert_eq!( aProbabilityDistribution, flatmap!
            {
                    20 => 10_000f64 / 11_110f64,
                   200 =>  1_000f64 / 11_110f64,
                 2_000 =>    100f64 / 11_110f64,
                20_000 =>     10f64 / 11_110f64,
            });
        }

        // Cumulative.
        assert_eq!( BuildCumulativeDistribution(&aCountDistribution), flatmap!
        {
                20 => Count::from(10_000u32),
               200 => Count::from(11_000u32),
             2_000 => Count::from(11_100u32),
            20_000 => Count::from(11_110u32),
        });

        // Survival.
        assert_eq!( BuildSurvivalDistribution(&aCountDistribution), flatmap!
        {
                20 => Count::from(11_110u32),
               200 => Count::from( 1_110u32),
             2_000 => Count::from(   110u32),
            20_000 => Count::from(    10u32),
        });

        // Reduce.
        {
            let mut aReducedCountDistribution = aCountDistribution.clone();
            ReduceDistribution( &mut aReducedCountDistribution );

            assert_eq!( aReducedCountDistribution, flatmap!
            {
                    20 => Count::from(1_000u32),
                   200 => Count::from(  100u32),
                 2_000 => Count::from(   10u32),
                20_000 => Count::from(    1u32),
            });
        }

        // Negate.
        assert_eq!( NegateDistribution(&aCountDistribution), flatmap!
        {
            -20_000 => Count::from(    10u32),
            - 2_000 => Count::from(   100u32),
            -   200 => Count::from( 1_000u32),
            -    20 => Count::from(10_000u32),
        });

        // Clamp.
        assert_eq!( ClampDistribution(&aCountDistribution, 1000, 3000), flatmap!
        {
            1000 => Count::from(11_000u32),
            2000 => Count::from(   100u32),
            3000 => Count::from(    10u32),
        });

        // Clip.
        assert_eq!( ClipDistribution(&aCountDistribution, 1000, 3000).unwrap(), flatmap!
        {
            2000 => Count::from(100u32),
        });

        assert!( ClipDistribution(&aCountDistribution, 1000, 1500).is_none() );

        // Merge twice.
        assert_eq!( MergeCountDistributions(&aCountDistribution, &aCountDistribution, Value::add), flatmap!
        {
               220 => Count::from( 20_000_000u32),
                40 => Count::from(100_000_000u32),
               400 => Count::from(  1_000_000u32),
             2_020 => Count::from(  2_000_000u32),
             2_200 => Count::from(    200_000u32),
             4_000 => Count::from(     10_000u32),
            22_000 => Count::from(      2_000u32),
            20_020 => Count::from(    200_000u32),
            20_200 => Count::from(     20_000u32),
            40_000 => Count::from(        100u32),
        });

        // Merge three times.
        assert_eq!( MergeCountDistributions_Mul(&aCountDistribution, 3), flatmap!
        {
                60 => Count::from(1_000_000_000_000u64),
               240 => Count::from(  300_000_000_000u64),
               420 => Count::from(   30_000_000_000u64),
               600 => Count::from(    1_000_000_000u64),
             2_040 => Count::from(   30_000_000_000u64),
             2_220 => Count::from(    6_000_000_000u64),
             2_400 => Count::from(      300_000_000u64),
             4_020 => Count::from(      300_000_000u64),
             4_200 => Count::from(       30_000_000u64),
             6_000 => Count::from(        1_000_000u64),
            20_040 => Count::from(    3_000_000_000u64),
            20_220 => Count::from(      600_000_000u64),
            20_400 => Count::from(       30_000_000u64),
            22_020 => Count::from(       60_000_000u64),
            22_200 => Count::from(        6_000_000u64),
            24_000 => Count::from(          300_000u64),
            40_020 => Count::from(        3_000_000u64),
            40_200 => Count::from(          300_000u64),
            42_000 => Count::from(           30_000u64),
            60_000 => Count::from(            1_000u64),
        });
    }

    #[test]
    #[should_panic(expected = "must be <= than the clamp max")]
    fn ClampInvalidMinMax()
    {
        let _ = ClampDistribution( &flatmap!{ 0 => Count::from(1u32) }, 2, 1 );
    }

    #[test]
    fn ClipAwayAllValues()
    {
        assert!( ClipDistribution( &flatmap!{ 0 => Count::from(1u32) }, 1, 2 ).is_none() );
        assert!( ClipDistribution( &flatmap!{ 0 => Count::from(1u32) }, 2, 1 ).is_none() );
    }

    #[test]
    fn DuplicateKeys()
    {
        let expected = flatmap!
        {
            1 => Count::from(1u32)
        };

        for i in 1..=50
        {
            let fromCollectedPairs : CountDistribution = (1u32..=i).map(|x| (1, Count::from(x)) ).collect();

            // Collecting from a vector always takes the first duplicate.
            assert_eq!( fromCollectedPairs, expected );
        }

        // The macro has different behaviour, it always takes the last duplicate.
        let fromDuplicateInMacro = flatmap!
        {
            1 => Count::from(5u32),
            1 => Count::from(4u32),
            1 => Count::from(3u32),
            1 => Count::from(2u32),
            1 => Count::from(1u32),
        };

        assert_eq!( fromDuplicateInMacro, expected );
    }
}
