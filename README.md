# README #

### What is this repository for? ###

This is a Rust library for generating linear combinations of discrete probability distributions, along with functions to run queries against these distributions.

This is useful for answering many statistical questions that you might have while playing tabletop games such as Dungeons & Dragons 5th Edition.

You can also generate random samples from the distribution to simulate rolling dice if you're not interested in global analysis of the distribution.

### Example Queries ###

If I have +5 to hit and deal 2d6 + 3 damage with my greatsword, what are the odds that I will kill a goblin with 13 AC and 9 HP remaining with a single attack?

```
12,523 / 25,920 or approximately 48.3%
```

What if I have Advantage on the attack as well?

```
114,127 / 172,800 or approximately 66%
```

What if I'm also a Fighter who gets to crit on a 19 or 20?

```
88,879 / 129,600 or approximately 68.6%
```

What if I'm also a Half-Orc, so my Savage Attacks feature makes me do 3d6 extra damage on a crit rather than 2d6?

```
533,825 / 777,600 or approximately 68.7%
```

And so on...

### Code Examples ###

#### Dice ####
```rust
use dnd5e::dice::*;
use std::str::FromStr;

assert_eq!( Die::from_str("1d8 + 2").unwrap(), _1d8() + 2 );

assert_eq!( _1d20().ChanceOfGT_Value(10), 0.5 );

assert_eq!( _1d20().Advantage().ChanceToRollLowestValue(), 1.0 / 400.0 );

assert_eq!( (_1d6() + 4).Max(), (_1d12() - _2d4()).Max() );

assert_eq!( _1d6() * 2, _2d6() );
```

#### Attacks ####
```rust
use dnd5e::attack_sequence::*;
use dnd5e::attack_roller::*;
use dnd5e::attack::*;
use dnd5e::dice::*;
use dnd5e::typedefs::*;

let attacks = vec![(Attack( AttackRoller_AC::FromMod(AttackModifier(5), RollStyle::Normal), _1d2(), DamageModifier(3) ), 1),
                   (Attack( AttackRoller_AC::FromMod(AttackModifier(6), RollStyle::Normal), _1d4(), DamageModifier(1) ), 1)];

let oncePerTurnDamage = Some( _3d6() );

let attackSequence = AttackSequence(attacks, oncePerTurnDamage);

let damageDistribution = attackSequence.CalculateDamageDistribution( &AC(17) );

assert_eq!( damageDistribution.ChanceOfGE_Value(26), 551_599.0 / 9_331_200.0 ); // ~5.9%
```

#### Saving Throws ####
```rust
use dnd5e::dice::*;
use dnd5e::typedefs::*;
use dnd5e::saving_throw::*;

let savingThrow = SavingThrow::FromMod( RollStyle::Advantage, SavingThrowModifier(5) );

assert_eq!( savingThrow.ChanceToPass(DC(15)), 319.0 / 400.0 ); // 79.75%
```

#### Spells ####
```rust
use dnd5e::dice::*;
use dnd5e::typedefs::*;
use dnd5e::saving_throw::*;
use dnd5e::spell::*;

let spell = Spell::New( SpellType::Cantrip, _2d8(), DC(14) );

let savingThrow = SavingThrow::FromMod( RollStyle::Normal, SavingThrowModifier(5) );

assert_eq!( spell.ExpectedDamage(&savingThrow), MeanValue(3.6) );
```

### How do I use it in my project? ###

This library is not on [crates.io](https://crates.io) so you will have to reference the Git repo directly by adding this to your `Cargo.toml`.

```
[dependencies]
d-d-5e-statistics = { git = "https://Stuart_Rudderham@bitbucket.org/Stuart-Rudderham/d-d-5e-statistics" }`
```

### Implementation details ###

BigInts are used internally to avoid overflow as well as floating point precision issues. This especially important since many of the calculations deal with the combinatorics of dice rolling so the numbers involved can get very large very quickly. For example, 25d6 can be rolled in 6^25 different ways which is already more than a 64-bit integer can store.

There are extensive unit tests to ensure numerical accuracy in all situations. The 3 main types of tests are:

*  Statistical tests that simulate rolling the dice many times to ensure that the empirical distribution is the same as the closed form distribution.
*  Property based checks that can be run against any generic distribution to ensure that, for example, the [cumulative distribution function](https://en.wikipedia.org/wiki/Cumulative_distribution_function) is monotonically increasing.
*  Hardcoded tests against specific distributions to ensure that the results are exactly as expected, down to the last decimal place.
