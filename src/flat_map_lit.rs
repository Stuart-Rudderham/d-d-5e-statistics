// This macro was "borrowed" from btreemap! macro in the maplit crate.
// https://github.com/bluss/maplit/blob/master/src/lib.rs
#[macro_export]
macro_rules! flatmap {
    (@single $($x:tt)*) => (());
    (@count $($rest:expr),*) => (<[()]>::len(&[$(flatmap!(@single $rest)),*]));

    ($($key:expr => $value:expr,)+) => { flatmap!($($key => $value),+) };
    ($($key:expr => $value:expr),*) => {
        {
            let _cap = flatmap!(@count $($key),*);
            let mut _map = ::flat_map::FlatMap::with_capacity(_cap);
            $(
                let _ = _map.insert($key, $value);
            )*
            _map
        }
    };
}

#[cfg(test)]
mod tests
{
    #[test]
    fn CreateWithCapacity()
    {
        let fm = flatmap!{ 1 => false,
                           2 => false,
                           4 => true,
                           8 => false, };

        assert_eq!( fm.len(), 4 );
        assert_eq!( fm.capacity(), 4 );

        assert!( fm.get(&0).is_none() );
        assert!( fm.get(&1).is_some() );
        assert!( fm.get(&2).is_some() );
        assert!( fm.get(&3).is_none() );
        assert!( fm.get(&4).is_some() );
        assert!( fm.get(&5).is_none() );
        assert!( fm.get(&6).is_none() );
        assert!( fm.get(&7).is_none() );
        assert!( fm.get(&8).is_some() );
        assert!( fm.get(&9).is_none() );
    }
}
