#!/bin/bash

# https://stackoverflow.com/a/12797512
time                `# Useful for seeing how long everything takes`                                                 \
cargo test          `# Run the tests`                                                                               \
--profile $1        `# Controls if we're running debug or release tests`                                            \
-Z unstable-options `# Required for the --profile option`                                                           \
--jobs 2            `# Using 100% of the CPU makes it difficult to use my PC while running tests in the background` \
--features $2       `# Lets up enable the "run-slow-unit-tests" feature`                                            \
                                                                                                                    \
--                  `# libtest arguments`                                                                           \
--test-threads=2    `# Using 100% of the CPU makes it difficult to use my PC while running tests in the background` \
--report-time       `# Useful for seeing what tests are slow`                                                       \
-Z unstable-options `# Required for the --report-time option`                                                       \
${@:3}              `# Pass through any user supplied arguments that haven't already been used`
