#!/bin/bash

# Clear out old state
echo "-------------------------------"
echo "- CLEAN                       -"
echo "-------------------------------"
cargo clean

# Build and benchmark new code
echo "-------------------------------"
echo "- BENCH NEW                   -"
echo "-------------------------------"
time cargo bench -- --save-baseline new-feature $1

# Re-benchmark the exact same code to make sure that the benchmarks are actually stable.
#
# Disabling it for now, previous usage has shown that the benchmarks generally are
# stable and running them twice makes the test take, well, twice as long.
#
#echo "-------------------------------"
#echo "- BENCH NEW AGAIN             -"
#echo "-------------------------------"
#time cargo bench -- --save-baseline new-feature-unchanged

# Clear new code
echo "-------------------------------"
echo "- STASH                       -"
echo "-------------------------------"
git stash

# Build and benchmark old code
echo "-------------------------------"
echo "- BENCH OLD                   -"
echo "-------------------------------"
time cargo bench -- --save-baseline initial $1

# Bring back the old code.
echo "-------------------------------"
echo "- STASH POP                   -"
echo "-------------------------------"
git stash pop

# Compare the results and save for offline analysis
critcmp initial new-feature > cmp.txt

# Compare results and print to screen so I can easily see differences when I come back to the computer.
#critcmp initial new-feature new-feature-unchanged
critcmp initial new-feature
