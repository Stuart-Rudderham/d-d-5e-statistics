use more_asserts::*;
use num_traits::identities::Zero;
use serde::Deserialize;
use std::ops::Add;
use crate::dice::*;
use crate::distribution::*;
use crate::typedefs::*;

pub enum AttackResult
{
    Hit        ,
    CrititalHit,
    Miss       ,
}

const CriticalMissThreshold : Value =  1; // I don't think there are any features that affect crit misses?

pub fn GetCriticalHitThreshold( anOverrideCriticalHitThreshold : Option<Value> ) -> Value
{
    // Crit hit on a 20 by default.
    anOverrideCriticalHitThreshold.unwrap_or(20)
}

#[derive(Clone, Debug, PartialEq, Deserialize)]
#[serde(from = "DeserializeData_AttackRoller")]
pub struct AttackRoller
{
    baseAttackDie               : Die,          // Basic d20, weighted as desired (advantage, disadvantage, etc...).

    attackModifierDie           : Die,          // Any modifiers for the attack roll. Needs to be separate to handle crits properly.

    combinedAndClippedAttackDie : Option<Die>,  // An optimization for CountWaysToHitNonCritical.
                                                // It is "baseAttackDie + attackModifierDie" with
                                                // the crit hit/miss values removed.

    criticalHitThreshold        : Value,        // By default it's 20, but various class features (Champion Fighter, Hexblade) can change that.
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
struct DeserializeData_AttackRoller
{
                      attackMod                : Die,
    #[serde(default)] rollStyle                : RollStyle,
                      overrideCritHitThreshold : Option<Value>,
}

impl From<DeserializeData_AttackRoller> for AttackRoller
{
    fn from(data : DeserializeData_AttackRoller) -> Self
    {
        AttackRoller::FromDie(data.attackMod, data.rollStyle, data.overrideCritHitThreshold)
    }
}

impl AttackRoller
{
    pub fn FromDie( anAttackModifierDie : Die, aRollStyle : RollStyle, anOverrideCriticalHitThreshold : Option<Value> ) -> AttackRoller
    {
        let baseAttackDie = _1d20_roll_style(aRollStyle);

        let criticalHitThreshold = GetCriticalHitThreshold(anOverrideCriticalHitThreshold);

        assert_gt!(criticalHitThreshold, CriticalMissThreshold, "Crit hit threshold ({}) must be >= crit miss threshold ({})", criticalHitThreshold, CriticalMissThreshold);

        let combinedAndClippedAttackDie =
        {
            //
            // Since clipping a distribution removes entire entries it might allow
            // the resulting distribution to be reduced more than the input was.
            //
            // For example, consider what would happen to
            //
            //    flatmap!{ 1 => 1
            //            , 2 => 4
            //            , 3 => 4 }
            //
            // If the 1 entry was clipped away.
            //
            // However, since we need to be able to calculate the raw number
            // of counts (rather than probabilities) this reduction can actually
            // change the answer drastically. So we need to take very special
            // care to not reduce the distributions we're operating on.
            //

            if let Some(clippedBase) = ClipDistribution( baseAttackDie.GetDistribution(), CriticalMissThreshold + 1, criticalHitThreshold - 1 )
            {
                // Manually merge the distributions rather than using
                // std::ops::Add to avoid reducing the distribution counts.
                let mergedDistributions = MergeCountDistributions(&clippedBase, anAttackModifierDie.GetDistribution(), Value::add);

                // Again, we cannot reduce the distribution counts or else it will be wrong.
                Some( Die::NewRawUnreduced(mergedDistributions) )
            }
            else
            {
                None
            }
        };

        AttackRoller
        {
            baseAttackDie,
            attackModifierDie : anAttackModifierDie,
            combinedAndClippedAttackDie,
            criticalHitThreshold,
        }
    }

    pub fn FromMod( anAttackModifier : AttackModifier, aRollStyle : RollStyle, anOverrideCriticalHitThreshold : Option<Value> ) -> AttackRoller
    {
        AttackRoller::FromDie( Die::from(*anAttackModifier), aRollStyle, anOverrideCriticalHitThreshold )
    }

    pub fn Roll( &self, anAC : AC ) -> AttackResult
    {
        let baseRoll = self.baseAttackDie.Roll();

        if baseRoll <= CriticalMissThreshold
        {
            // Crit miss.
            AttackResult::Miss
        }
        else if baseRoll >= self.criticalHitThreshold
        {
            AttackResult::CrititalHit
        }
        else
        {
            let modifierRoll = self.attackModifierDie.Roll();

            if baseRoll + modifierRoll < *anAC
            {
                // Normal miss.
                AttackResult::Miss
            }
            else
            {
                AttackResult::Hit
            }
        }
    }

    pub fn NumPermutations( &self ) -> Count
    {
        self.baseAttackDie.NumPermutations() * self.attackModifierDie.NumPermutations()
    }

    pub fn CountWaysToHitCritical( &self ) -> Count
    {
        self.baseAttackDie.CountWaysOfGE_Value(self.criticalHitThreshold) * self.attackModifierDie.NumPermutations()
    }

    pub fn CountWaysToHitNonCritical( &self, anAC : AC ) -> Count
    {
        match self.combinedAndClippedAttackDie
        {
            Some(ref d) => d.CountWaysOfGE_Value(*anAC), // We've removed all the crit hit/miss values so we can just do a simple check.
            None        => Count::zero(),                // We clipped away all the values, so we can only do crit hits.
        }
    }

    pub fn CountWaysToMiss( &self, anAC : AC ) -> Count
    {
        self.NumPermutations() - self.CountWaysToHitNonCritical(anAC) - self.CountWaysToHitCritical()
    }

    pub fn ChanceToHitCritical      ( &self,           ) -> Probability { CountRatioToProbability(&self.CountWaysToHitCritical   (    ), &self.NumPermutations()) }
    pub fn ChanceToHitNonCritical   ( &self, anAC : AC ) -> Probability { CountRatioToProbability(&self.CountWaysToHitNonCritical(anAC), &self.NumPermutations()) }
    pub fn ChanceToMiss             ( &self, anAC : AC ) -> Probability { CountRatioToProbability(&self.CountWaysToMiss          (anAC), &self.NumPermutations()) }

    #[cfg(test)]
    pub fn GetRollRange( &self, aMinExtension : Value, aMaxExtension : Value ) -> std::ops::RangeInclusive<Value>
    {
        let min = self.baseAttackDie.Min() + self.attackModifierDie.Min() + aMinExtension;
        let max = self.baseAttackDie.Max() + self.attackModifierDie.Max() + aMaxExtension;

        assert!(min <= max);

        min..=max
    }
}

//
// TODO: Find a better way to implement AttackRoller_SaveThenAC that isn't so invasive
//
// TODO: New attack type, AttackRollerThenSave
//     e.g. you get hit, take damage as usual, and then have to roll a con save to avoid more poison damage
//     Attack against AC causing a saving throw for more damage
//

#[cfg(test)]
mod tests
{
    use super::*;
    use num_traits::cast::ToPrimitive;
    use num_traits::identities::Zero;
    use strum::IntoEnumIterator;

    #[test]
    fn GetRollRange()
    {
        for anAttackModifierDie in &[Die::from(0), Die::from(-5), Die::from(5), _1d4(), -_1d6(), 5 + _1d4(), 10 - _1d6()]
        {
            for aRollStyle in RollStyle::iter()
            {
                for &aMinRangeExtension in [-2, 0, 2].iter()
                {
                    for &aMaxRangeExtension in [-2, 0, 2].iter()
                    {
                        let anExpectedRollRange = (1 + aMinRangeExtension + anAttackModifierDie.Min())..=(20 + aMaxRangeExtension + anAttackModifierDie.Max());

                        let rollRange = AttackRoller::FromDie(anAttackModifierDie.clone(), aRollStyle, None).GetRollRange(aMinRangeExtension, aMaxRangeExtension);

                        assert_eq!( rollRange, anExpectedRollRange );
                    }
                }
            }
        }
    }

    #[test]
    #[should_panic(expected = "Crit hit threshold (1) must be >= crit miss threshold (1)")]
    fn InvalidCritHitThreshold()
    {
        let _ = AttackRoller::FromDie( _1d4(), RollStyle::Advantage, Some(1) );
    }

    #[test]
    fn Deserialize()
    {
        let validStrings =
        [
            ( AttackRoller::FromMod( AttackModifier(5) , RollStyle::Normal        , None     ), r#"{ "attackMod": "5"                                                                        }"# ),
            ( AttackRoller::FromDie( _1d4()            , RollStyle::Advantage     , Some(18) ), r#"{ "attackMod": "1d4"      , "rollStyle": "Advantage"     , "overrideCritHitThreshold": 18 }"# ),
            ( AttackRoller::FromDie( _1d10() - 10      , RollStyle::Disadvantage  , None     ), r#"{ "attackMod": "1d10 - 10", "rollStyle": "Disadvantage"                                   }"# ),
            ( AttackRoller::FromMod( AttackModifier(-1), RollStyle::SuperAdvantage, Some(25) ), r#"{ "attackMod": "-1"       , "rollStyle": "SuperAdvantage", "overrideCritHitThreshold": 25 }"# ),
        ];

        let invalidStrings =
        [
            (r#"{ "attackModifier": "5"                }"#, "unknown field `attackModifier`"),
            (r#"{ "attackMod": "5", "rollStyle": "Foo" }"#, "unknown variant `Foo`"),
            (r#"{                                      }"#, "missing field `attackMod`"),
            (r#"{ "attackMod": "5", "foo": 2           }"#, "unknown field `foo`, expected one of `attackMod`, `rollStyle`, `overrideCritHitThreshold`"),
        ];

        crate::unit_test_utils::TestDeserializeFromJSON( &validStrings, &invalidStrings );
    }

    fn TestAttackRoller( anAttackModifier : AttackModifier, anAttackModifierDie : Die, anOverrideCriticalHitThreshold : Option<Value> )
    {
        let criticalHitThreshold = GetCriticalHitThreshold(anOverrideCriticalHitThreshold);

        for aRollStyle in RollStyle::iter()
        {
            println!("RollStyle: {:?}", aRollStyle);

            let anAttackRoller = AttackRoller::FromDie( anAttackModifierDie.clone() + *anAttackModifier, aRollStyle, anOverrideCriticalHitThreshold );

            for anAC in anAttackRoller.GetRollRange(-2, 2).map(AC)
            {
                println!("    AC: {}", anAC);

                // Calculate the distribution manually.
                let calculatedSuccessProbabilityDistribution =
                {
                    let mut ret = CountDistribution::new();

                    for (baseDieValue, baseDieCount) in anAttackRoller.baseAttackDie.GetDistribution().iter()
                    {
                        for (modifierDieValue, modifierDieCount) in anAttackRoller.attackModifierDie.GetDistribution().iter()
                        {
                            let numWaysToRoll = baseDieCount * modifierDieCount;

                            let key = if *baseDieValue >= criticalHitThreshold
                            {
                                // Crit hit.
                                2
                            }
                            else if *baseDieValue <= CriticalMissThreshold
                            {
                                // Crit miss.
                                0
                            }
                            else if baseDieValue + modifierDieValue >= *anAC
                            {
                                // Normal hit.
                                1
                            }
                            else
                            {
                                // Normal miss.
                                0
                            };

                            let entry = ret.entry(key).or_insert_with(Count::zero);
                            *entry += numWaysToRoll;
                        }
                    }

                    BuildProbabilityDistribution( &ret )
                };

                let expectedChanceToMiss           = anAttackRoller.ChanceToMiss(anAC);
                let expectedChanceToHitNonCritical = anAttackRoller.ChanceToHitNonCritical(anAC);
                let expectedChanceToHitCritical    = anAttackRoller.ChanceToHitCritical();

                // Can (almost) always crit hit and always crit miss.
                if criticalHitThreshold <= 20
                {
                    assert_gt!( expectedChanceToHitCritical, 0.0 );
                }

                assert_gt!( expectedChanceToMiss, 0.0 );

                // Compare it with the closed form version, they should be identical.
                let mut expectedProbabilityDistribution = flatmap!
                {
                    0 => expectedChanceToMiss,
                    1 => expectedChanceToHitNonCritical,
                    2 => expectedChanceToHitCritical,
                };

                // If it's not possible to hit/crit hit/miss then that entry won't
                // appear in calculatedSuccessProbabilityDistribution and we need
                // expectedProbabilityDistribution to match it exactly.
                expectedProbabilityDistribution.retain(|_, probability| *probability > 0.0);


                // Calculate the mean ourselves since there isn't a function for it.
                let expectedValue =
                {
                    let weightedSum = (0u32 * anAttackRoller.CountWaysToMiss          (anAC))
                                    + (1u32 * anAttackRoller.CountWaysToHitNonCritical(anAC))
                                    + (2u32 * anAttackRoller.CountWaysToHitCritical   (    ));

                    MeanValue(weightedSum.to_f64().unwrap() / anAttackRoller.NumPermutations().to_f64().unwrap())
                };

                assert_eq!( calculatedSuccessProbabilityDistribution, expectedProbabilityDistribution );

                // Test it a second way by emperically rolling things.
                let RollFunction = ||
                {
                    // Convert to a number to re-use existing test framework.
                    match anAttackRoller.Roll(anAC)
                    {
                        AttackResult::Hit         => 1,
                        AttackResult::CrititalHit => 2,
                        AttackResult::Miss        => 0,
                    }
                };

                // Magic number. Needs to be low so that the tests don't take forever to run
                // but high enough that we don't get spurious failures due to random rolling.
                let numIterations = 50_000;

                crate::unit_test_utils::TestRolling(RollFunction, numIterations, expectedValue, &expectedProbabilityDistribution);
            }
        }
    }

    // By having many smaller tests we can run them in parallel rather than having a single monolithic test.
    mod unrolled
    {
        use super::*;

        macro_rules! UnrollTest_AddAndSubtract
        {
            ($attackModValueName:ident, $attackModValue:expr, $attackModDieName:ident, $attackModDie:expr, $critHitThresholdName:ident, $critHitThreshold:expr) =>
            {
                // Use the paste crate to be able to construct concatenated function names.
                paste::item!
                {
                    #[test] fn [< TestAttackRoller_AttackMod _ $attackModValueName _ Add $attackModDieName _ CritHit _ $critHitThresholdName >] () { TestAttackRoller( $attackModValue,  $attackModDie, $critHitThreshold ); }
                    #[test] fn [< TestAttackRoller_AttackMod _ $attackModValueName _ Sub $attackModDieName _ CritHit _ $critHitThresholdName >] () { TestAttackRoller( $attackModValue, -$attackModDie, $critHitThreshold ); }
                }
            };
        }

        macro_rules! UnrollTest_CritHitThreshold
        {
            ($attackModValueName:ident, $attackModValue:expr, $attackModDieName:ident, $attackModDie:expr) =>
            {
                UnrollTest_AddAndSubtract!( $attackModValueName, $attackModValue, $attackModDieName, $attackModDie, Norm, None     ); // Default behaviour
                UnrollTest_AddAndSubtract!( $attackModValueName, $attackModValue, $attackModDieName, $attackModDie, __25, Some(25) ); // Can't crit hit.
                UnrollTest_AddAndSubtract!( $attackModValueName, $attackModValue, $attackModDieName, $attackModDie, __18, Some(18) ); // Custom crit range, mix of normal & crit hits.
                UnrollTest_AddAndSubtract!( $attackModValueName, $attackModValue, $attackModDieName, $attackModDie, ___2, Some( 2) ); // Only crit hit.
            };
        }

        macro_rules! UnrollTest_AttackModDie
        {
            ($attackModValueName:ident, $attackModValue:expr) =>
            {
                UnrollTest_CritHitThreshold!( $attackModValueName, $attackModValue, _1d0___, _1d0()             );
                UnrollTest_CritHitThreshold!( $attackModValueName, $attackModValue, _1d1___, _1d1()             );
                UnrollTest_CritHitThreshold!( $attackModValueName, $attackModValue, _1d2___, _1d2()             );
                UnrollTest_CritHitThreshold!( $attackModValueName, $attackModValue, _1d4___, _1d4()             );
                UnrollTest_CritHitThreshold!( $attackModValueName, $attackModValue, _2d6Adv, _2d6().Advantage() );
                UnrollTest_CritHitThreshold!( $attackModValueName, $attackModValue, _2d20__, _2d20()            );
            };
        }

        UnrollTest_AttackModDie!( N30, AttackModifier(-30) );
        UnrollTest_AttackModDie!( N10, AttackModifier(-10) );
        UnrollTest_AttackModDie!( N3_, AttackModifier(- 3) );
        UnrollTest_AttackModDie!( N2_, AttackModifier(- 2) );
        UnrollTest_AttackModDie!( N1_, AttackModifier(- 1) );
        UnrollTest_AttackModDie!( P0_, AttackModifier(  0) );
        UnrollTest_AttackModDie!( P1_, AttackModifier(  1) );
        UnrollTest_AttackModDie!( P2_, AttackModifier(  2) );
        UnrollTest_AttackModDie!( P3_, AttackModifier(  3) );
        UnrollTest_AttackModDie!( P10, AttackModifier( 10) );
        UnrollTest_AttackModDie!( P30, AttackModifier( 30) );
    }

    #[test]
    fn Hardcoded()
    {
        // Just some very basic tests with hardcoded numbers as a sanity check.
        let Test = |anAttackRoller : AttackRoller, anExpectedNumWaysToHitCritial : u64, anExpectedNumWaysToHitNonCritial : u64, anExpectedNumWaysToMiss : u64|
        {
            let anAC = AC(16);

            assert_eq!( anAttackRoller.CountWaysToHitCritical   (    ), Count::from(anExpectedNumWaysToHitCritial   ) );
            assert_eq!( anAttackRoller.CountWaysToHitNonCritical(anAC), Count::from(anExpectedNumWaysToHitNonCritial) );
            assert_eq!( anAttackRoller.CountWaysToMiss          (anAC), Count::from(anExpectedNumWaysToMiss         ) );
        };

        Test( AttackRoller::FromDie(1 - _1d2()         , RollStyle::Normal              , None    ),    2,   7,  31 );
        Test( AttackRoller::FromDie(1 + _1d2()         , RollStyle::Advantage           , None    ),   78, 409, 313 );
        Test( AttackRoller::FromMod(AttackModifier(  1), RollStyle::Disadvantage        , None    ),    1,  35, 364 );
        Test( AttackRoller::FromMod(AttackModifier(-10), RollStyle::SuperAdvantage      , Some( 2)), 7999,   0,   1 );
        Test( AttackRoller::FromDie(_1d4()             , RollStyle::Normal_HalflingLucky, Some(19)),  168, 462, 970 );
    }
}
