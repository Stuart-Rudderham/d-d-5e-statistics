use crate::dice::*;
use crate::distribution::*;
use crate::typedefs::*;
use crate::saving_throw::*;
use crate::check_against_dc::*;
use crate::parsing::GenerateJSON;

use std::convert::TryFrom;
use std::ops::Add;

use serde::Deserialize;

use rand::prelude::*;

use strum::{EnumIter, Display, IntoEnumIterator};

use num_traits::identities::Zero;

#[derive(Copy, Clone, Debug, PartialEq, Deserialize, EnumIter, Display)]
pub enum PassCriteria
{
    MajorityPass,
    AverageRoll,
}

pub fn GenerateRandomPassCriteriaString<R : Rng>( rng : &mut R ) -> String
{
    let randomPassCriteria = PassCriteria::iter().choose(rng).unwrap();

    // These are for use in JSON strings, hence the embedded quotes.
    format!("\"{}\"", randomPassCriteria)
}

impl Default for PassCriteria
{
    fn default() -> Self
    {
        // This is what group checks use in the official rules.
        PassCriteria::MajorityPass
    }
}

#[derive(Clone, Debug, PartialEq, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct GroupCheck
{
    group        : Vec<SavingThrow>,

    #[serde(default)]
    passCriteria : PassCriteria,
}

// NOTE: This should match the structure of GroupCheck.
pub fn GenerateRandomJSON_GroupCheck<R : Rng>( rng : &mut R ) -> String
{
    let group = GenerateJSON::BuildArray( vec!
    [
        Some( GenerateRandomJSON_SavingThrow(rng) ),
        Some( GenerateRandomJSON_SavingThrow(rng) ),
        Some( GenerateRandomJSON_SavingThrow(rng) ),
    ]);

    let passCriteria = if rng.gen()
    {
        Some( GenerateRandomPassCriteriaString(rng) )
    }
    else
    {
        None
    };

    GenerateJSON::BuildObject( vec!
    [
        GenerateJSON::BuildField( "group"       , Some(group)  ),
        GenerateJSON::BuildField( "passCriteria", passCriteria ),
    ])
}

impl CheckAgainstDC for GroupCheck
{
    fn Roll( &self, aDC : DC ) -> bool
    {
        match self.passCriteria
        {
            PassCriteria::MajorityPass => self.Roll_MajorityPass(aDC),
            PassCriteria::AverageRoll  => self.Roll_AverageRoll (aDC),
        }
    }

    fn NumPermutations(&self) -> Count
    {
        // TODO: Could cache for performance.
        self.group.iter().map(|savingThrow| savingThrow.NumPermutations()).product()
    }

    fn CountWaysToPass( &self, aDC : DC ) -> Count
    {
        match self.passCriteria
        {
            PassCriteria::MajorityPass => self.CountWaysToPass_MajorityPass(aDC),
            PassCriteria::AverageRoll  => self.CountWaysToPass_AverageRoll (aDC),
        }
    }

    #[cfg(test)]
    fn GetRollRange( &self, aMinExtension : Value, aMaxExtension : Value ) -> std::ops::RangeInclusive<Value>
    {
        let groupRollRanges = self.group.iter().map(|x| x.GetRollRange(aMinExtension, aMaxExtension));

        crate::unit_test_utils::GetEncompassingRollRange( groupRollRanges )
    }
}

impl GroupCheck
{
    fn Roll_MajorityPass( &self, aDC : DC ) -> bool
    {
        assert!(self.passCriteria == PassCriteria::MajorityPass);

        let numPassesRequired = Self::NumPassesRequired(self.group.len());

        let mut numPasses = 0;

        for savingThrow in &self.group
        {
            numPasses += if savingThrow.Roll(aDC) { 1 } else { 0 };

            if numPasses >= numPassesRequired
            {
                return true;
            }
        }

        false
    }

    fn Roll_AverageRoll( &self, aDC : DC ) -> bool
    {
        assert!(self.passCriteria == PassCriteria::AverageRoll);

        let totalGroupDC = Self::CalculateTotalGroupDC(self.group.len(), aDC);

        let rolledValues : Value = self.group.iter().map(|savingThrow| savingThrow.RollValue() ).sum();

        rolledValues >= totalGroupDC
    }

    fn CountWaysToPass_MajorityPass(&self, aDC : DC) -> Count
    {
        assert!(self.passCriteria == PassCriteria::MajorityPass);

        fn Helper( savingThrows : &[SavingThrow], aDC : DC, numPassesRequired : usize ) -> Count
        {
            if numPassesRequired > savingThrows.len()
            {
                // Not possible to pass this group check.
                return Count::zero();
            }

            // TODO: Add fastpath for must make all future saves (i.e. "numPassesRequired == savingThrows.len()")

            if numPassesRequired == 0
            {
                // Already succeeded, doesn't matter what anyone else rolls.
                return savingThrows.iter().map(|x| x.NumPermutations()).product()
            }

            // Recursive case.
            //
            // This unwrap() is safe because if there were 0 saving
            // throws left we would already have early exited above.
            let (savingThrow, remainingSavingThrows) = savingThrows.split_first().unwrap();

            // TODO: This potentially calculates CountWaysToPass twice.
            let numWaysToPass = savingThrow.CountWaysToPass(aDC);
            let numWaysToFail = savingThrow.CountWaysToFail(aDC);

            let total = (numWaysToPass * Helper(remainingSavingThrows, aDC, numPassesRequired - 1))
                      + (numWaysToFail * Helper(remainingSavingThrows, aDC, numPassesRequired    ));
            total
        }

        Helper( &self.group, aDC, Self::NumPassesRequired(self.group.len()) )
    }

    fn CountWaysToPass_AverageRoll(&self, aDC : DC) -> Count
    {
        assert!(self.passCriteria == PassCriteria::AverageRoll);

        let totalGroupDC = Self::CalculateTotalGroupDC(self.group.len(), aDC);

        let groupDistribution : Die = self.group.iter().map(|x| x.GetDie() ).fold(_1d0(), Die::add );

        groupDistribution.CountWaysOfGE_Value(totalGroupDC)
    }

    pub fn NumPassesRequired( groupSize : usize ) -> usize
    {
        // We want to round up rather than the usual down, hence the "+ 1".
        (groupSize + 1) / 2
    }

    pub fn CalculateTotalGroupDC( groupSize : usize , aDC : DC) -> Value
    {
        // We will never realistically have a large enough group that this conversion fails.
        let groupSizeAsValue = Value::try_from(groupSize).unwrap();

        *aDC * groupSizeAsValue
    }

    // TODO:
    //     - Calculate the odds of X out of Y group members passing
    //     - Calculate the odds of >= X out of Y group members passing
}

pub fn GroupCheck( group : Vec<SavingThrow>, passCriteria : PassCriteria ) -> GroupCheck
{
    // TODO: Graceful failure, return Result instead.
    assert!( !group.is_empty(), "Cannot have a group check with no one in the group!" );

    GroupCheck{ group, passCriteria }
}

#[cfg(test)]
mod tests
{
    use super::*;
    use itertools::Itertools;

    #[test]
    fn Deserialize()
    {
        // Authored input.
        let e1 = ( SavingThrow::FromMod( RollStyle::Normal      , SavingThrowModifier(-1) ), r#"{ "modifier": "-1"                                         }"# );
        let e2 = ( SavingThrow::FromDie( RollStyle::Normal      , 5 + _1d4()              ), r#"{ "modifier": "5 + 1d4"      , "rollStyle": "Normal"       }"# );
        let e3 = ( SavingThrow::FromMod( RollStyle::Advantage   , SavingThrowModifier(3)  ), r#"{ "modifier": "3"            , "rollStyle": "Advantage"    }"# );
        let e4 = ( SavingThrow::FromDie( RollStyle::Normal      , 1 - _1d4() + _1d8()     ), r#"{ "modifier": "1 - 1d4 + 1d8"                              }"# );
        let e5 = ( SavingThrow::FromMod( RollStyle::Disadvantage, SavingThrowModifier(0)  ), r#"{ "modifier": "0"            , "rollStyle": "Disadvantage" }"# );

        let passCriterias = vec!
        [
            (PassCriteria::default()   , ""                                    ),
            (PassCriteria::MajorityPass, r#", "passCriteria": "MajorityPass" "#), // Note the "," at the beginning.
            (PassCriteria::AverageRoll , r#", "passCriteria": "AverageRoll"  "#), // Note the "," at the beginning.
        ];

        for passCriteria in &passCriterias
        {
            let validStrings =
            [
                ( GroupCheck( vec![e1.0.clone()                                                        ], passCriteria.0 ), format!( r#"{{ "group": [{}                ] {} }}"#, e1.1                        , passCriteria.1 ) ),
                ( GroupCheck( vec![e1.0.clone(), e2.0.clone()                                          ], passCriteria.0 ), format!( r#"{{ "group": [{}, {}            ] {} }}"#, e1.1, e2.1                  , passCriteria.1 ) ),
                ( GroupCheck( vec![e1.0.clone(), e2.0.clone(), e3.0.clone()                            ], passCriteria.0 ), format!( r#"{{ "group": [{}, {}, {}        ] {} }}"#, e1.1, e2.1, e3.1            , passCriteria.1 ) ),
                ( GroupCheck( vec![e1.0.clone(), e2.0.clone(), e3.0.clone(), e4.0.clone()              ], passCriteria.0 ), format!( r#"{{ "group": [{}, {}, {}, {}    ] {} }}"#, e1.1, e2.1, e3.1, e4.1      , passCriteria.1 ) ),
                ( GroupCheck( vec![e1.0.clone(), e2.0.clone(), e3.0.clone(), e4.0.clone(), e5.0.clone()], passCriteria.0 ), format!( r#"{{ "group": [{}, {}, {}, {}, {}] {} }}"#, e1.1, e2.1, e3.1, e4.1, e5.1, passCriteria.1 ) ),
            ];

            let invalidStrings =
            [
                (r#"{ "Group": [{ "modifier": "-1"          }], "passCriteria": "MajorityPass"           }"#, "unknown field `Group`, expected `group` or `passCriteria`"),
                (r#"{ "group": [{ "modifier": "-1"          }], "PassCriteria": "MajorityPass"           }"#, "unknown field `PassCriteria`, expected `group` or `passCriteria`"),
                (r#"{ "group": [{ "modifier": "-1"          }], "passCriteria": "Foo"                    }"#, "unknown variant `Foo`, expected `MajorityPass` or `AverageRoll`"),
                (r#"{ "group": [{ "mod": "-1"               }], "passCriteria": "MajorityPass"           }"#, "unknown field `mod`, expected `rollStyle` or `modifier`"),
                (r#"{ "group": [{ "modifier": "-1", "foo": 2}], "passCriteria": "MajorityPass"           }"#, "unknown field `foo`, expected `rollStyle` or `modifier`"),
                (r#"{ "group": [{ "modifier": "-1"          }], "passCriteria": "MajorityPass", "foo": 2 }"#, "unknown field `foo`, expected `group` or `passCriteria`"),
            ];

            crate::unit_test_utils::TestDeserializeFromJSON( &validStrings, &invalidStrings );
        }

        // Random input.
        let mut rng = rand::thread_rng();
        crate::unit_test_utils::TestGenerateRandomJSON::<_, GroupCheck>( ||{ GenerateRandomJSON_GroupCheck(&mut rng) } );
    }

    #[test]
    fn NumPassesRequired()
    {
        assert_eq!( GroupCheck::NumPassesRequired(1), 1 );
        assert_eq!( GroupCheck::NumPassesRequired(2), 1 );
        assert_eq!( GroupCheck::NumPassesRequired(3), 2 );
        assert_eq!( GroupCheck::NumPassesRequired(4), 2 );
        assert_eq!( GroupCheck::NumPassesRequired(5), 3 );
        assert_eq!( GroupCheck::NumPassesRequired(6), 3 );
        assert_eq!( GroupCheck::NumPassesRequired(7), 4 );
    }

    #[test]
    fn CalculateTotalGroupDC()
    {
        assert_eq!( GroupCheck::CalculateTotalGroupDC( 1, DC(10) ), 10 );
        assert_eq!( GroupCheck::CalculateTotalGroupDC( 2, DC(10) ), 20 );
        assert_eq!( GroupCheck::CalculateTotalGroupDC( 3, DC(10) ), 30 );
        assert_eq!( GroupCheck::CalculateTotalGroupDC( 4, DC(10) ), 40 );
        assert_eq!( GroupCheck::CalculateTotalGroupDC( 5, DC(10) ), 50 );
        assert_eq!( GroupCheck::CalculateTotalGroupDC( 6, DC(10) ), 60 );
        assert_eq!( GroupCheck::CalculateTotalGroupDC( 7, DC(10) ), 70 );
    }

    #[test]
    fn Commutative()
    {
        for passCriteria in PassCriteria::iter()
        {
            let mut saves = vec!
            [
                SavingThrow::FromMod( RollStyle::Normal      , SavingThrowModifier(5)  ),
                SavingThrow::FromDie( RollStyle::Advantage   , 3 - _1d4()              ),
                SavingThrow::FromMod( RollStyle::Disadvantage, SavingThrowModifier(-2) ),
                SavingThrow::FromDie( RollStyle::Normal      , _2d10()                 ),
            ];

            let group = GroupCheck( saves.clone(), passCriteria );

            saves.reverse();

            let reversedGroup = GroupCheck(saves, passCriteria );

            let         groupRollRange =         group.GetRollRange(-2, 2);
            let reversedGroupRollRange = reversedGroup.GetRollRange(-2, 2);

            assert_eq!(groupRollRange, reversedGroupRollRange);

            for aDC in groupRollRange.map(DC)
            {
                // The order of the members in the group shouldn't affect anything.
                assert_eq!( group.CalculateSuccessDistribution(aDC), reversedGroup.CalculateSuccessDistribution(aDC) );
            }
        }
    }

    fn TestGroupCheck( someSavingThrows : Vec<SavingThrow> )
    {
        // As an optimization, cache all possible combinations of values the saving throws could roll.
        // We do this outside of the nested for-loops below for a huge speedup (1m 30s down to 3s).
        let explodedDistribution =
        {
            let mut ret : flat_map::FlatMap<Vec<Value>, Count> = flat_map::FlatMap::new();

            for combination in someSavingThrows.iter().map(|aSavingThrow| aSavingThrow.GetDie().GetDistribution().iter()).multi_cartesian_product()
            {
                let numWaysToRoll : Count = combination.iter().map(|(_, count)| *count).product();

                let mut valuesRolled : Vec<Value> = combination.iter().map(|(value, _)| **value).collect();

                // Reduce the number of unique entries we store, which reduces the number of values we need
                // to iterate over below. For example, now the entries (1, 2, 1) and (1, 1, 2) would be merged.
                valuesRolled.sort_unstable();

                *ret.entry(valuesRolled).or_insert_with(Count::zero) += numWaysToRoll;
            }

            ret
        };

        for passCriteria in PassCriteria::iter()
        {
            let groupSize = someSavingThrows.len();
            let groupCheck = GroupCheck(someSavingThrows.clone(), passCriteria);

            for aDC in groupCheck.GetRollRange(-2, 2).map(DC)
            {
                // Calculate the distribution manually.
                let calculatedSuccessProbabilityDistribution =
                {
                    let mut ret = CountDistribution::new();

                    for (savingThrowValues, count) in &explodedDistribution
                    {
                        let passedCheck = match passCriteria
                        {
                            PassCriteria::MajorityPass =>
                            {
                                let numPassesRequired = GroupCheck::NumPassesRequired(groupSize);

                                let numPassed = savingThrowValues.iter().filter(|a| **a >= *aDC).count();

                                numPassed >= numPassesRequired
                            },

                            PassCriteria::AverageRoll =>
                            {
                                let totalGroupDC = GroupCheck::CalculateTotalGroupDC(groupSize, aDC);

                                let groupRolled : Value = savingThrowValues.iter().sum();

                                groupRolled >= totalGroupDC
                            },
                        };

                        let key = if passedCheck { 1 } else { 0 };

                        *ret.entry(key).or_insert_with(Count::zero) += count;
                    }

                    BuildProbabilityDistribution( &ret )
                };

                // Compare it with the closed form version, they should be identical.
                let expectedSuccessCountDistribution = groupCheck.CalculateSuccessDistribution(aDC);

                let expectedSuccessProbabilityDistribution = expectedSuccessCountDistribution.BuildProbabilityDistribution();

                assert_eq!( calculatedSuccessProbabilityDistribution, expectedSuccessProbabilityDistribution );

                // Test it a second way by empirically rolling things.
                let RollFunction = ||
                {
                    if groupCheck.Roll(aDC) { 1 } else { 0 }
                };

                // Magic number. Needs to be low so that the tests don't take forever to run
                // but high enough that we don't get spurious failures due to random rolling.
                let numIterations = 50_000;

                crate::unit_test_utils::TestRolling(RollFunction, numIterations, expectedSuccessCountDistribution.Mean(), &expectedSuccessProbabilityDistribution);
            }
        }
    }

    #[test]
    fn TestGroupCheck_CannotPass()
    {
        TestGroupCheck( vec!
        [
            SavingThrow::FromMod( RollStyle::Normal      , SavingThrowModifier(-30) ),
            SavingThrow::FromMod( RollStyle::Advantage   , SavingThrowModifier(-30) ),
            SavingThrow::FromMod( RollStyle::Disadvantage, SavingThrowModifier(-30) ),
        ]);
    }

    #[test]
    fn TestGroupCheck_CannotFail()
    {
        TestGroupCheck( vec!
        [
            SavingThrow::FromMod( RollStyle::Normal      , SavingThrowModifier(30) ),
            SavingThrow::FromMod( RollStyle::Advantage   , SavingThrowModifier(30) ),
            SavingThrow::FromMod( RollStyle::Disadvantage, SavingThrowModifier(30) ),
        ]);
    }

    #[test]
    fn TestGroupCheck_PartyOf1()
    {
        TestGroupCheck( vec!
        [
            SavingThrow::FromMod( RollStyle::Advantage, SavingThrowModifier(3) ),
        ]);
    }

    #[test]
    fn TestGroupCheck_PartyOf2()
    {
        TestGroupCheck( vec!
        [
            SavingThrow::FromDie( RollStyle::Normal, 4 - _1d4() ),
            SavingThrow::FromDie( RollStyle::Normal, _1d4() - 4 ),
        ]);
    }

    #[test]
    fn TestGroupCheck_PartyOf3()
    {
        TestGroupCheck( vec!
        [
            SavingThrow::FromMod( RollStyle::Disadvantage, SavingThrowModifier(3)  ),
            SavingThrow::FromMod( RollStyle::Normal      , SavingThrowModifier(-1) ),
            SavingThrow::FromDie( RollStyle::Advantage   , 1 + _1d4()              ),
        ]);
    }

    #[test]
    fn TestGroupCheck_PartyOf4()
    {
        // With deoptimized code this takes ~10 seconds to run.
        // The other TestGroupCheck_PartyOfN() tests should cover most of this anyways.
        if !crate::unit_test_utils::ShouldRunSlowUnitTests()
        {
            return;
        }

        TestGroupCheck( vec!
        [
            SavingThrow::FromDie( RollStyle::Normal, _1d10() ),
            SavingThrow::FromDie( RollStyle::Normal, _1d10() ),
            SavingThrow::FromDie( RollStyle::Normal, _1d10() ),
            SavingThrow::FromDie( RollStyle::Normal, _1d10() ),
        ]);
    }

    #[test]
    fn TestGroupCheck_PartyOf5()
    {
        // With deoptimized code this takes ~40 seconds to run.
        // The other TestGroupCheck_PartyOfN() tests should cover most of this anyways.
        if !crate::unit_test_utils::ShouldRunSlowUnitTests()
        {
            return;
        }

        TestGroupCheck( vec!
        [
            SavingThrow::FromMod( RollStyle::Normal, SavingThrowModifier(-4) ),
            SavingThrow::FromMod( RollStyle::Normal, SavingThrowModifier(-2) ),
            SavingThrow::FromMod( RollStyle::Normal, SavingThrowModifier( 0) ),
            SavingThrow::FromMod( RollStyle::Normal, SavingThrowModifier( 2) ),
            SavingThrow::FromMod( RollStyle::Normal, SavingThrowModifier( 4) ),
        ]);
    }

    #[test]
    fn Hardcoded()
    {
        // Just some very basic tests with hardcoded numbers as a sanity check.
        let Test = |aPassCriteria : PassCriteria, anExpectedSuccessProbabilityDistribution : ProbabilityDistribution|
        {
            let dc = DC(14);

            let savingThrows = vec!
            [
                SavingThrow::FromMod( RollStyle::Disadvantage, SavingThrowModifier(3)  ),
                SavingThrow::FromMod( RollStyle::Normal      , SavingThrowModifier(-1) ),
                SavingThrow::FromDie( RollStyle::Advantage   , 1 + _1d4()              ),
            ];

            let groupCheck = GroupCheck( savingThrows, aPassCriteria );

            let calculatedSuccessProbabilityDistribution = groupCheck.CalculateSuccessDistribution(dc).BuildProbabilityDistribution();

            assert_eq!(calculatedSuccessProbabilityDistribution, anExpectedSuccessProbabilityDistribution);
        };

        Test( PassCriteria::MajorityPass, flatmap!{0 =>  7_891_200./12_800_000., 1 =>  4_908_800./12_800_000.} );
        Test( PassCriteria::AverageRoll , flatmap!{0 =>  8_776_810./12_800_000., 1 =>  4_023_190./12_800_000.} );
    }
}
