use boolean_enums::*;
use num_integer::Integer;
use num_integer;
use num_traits::identities::Zero;
use num_traits;
use rand::distributions::{Distribution, Uniform};
use rand::prelude::*;
use rand::seq::IteratorRandom;
use rand;
use serde::{Serialize, Deserialize};
use std::cmp;
use std::convert::TryFrom;
use std::ops::Add;
use std::ops::AddAssign;
use std::ops::Mul;
use std::ops::MulAssign;
use std::ops::Neg;
use std::ops::Sub;
use std::ops::SubAssign;
use std::str::FromStr;
use strum::{EnumIter, Display, IntoEnumIterator};

use crate::distribution::*;
use crate::typedefs::MeanValue;
use crate::parsing::*;
use crate::parsing::MaxSizeNonZeroU8;

#[derive(Copy, Clone, Debug, Serialize, Deserialize, EnumIter, Display)]
pub enum RollStyle
{
    Normal,
    Advantage,              // Roll 2, take the highest
    SuperAdvantage,         // Roll 3, take the highest
    Disadvantage,           // Roll 2, take the lowest
    Normal_HalflingLucky,   // A normal die roll done by a Halfling (i.e. re-roll a 1 one time)
}

pub fn GenerateRandomRollStyleString<R : Rng>( rng : &mut R ) -> String
{
    let randomRollStyle = RollStyle::iter().choose(rng).unwrap();

    // These are for use in JSON strings, hence the embedded quotes.
    format!("\"{}\"", randomRollStyle)
}

impl Default for RollStyle
{
    fn default() -> Self
    {
        RollStyle::Normal
    }
}

#[derive(Clone, Debug, Deserialize)]
#[serde(try_from = "DeserializeData_Die")]
pub struct Die
{
    // This variable fully defines the Die.
    // It holds the number of different ways each value can be rolled.
    pub distribution            : CountDistribution                           ,

    // These variables are derived from the distribution and cached for quick
    // lookups. OnceCell is used to allow us to lazily compute the data if it
    // is slow to calculate. This can be a big saving as a common pattern is
    // to construct and combine multiple dice into a single Die and then start
    // querying it. By lazily computing things only when they are needed we
    // avoid doing a bunch of work only to throw it away.
    //
    // We use once_cell::sync rather than once_cell::unsync so that we can
    // have global static Die variables for commonly used dice (e.g. 1d6).
    //
    // Static variables in Rust are required to implement the Sync trait
    // which once_cell::unsync does not since it uses interior mutability.
    //
    // TODO: Try thread local stuff?
    cached_min                  : Value                                       , // The minimum value the die can produce. Quick to calculate so don't use OnceCell to reduce struct size.
    cached_max                  : Value                                       , // The maximum value the die can produce. Quick to calculate so don't use OnceCell to reduce struct size.
    cached_mean                 : once_cell::sync::OnceCell<MeanValue>        , // The expected value that the die will produce.
    cached_numPermutations      : once_cell::sync::OnceCell<Count>            , // The total number of different ways the die can be rolled.
    cached_rngRange             : once_cell::sync::OnceCell<Uniform<Count>>   , // Used to speed up generating random numbers for rolling.

    cached_survivalDistribution : once_cell::sync::OnceCell<CountDistribution>, // The number of different ways the Die can roll >= a Value.
                                                                                // Used to speed up distribution lookups for rolling and queries.
                                                                                // Use this rather than the CDF to optimize for >= queries rather than <=.
}

impl PartialEq for Die
{
    fn eq( &self, other: &Die ) -> bool
    {
        // All other struct members are derived from the distribution, so we don't need to compare them.
        self.distribution == other.distribution
    }
}

impl Die
{
    #[must_use]
    fn ToProbability( &self, aCount : &Count ) -> Probability
    {
        CountRatioToProbability( aCount, self.NumPermutations() )
    }

    #[must_use]
    pub fn GetDistribution(&self) -> &CountDistribution
    {
        &self.distribution
    }

    #[must_use]
    pub fn BuildProbabilityDistribution(&self) -> ProbabilityDistribution
    {
        // Save ourselves having to recalculate the number
        // of permutations since we have it cached already.
        BuildProbabilityDistributionHelper( &self.distribution, self.NumPermutations() )
    }

    #[must_use]
    pub fn GetSurvivalDistribution(&self) -> &CountDistribution
    {
        self.cached_survivalDistribution.get_or_init( || BuildSurvivalDistribution(&self.distribution) )
    }

    #[must_use]
    pub fn BuildSurvivalProbabilityDistribution(&self) -> ProbabilityDistribution
    {
        BuildProbabilityDistributionHelper( self.GetSurvivalDistribution(), self.NumPermutations() )
    }

    #[must_use]
    pub fn BuildCumulativeDistribution(&self) -> CountDistribution
    {
        BuildCumulativeDistribution( &self.distribution )
    }

    #[must_use]
    pub fn BuildCumulativeProbabilityDistribution(&self) -> ProbabilityDistribution
    {
        BuildProbabilityDistributionHelper( &self.BuildCumulativeDistribution(), self.NumPermutations() )
    }

    #[must_use]
    fn RollPriv( &self, aPermutationIndex : &Count ) -> Value
    {
        assert!( aPermutationIndex < self.NumPermutations() );

        // Inverse transform sampling.
        // Search the distribution for range that contains the given permutation index.
        // The arrays are short enough that it is faster to do a linear search than to do a binary search.
        let entry = self.GetSurvivalDistribution().iter().rev().find(|(_, partialCount)| aPermutationIndex < *partialCount);

        // Use "unwrap_or_else" rather than "expect" to avoid unconditional formatting of error string.
        *entry.unwrap_or_else(|| unreachable!("Rolling algorithm has a bug!\nDie = {:?}\nPermutation index = {:?}", self, aPermutationIndex)).0
    }

    #[must_use]
    pub fn Roll( &self ) -> Value
    {
        let rngRange = self.cached_rngRange.get_or_init(|| Uniform::new(Count::zero(), self.NumPermutations().clone()));

        //
        // It's not the most flexible design to hardcode the RNG directly inside
        // this function. It is, however, what is most convenient for the caller.
        //
        // Ideally we would let the caller provide the RNG as a param. This would
        // allow them to decide the trade-off between speed and quality of the random
        // number (e.g. most case probably don't require the cryptographically secure
        // generation that thread_rng provides).
        //
        // This was implemented as a prototype and the benchmark results were
        // underwhelming. Performance was compared between SmallRng and StdRng in
        // rand-0.7 and StdRng was "only" ~1.15x slower. It turns out that the
        // random number generation is not a bottleneck here, searching through the
        // survival distribution takes the majority of the time.
        //
        // So in this case it makes sense for convenience to win out over optimal
        // performance in the API design.
        //
        let r = rngRange.sample(&mut rand::thread_rng());

        self.RollPriv(&r)
    }

    #[must_use]
    pub fn Min( &self ) -> Value
    {
        self.cached_min
    }

    #[must_use]
    pub fn Max( &self ) -> Value
    {
        self.cached_max
    }

    #[must_use]
    pub fn NumPermutations( &self ) -> &Count
    {
        self.cached_numPermutations.get_or_init(|| CalculateNumPermutations(&self.distribution))
    }

    #[must_use]
    pub fn Mean( &self ) -> MeanValue
    {
        *self.cached_mean.get_or_init(|| CalculateMean(&self.distribution))
    }

    #[must_use]
    pub fn CountQuery_Die<F>( &self, aDie : &Die, aFn : F ) -> Count
        where F : Fn(Value, Value) -> bool
    {
        let mut numSuccessfulPermutations = Count::zero();

        for (myValue, myCount) in &self.distribution
        {
            for (theirValue, theirCount) in &aDie.distribution
            {
                if aFn(*myValue, *theirValue)
                {
                    let numWaysToRollThis = myCount * theirCount;

                    numSuccessfulPermutations += numWaysToRollThis;
                }
            }
        }

        numSuccessfulPermutations
    }

    #[must_use] pub fn CountWaysOfLT_Die( &self, aDie : &Die ) -> Count       { self.CountQuery_Die (aDie, |myRoll, theirRoll| myRoll <  theirRoll) }
    #[must_use] pub fn CountWaysOfLE_Die( &self, aDie : &Die ) -> Count       { self.CountQuery_Die (aDie, |myRoll, theirRoll| myRoll <= theirRoll) }
    #[must_use] pub fn CountWaysOfEQ_Die( &self, aDie : &Die ) -> Count       { self.CountQuery_Die (aDie, |myRoll, theirRoll| myRoll == theirRoll) }
    #[must_use] pub fn CountWaysOfNE_Die( &self, aDie : &Die ) -> Count       { self.CountQuery_Die (aDie, |myRoll, theirRoll| myRoll != theirRoll) }
    #[must_use] pub fn CountWaysOfGE_Die( &self, aDie : &Die ) -> Count       { self.CountQuery_Die (aDie, |myRoll, theirRoll| myRoll >= theirRoll) }
    #[must_use] pub fn CountWaysOfGT_Die( &self, aDie : &Die ) -> Count       { self.CountQuery_Die (aDie, |myRoll, theirRoll| myRoll >  theirRoll) }

    // Chance functions just delegate to the Count functions.
    #[must_use] pub fn ChanceOfLT_Die   ( &self, aDie : &Die ) -> Probability { CountRatioToProbability( &self.CountWaysOfLT_Die(aDie), &(self.NumPermutations() * aDie.NumPermutations()) ) }
    #[must_use] pub fn ChanceOfLE_Die   ( &self, aDie : &Die ) -> Probability { CountRatioToProbability( &self.CountWaysOfLE_Die(aDie), &(self.NumPermutations() * aDie.NumPermutations()) ) }
    #[must_use] pub fn ChanceOfEQ_Die   ( &self, aDie : &Die ) -> Probability { CountRatioToProbability( &self.CountWaysOfEQ_Die(aDie), &(self.NumPermutations() * aDie.NumPermutations()) ) }
    #[must_use] pub fn ChanceOfNE_Die   ( &self, aDie : &Die ) -> Probability { CountRatioToProbability( &self.CountWaysOfNE_Die(aDie), &(self.NumPermutations() * aDie.NumPermutations()) ) }
    #[must_use] pub fn ChanceOfGE_Die   ( &self, aDie : &Die ) -> Probability { CountRatioToProbability( &self.CountWaysOfGE_Die(aDie), &(self.NumPermutations() * aDie.NumPermutations()) ) }
    #[must_use] pub fn ChanceOfGT_Die   ( &self, aDie : &Die ) -> Probability { CountRatioToProbability( &self.CountWaysOfGT_Die(aDie), &(self.NumPermutations() * aDie.NumPermutations()) ) }

    #[must_use]
    pub fn CountWaysOfLT_Value( &self, aValue : Value ) -> Count
    {
        self.NumPermutations() - self.CountWaysOfGE_Value(aValue)
    }

    #[must_use]
    pub fn CountWaysOfLE_Value( &self, aValue : Value ) -> Count
    {
        self.CountWaysOfLT_Value(aValue) + self.CountWaysOfEQ_Value(aValue)
    }

    #[must_use]
    pub fn CountWaysOfEQ_Value( &self, aValue : Value ) -> Count
    {
        self.distribution.get(&aValue).cloned().unwrap_or_else(Count::zero)
    }

    #[must_use]
    pub fn CountWaysOfNE_Value( &self, aValue : Value ) -> Count
    {
        self.NumPermutations() - self.CountWaysOfEQ_Value(aValue)
    }

    #[must_use]
    pub fn CountWaysOfGE_Value( &self, aValue : Value ) -> Count
    {
        if aValue > self.Max()
        {
            Count::zero()
        }
        else
        {
            // The arrays are short enough that it is faster to do a linear search than to do a binary search.
            let entry = self.GetSurvivalDistribution().iter().find(|(value, _)| aValue <= **value);

            // Use "unwrap_or_else" rather than "expect" to avoid unconditional formatting of error string.
            entry.unwrap_or_else(|| unreachable!("CountWaysOfGE_Value has a bug!")).1.clone()
        }
    }

    #[must_use] pub fn CountWaysOfGT_Value( &self, aValue : Value ) -> Count
    {
        self.CountWaysOfGE_Value(aValue) - self.CountWaysOfEQ_Value(aValue)
    }

    // Chance functions just delegate to the Count functions.
    #[must_use] pub fn ChanceOfLT_Value   ( &self, aValue : Value ) -> Probability { self.ToProbability( &self.CountWaysOfLT_Value(aValue) ) }
    #[must_use] pub fn ChanceOfLE_Value   ( &self, aValue : Value ) -> Probability { self.ToProbability( &self.CountWaysOfLE_Value(aValue) ) }
    #[must_use] pub fn ChanceOfEQ_Value   ( &self, aValue : Value ) -> Probability { self.ToProbability( &self.CountWaysOfEQ_Value(aValue) ) }
    #[must_use] pub fn ChanceOfNE_Value   ( &self, aValue : Value ) -> Probability { self.ToProbability( &self.CountWaysOfNE_Value(aValue) ) }
    #[must_use] pub fn ChanceOfGE_Value   ( &self, aValue : Value ) -> Probability { self.ToProbability( &self.CountWaysOfGE_Value(aValue) ) }
    #[must_use] pub fn ChanceOfGT_Value   ( &self, aValue : Value ) -> Probability { self.ToProbability( &self.CountWaysOfGT_Value(aValue) ) }

    #[must_use] pub fn CountWaysToRollHighestValue( &self ) -> Count { self.CountWaysOfEQ_Value( self.Max() ) }
    #[must_use] pub fn CountWaysToRollLowestValue ( &self ) -> Count { self.CountWaysOfEQ_Value( self.Min() ) }

    #[must_use] pub fn ChanceToRollHighestValue( &self ) -> Probability { self.ToProbability( &self.CountWaysToRollHighestValue() ) }
    #[must_use] pub fn ChanceToRollLowestValue ( &self ) -> Probability { self.ToProbability( &self.CountWaysToRollLowestValue () ) }

    #[must_use]
    pub fn Clamp(&self, aNewMin : Value, aNewMax : Value) -> Die
    {
        Die::NewRaw( ClampDistribution(&self.distribution, aNewMin, aNewMax) )
    }

    #[must_use]
    pub fn ClampMin(&self, aNewMin : Value) -> Die
    {
        self.Clamp(aNewMin, Value::max_value())
    }

    #[must_use]
    pub fn ClampMax(&self, aNewMax : Value) -> Die
    {
        self.Clamp(Value::min_value(), aNewMax)
    }

    #[must_use]
    pub fn Half(&self) -> Die
    {
        let mut halvedDistribution = CountDistribution::with_capacity( self.distribution.len() );

        for (value, count) in &self.distribution
        {
            // Round towards 0.
            // TODO: Could add flag to control rounding towards/away from 0.
            let halvedValue = value / 2;

            let entry = halvedDistribution.entry(halvedValue).or_insert_with(Count::zero);
            *entry += count;
        }

        Die::NewRaw( halvedDistribution )
    }

    #[must_use]
    pub fn WeightedMerge( someDiceAndWeights : &[(&Die, Count)] ) -> Die
    {
        assert!( !someDiceAndWeights.is_empty(), "Can't do weighted merge with no dice!" );

        // TODO: It's not obvious what a good capacity value would be.
        let mut mergedCountDistribution = CountDistribution::new();

        // If trying to switch to a different bigint library, often they will be missing an LCM
        // function. In that case, we could replace the  LCM call with GCD, or implement it ourselves.
        // https://en.wikipedia.org/wiki/Least_common_multiple#Using_the_greatest_common_divisor
        let lcm = someDiceAndWeights.iter()
                                    .map(|&(die, _)| die.NumPermutations().clone())
                                    .reduce(num_integer::lcm)
                                    .unwrap();

        for (die, weight) in someDiceAndWeights.iter()
        {
            if weight.is_zero()
            {
                continue;
            }

            assert!(lcm.is_multiple_of(die.NumPermutations()));

            let scale = &lcm / die.NumPermutations();

            for (value, count) in &die.distribution
            {
                let weightedCount = count * &scale * weight;

                let entry = mergedCountDistribution.entry(*value).or_insert_with(Count::zero);
                *entry += weightedCount;
            }
        }

        Die::NewRaw( mergedCountDistribution )
    }

    #[must_use]
    pub fn NewRaw( mut aDistribution : CountDistribution ) -> Die
    {
        // We want to make sure the numbers we're dealing with are as
        // small as possible to reduce memory/performance costs.
        //
        // TODO: Maybe this is causing more harm than good? This was originally added
        //       to help avoid u64 overflow as much as possible, but now that we use
        //       num_bigint that's not as big of an issue anymore. Below are benchmark
        //       results when not reducing the distribution.
        //
        // $ critcmp initial new-feature -t 10
        // group                                      initial                                new-feature
        // -----                                      -------                                -----------
        // AttackSequence::"OneSneakAttack"::mean     1.25    220.5±3.06µs        ? ?/sec    1.00    176.2±1.68µs        ? ?/sec
        // AttackSequence::"TwoSneakAttack"::mean     1.23    555.5±5.53µs        ? ?/sec    1.00    453.5±4.42µs        ? ?/sec
        // Die::"Die::from(1..=1) * 10"::new          1.10   1037.4±9.85ns        ? ?/sec    1.00   942.7±14.06ns        ? ?/sec
        // Die::"Die::from(1..=1)"::GE_value          1.10     47.3±0.64ns        ? ?/sec    1.00     42.9±0.63ns        ? ?/sec
        // Die::"Die::from(1..=1)"::new               1.34    205.7±1.76ns        ? ?/sec    1.00    153.3±2.21ns        ? ?/sec
        // Die::"Die::from(1..=20) * 10"::NE_value    1.00     52.0±0.54ns        ? ?/sec    1.13     58.6±8.32ns        ? ?/sec
        // Die::"Die::from(1..=6)"::GE_value          1.11     49.3±0.41ns        ? ?/sec    1.00     44.5±0.35ns        ? ?/sec
        // Die::"Die::from(1..=6)"::new               1.22    769.5±9.36ns        ? ?/sec    1.00    632.0±6.34ns        ? ?/sec
        // Spell::"Fireball_C"::mean                  2.08     32.0±0.44µs        ? ?/sec    1.00     15.3±0.20µs        ? ?/sec
        // Spell::"Fireball_EV"::mean                 2.06     16.9±0.16µs        ? ?/sec    1.00      8.2±0.17µs        ? ?/sec
        // Spell::"Fireball_N"::mean                  2.14     37.3±0.48µs        ? ?/sec    1.00     17.4±0.22µs        ? ?/sec

        ReduceDistribution( &mut aDistribution );

        Die::NewRawUnreduced( aDistribution )
    }

    #[must_use]
    pub fn NewRawUnreduced( aDistribution : CountDistribution ) -> Die
    {
        AssertCountDistributionValid(&aDistribution);

        Die
        {
            cached_min                  : *GetMin( &aDistribution ).0,
            cached_max                  : *GetMax( &aDistribution ).0,
            cached_mean                 : once_cell::sync::OnceCell::new(),
            cached_numPermutations      : once_cell::sync::OnceCell::new(),
            cached_rngRange             : once_cell::sync::OnceCell::new(),
            cached_survivalDistribution : once_cell::sync::OnceCell::new(),
            distribution                : aDistribution,
        }
    }

    #[must_use]
    pub fn Advantage(&self) -> Die
    {
        let newDistribution = MergeCountDistributions( &self.distribution, &self.distribution, &cmp::max );

        Die::NewRaw( newDistribution )
    }

    #[must_use]
    pub fn SuperAdvantage(&self) -> Die
    {
        let regularAdvantageDistribution = MergeCountDistributions( &self.distribution, &self.distribution           , &cmp::max );
        let   superAdvantageDistribution = MergeCountDistributions( &self.distribution, &regularAdvantageDistribution, &cmp::max );

        Die::NewRaw( superAdvantageDistribution )
    }

    #[must_use]
    pub fn Disadvantage(&self) -> Die
    {
        let newDistribution = MergeCountDistributions( &self.distribution, &self.distribution, &cmp::min );

        Die::NewRaw( newDistribution )
    }

    #[must_use]
    pub fn RerollNumbers<ShouldRerollFn>(&self, shouldRerollFn : ShouldRerollFn ) -> Die
        where ShouldRerollFn: Fn(Value) -> bool
    {
        let MergeFn = |aLHS : Value, aRHS : Value|
        {
            if shouldRerollFn(aLHS)
            {
                // If we re-roll we must use the second value, whatever it is.
                aRHS
            }
            else
            {
                // Otherwise stick with the original roll.
                aLHS
            }
        };

        let newDistribution = MergeCountDistributions( &self.distribution, &self.distribution, &MergeFn );

        Die::NewRaw( newDistribution )
    }
}

// For creating a Die from a single value.
impl From<Value> for Die
{
    #[must_use]
    fn from( aValue : Value ) -> Self
    {
        // A die that only has 1 side.
        let distribution = flatmap!{ aValue => Count::from(1u32) };

        Die::NewRaw(distribution)
    }
}

// Overloads for a reference rather than a value.
impl From<&     Value> for Die { #[must_use] fn from( aValue : &    Value ) -> Self { Die::from(*aValue) } }
impl From<& mut Value> for Die { #[must_use] fn from( aValue : &mut Value ) -> Self { Die::from(*aValue) } }

// For creating a Die from a range of values.
impl From<std::ops::RangeInclusive<Value>> for Die
{
    #[must_use]
    fn from( aRange : std::ops::RangeInclusive<Value> ) -> Self
    {
        // Every value has the same chance of being rolled.
        let distribution : CountDistribution = aRange.map(|value| (value, Count::from(1u32))).collect();

        Die::NewRaw( distribution )
    }
}

//
// Only create the dice parser once per program execution.
//
// The parser is not a cheap object to create since it compiles a
// regex internally so we don't want to create it unnecessarily.
//
static GlobalDiceExpressionParser : once_cell::sync::OnceCell<parsing::DiceExpressionParser> = once_cell::sync::OnceCell::new();

// For creating a Die from a string.
impl FromStr for Die
{
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err>
    {
        let parser = GlobalDiceExpressionParser.get_or_init(||
        {
             parsing::DiceExpressionParser::new()
        });

        CleanupParserResult( parser.parse(MaxSizeNonZeroU8, s) )
    }
}

#[derive(Deserialize)]
#[serde(transparent)]
struct DeserializeData_Die
{
    diceStr : String
}

impl TryFrom<DeserializeData_Die> for Die
{
    type Error = String;

    fn try_from(data : DeserializeData_Die) -> Result<Self, Self::Error>
    {
        Die::from_str(&data.diceStr)
    }
}

// NOTE: This should match the structure of DeserializeData_Die.
pub fn GenerateRandomJSON_Die<R : Rng>( rng : &mut R ) -> String
{
    GenerateRandomDieHelper( rng, AllowStandaloneNumbers::Yes, AllowOnlyStandaloneNumbers::No, AllowSubtraction::Yes )
}

gen_boolean_enum!(AllowStandaloneNumbers);
gen_boolean_enum!(AllowOnlyStandaloneNumbers);
gen_boolean_enum!(AllowSubtraction);

fn GenerateRandomDieHelper<R : Rng>( rng                        : &mut R
                                   , allowStandaloneNumbers     : AllowStandaloneNumbers
                                   , allowOnlyStandaloneNumbers : AllowOnlyStandaloneNumbers
                                   , allowSubtraction           : AllowSubtraction ) -> String
{
    let numEntries = rng.gen_range(1..3);

    let mut result = String::new();
    result += "\"";

    let mut numDiceGenerated = 0;

    for i in 0..numEntries
    {
        let isFirst = i == 0;
        let isLast  = i == (numEntries - 1);

        let entry =
        {
            let allowOnlyStandaloneNumbers : bool = allowOnlyStandaloneNumbers.into();

            let mustGenerateDice = !allowOnlyStandaloneNumbers
                                && isLast
                                && (numDiceGenerated == 0);

            let shouldGenerateStandaloneNumber = allowStandaloneNumbers.into()
                                              && !mustGenerateDice
                                              && rng.gen();

            if shouldGenerateStandaloneNumber
            {
                let num = if isFirst { rng.gen_range(-5..6) } else { rng.gen_range(1..6) };

                format!("{}", num)
            }
            else
            {
                numDiceGenerated += 1;

                let numDice = rng.gen_range(1..3);
                let dieSize = [4, 6, 8, 10, 12].choose(rng).unwrap();

                format!("{}d{}", numDice, dieSize)
            }
        };

        if !isFirst
        {
            let shouldSubtract = allowSubtraction.into() && rng.gen();

            result += if shouldSubtract { " - " } else { " + " };
        }

        result += &entry;
    }

    result += "\"";

    result
}

pub fn GenerateRandomModifierDie<R : Rng>( rng : &mut R ) -> String
{
    GenerateRandomDieHelper( rng, AllowStandaloneNumbers::Yes, AllowOnlyStandaloneNumbers::Yes, AllowSubtraction::Yes )
}

pub fn GenerateRandomDamageDie<R : Rng>( rng : &mut R ) -> String
{
    GenerateRandomDieHelper( rng, AllowStandaloneNumbers::No, AllowOnlyStandaloneNumbers::No, AllowSubtraction::No )
}

//
// Reference issue for all the crazy macros below.
//
//     https://github.com/rust-lang/rust/issues/21188
//     https://github.com/rust-lang/rust/issues/44762
//     https://github.com/rust-lang/rust/blob/master/src/libcore/internal_macros.rs
//
// TODO: Possible way to reduce duplication?
//     https://www.reddit.com/r/rust/comments/iwij5i/blog_post_why_not_rust/g613c9e/
//

//
// Implement all other combinations of unary operator
// given that an implementation of "op &T" already exists.
//
macro_rules! forward_ref_unop
{
    // TODO: Figure out how to setup the Output type correct so the macro doesn't need that extra param.
    (impl $imp:ident, $method:ident for $t:ty, $o:ty) =>
    {
        // &*other is used to re-borrow the mutable ref into an immutable ref.
        impl     $imp for         $t { type Output = /*<$t as $imp>::Output*/$o; fn $method(self) -> Self::Output { $imp::$method(& self) } }
        impl<'a> $imp for &'a mut $t { type Output = /*<$t as $imp>::Output*/$o; fn $method(self) -> Self::Output { $imp::$method(&*self) } }
    }
}

//
// Implement all other combinations of binary operator
// given that an implementation of "&T op &U" already exists.
//
macro_rules! forward_ref_binop
{
    // TODO: Figure out how to setup the Output type correct so the macro doesn't need that extra param.
    (impl $imp:ident, $method:ident for $t:ty, $u:ty, $o:ty) =>
    {
        // &*other is used to re-borrow the mutable ref into an immutable ref.
        impl         $imp<        $u> for         $t { type Output = /*<$t as $imp<$u>>::Output*/$o; fn $method(self, other :         $u) -> Self::Output { $imp::$method(& self, & other) } } //      T op      U
        impl<    'u> $imp<&'u     $u> for         $t { type Output = /*<$t as $imp<$u>>::Output*/$o; fn $method(self, other : &'u     $u) -> Self::Output { $imp::$method(& self,   other) } } //      T op &    U
        impl<    'u> $imp<&'u mut $u> for         $t { type Output = /*<$t as $imp<$u>>::Output*/$o; fn $method(self, other : &'u mut $u) -> Self::Output { $imp::$method(& self, &*other) } } //      T op &mut U
        impl<'t    > $imp<        $u> for &'t     $t { type Output = /*<$t as $imp<$u>>::Output*/$o; fn $method(self, other :         $u) -> Self::Output { $imp::$method(  self, & other) } } // &    T op      U
        impl<'t, 'u> $imp<&'u mut $u> for &'t     $t { type Output = /*<$t as $imp<$u>>::Output*/$o; fn $method(self, other : &'u mut $u) -> Self::Output { $imp::$method(  self, &*other) } } // &    T op &mut U
        impl<'t    > $imp<        $u> for &'t mut $t { type Output = /*<$t as $imp<$u>>::Output*/$o; fn $method(self, other :         $u) -> Self::Output { $imp::$method(&*self, & other) } } // &mut T op      U
        impl<'t, 'u> $imp<&'u     $u> for &'t mut $t { type Output = /*<$t as $imp<$u>>::Output*/$o; fn $method(self, other : &'u     $u) -> Self::Output { $imp::$method(&*self,   other) } } // &mut T op &    U
        impl<'t, 'u> $imp<&'u mut $u> for &'t mut $t { type Output = /*<$t as $imp<$u>>::Output*/$o; fn $method(self, other : &'u mut $u) -> Self::Output { $imp::$method(&*self, &*other) } } // &mut T op &mut U
    }
}

//
// Implement all combinations of assignment operator given
// that an implementation of "&T op &U" already exists.
//
macro_rules! forward_ref_op_assign
{
    (impl $imp:ident, $method:ident for $t:ty, $u:ty, $helper_trait:ident::$helper_method:ident) =>
    {
        // Implement "x op= y" as "x = x op y".
        impl     $imp<        $u> for $t { fn $method(&mut self, other :         $u) { *self = <&mut $t as $helper_trait<        $u>>::$helper_method(self, other); } } // &mut T op=      U
        impl<'u> $imp<&'u     $u> for $t { fn $method(&mut self, other : &'u     $u) { *self = <&mut $t as $helper_trait<&'u     $u>>::$helper_method(self, other); } } // &mut T op= &    U
        impl<'u> $imp<&'u mut $u> for $t { fn $method(&mut self, other : &'u mut $u) { *self = <&mut $t as $helper_trait<&'u mut $u>>::$helper_method(self, other); } } // &mut T op= &mut U
    }
}

// Neg.
impl<'a> Neg for &'a Die
{
    type Output = Die;

    fn neg( self ) -> Die
    {
        Die::NewRaw( NegateDistribution(&self.distribution) )
    }
}

forward_ref_unop! { impl Neg, neg for Die, Die }

// Add.
impl<'a, 'b> Add<&'a Die  > for &'b Die   { type Output = Die; fn add( self, other : &Die   ) -> Die { Die::NewRaw(MergeCountDistributions(&self.distribution, &other.distribution, Value::add)) } }
impl<'a, 'b> Add<&'a Value> for &'b Die   { type Output = Die; fn add( self, other : &Value ) -> Die { self.add(Die::from(other)) } }
impl<'a, 'b> Add<&'a Die  > for &'b Value { type Output = Die; fn add( self, other : &Die   ) -> Die { Die::from(self).add(other) } }

forward_ref_binop! { impl Add, add for Die  , Die  , Die }
forward_ref_binop! { impl Add, add for Die  , Value, Die }
forward_ref_binop! { impl Add, add for Value, Die  , Die }

// Sub.
impl<'a, 'b> Sub<&'a Die  > for &'b Die   { type Output = Die; fn sub( self, other : &Die   ) -> Die { Die::NewRaw(MergeCountDistributions(&self.distribution, &other.distribution, Value::sub)) } }
impl<'a, 'b> Sub<&'a Value> for &'b Die   { type Output = Die; fn sub( self, other : &Value ) -> Die { self.sub(Die::from(other)) } }
impl<'a, 'b> Sub<&'a Die  > for &'b Value { type Output = Die; fn sub( self, other : &Die   ) -> Die { Die::from(self).sub(other) } }

forward_ref_binop! { impl Sub, sub for Die  , Die  , Die }
forward_ref_binop! { impl Sub, sub for Die  , Value, Die }
forward_ref_binop! { impl Sub, sub for Value, Die  , Die }

// Mul.
impl<'a, 'b> Mul<&'a usize> for &'b Die   { type Output = Die; fn mul( self, other : &usize ) -> Die { Die::NewRaw(MergeCountDistributions_Mul(&self.distribution, *other)) } }
impl<'a, 'b> Mul<&'a Die  > for &'b usize { type Output = Die; fn mul( self, other : &Die   ) -> Die { other.mul(self) } } // Multiplication is commutative, so just flip the arguments.

forward_ref_binop! { impl Mul, mul for Die  , usize, Die }
forward_ref_binop! { impl Mul, mul for usize, Die  , Die }

// AddAssign.
forward_ref_op_assign! { impl AddAssign, add_assign for Die, Die  , Add::add }
forward_ref_op_assign! { impl AddAssign, add_assign for Die, Value, Add::add }

// SubAssign.
forward_ref_op_assign! { impl SubAssign, sub_assign for Die, Die  , Sub::sub }
forward_ref_op_assign! { impl SubAssign, sub_assign for Die, Value, Sub::sub }

// MulAssign.
forward_ref_op_assign! { impl MulAssign, mul_assign for Die, usize, Mul::mul }

macro_rules! StaticGlobalDieObject
{
    ($name:ident, $value:expr) =>
    {
        pub fn $name() -> /*&'static Die*/ Die
        {
            static INSTANCE : once_cell::sync::OnceCell<Die> = once_cell::sync::OnceCell::new();

            INSTANCE.get_or_init(||
            {
                $value
            })

            // TODO: investigate removing this and returning a static reference instead!
            .clone()
        }
    }
}

// Common pattern in D&D 5e.
StaticGlobalDieObject!{ _1d20_adv     , _1d20().Advantage()               }
StaticGlobalDieObject!{ _1d20_disadv  , _1d20().Disadvantage()            }
StaticGlobalDieObject!{ _1d20_superAdv, _1d20().SuperAdvantage()          }
StaticGlobalDieObject!{ _1d20_lucky   , _1d20().RerollNumbers(|x| x == 1) }

pub fn _1d20_roll_style( aRollStyle : RollStyle ) -> Die
{
    match aRollStyle
    {
        RollStyle::Normal               => _1d20         (),
        RollStyle::Advantage            => _1d20_adv     (),
        RollStyle::Disadvantage         => _1d20_disadv  (),
        RollStyle::SuperAdvantage       => _1d20_superAdv(),
        RollStyle::Normal_HalflingLucky => _1d20_lucky   (),
    }
}

pub fn _1d20_plus_mod( aRollStyle : RollStyle, aModifier : Value ) -> Die
{
    _1d20_roll_style(aRollStyle) + aModifier
}

// Can only roll a 0.
StaticGlobalDieObject!{ _1d0, Die::from(0) }

// Basic unweighted dice.
StaticGlobalDieObject!{ _1d1 , Die::from(1..= 1) }
StaticGlobalDieObject!{ _1d2 , Die::from(1..= 2) }
StaticGlobalDieObject!{ _1d4 , Die::from(1..= 4) }
StaticGlobalDieObject!{ _1d6 , Die::from(1..= 6) }
StaticGlobalDieObject!{ _1d8 , Die::from(1..= 8) }
StaticGlobalDieObject!{ _1d10, Die::from(1..=10) }
StaticGlobalDieObject!{ _1d12, Die::from(1..=12) }
StaticGlobalDieObject!{ _1d20, Die::from(1..=20) }

// Basic negative weighted dice.
StaticGlobalDieObject!{ _1dNeg1 , Die::from(- 1..=-1) }
StaticGlobalDieObject!{ _1dNeg2 , Die::from(- 2..=-1) }
StaticGlobalDieObject!{ _1dNeg4 , Die::from(- 4..=-1) }
StaticGlobalDieObject!{ _1dNeg6 , Die::from(- 6..=-1) }
StaticGlobalDieObject!{ _1dNeg8 , Die::from(- 8..=-1) }
StaticGlobalDieObject!{ _1dNeg10, Die::from(-10..=-1) }
StaticGlobalDieObject!{ _1dNeg12, Die::from(-12..=-1) }
StaticGlobalDieObject!{ _1dNeg20, Die::from(-20..=-1) }

// Multiple basic dice.
StaticGlobalDieObject!{ _2d1 , _1d1()  *  2 }
StaticGlobalDieObject!{ _3d1 , _1d1()  *  3 }
StaticGlobalDieObject!{ _4d1 , _1d1()  *  4 }
StaticGlobalDieObject!{ _5d1 , _1d1()  *  5 }
StaticGlobalDieObject!{ _6d1 , _1d1()  *  6 }
StaticGlobalDieObject!{ _7d1 , _1d1()  *  7 }
StaticGlobalDieObject!{ _8d1 , _1d1()  *  8 }
StaticGlobalDieObject!{ _9d1 , _1d1()  *  9 }
StaticGlobalDieObject!{ _10d1, _1d1()  * 10 }

StaticGlobalDieObject!{ _2d2 , _1d2()  *  2 }
StaticGlobalDieObject!{ _3d2 , _1d2()  *  3 }
StaticGlobalDieObject!{ _4d2 , _1d2()  *  4 }
StaticGlobalDieObject!{ _5d2 , _1d2()  *  5 }
StaticGlobalDieObject!{ _6d2 , _1d2()  *  6 }
StaticGlobalDieObject!{ _7d2 , _1d2()  *  7 }
StaticGlobalDieObject!{ _8d2 , _1d2()  *  8 }
StaticGlobalDieObject!{ _9d2 , _1d2()  *  9 }
StaticGlobalDieObject!{ _10d2, _1d2()  * 10 }

StaticGlobalDieObject!{ _2d4 , _1d4()  *  2 }
StaticGlobalDieObject!{ _3d4 , _1d4()  *  3 }
StaticGlobalDieObject!{ _4d4 , _1d4()  *  4 }
StaticGlobalDieObject!{ _5d4 , _1d4()  *  5 }
StaticGlobalDieObject!{ _6d4 , _1d4()  *  6 }
StaticGlobalDieObject!{ _7d4 , _1d4()  *  7 }
StaticGlobalDieObject!{ _8d4 , _1d4()  *  8 }
StaticGlobalDieObject!{ _9d4 , _1d4()  *  9 }
StaticGlobalDieObject!{ _10d4, _1d4()  * 10 }

StaticGlobalDieObject!{ _2d6 , _1d6()  *  2 }
StaticGlobalDieObject!{ _3d6 , _1d6()  *  3 }
StaticGlobalDieObject!{ _4d6 , _1d6()  *  4 }
StaticGlobalDieObject!{ _5d6 , _1d6()  *  5 }
StaticGlobalDieObject!{ _6d6 , _1d6()  *  6 }
StaticGlobalDieObject!{ _7d6 , _1d6()  *  7 }
StaticGlobalDieObject!{ _8d6 , _1d6()  *  8 }
StaticGlobalDieObject!{ _9d6 , _1d6()  *  9 }
StaticGlobalDieObject!{ _10d6, _1d6()  * 10 }

StaticGlobalDieObject!{ _2d8 , _1d8()  *  2 }
StaticGlobalDieObject!{ _3d8 , _1d8()  *  3 }
StaticGlobalDieObject!{ _4d8 , _1d8()  *  4 }
StaticGlobalDieObject!{ _5d8 , _1d8()  *  5 }
StaticGlobalDieObject!{ _6d8 , _1d8()  *  6 }
StaticGlobalDieObject!{ _7d8 , _1d8()  *  7 }
StaticGlobalDieObject!{ _8d8 , _1d8()  *  8 }
StaticGlobalDieObject!{ _9d8 , _1d8()  *  9 }
StaticGlobalDieObject!{ _10d8, _1d8()  * 10 }

StaticGlobalDieObject!{ _2d10 , _1d10() *  2 }
StaticGlobalDieObject!{ _3d10 , _1d10() *  3 }
StaticGlobalDieObject!{ _4d10 , _1d10() *  4 }
StaticGlobalDieObject!{ _5d10 , _1d10() *  5 }
StaticGlobalDieObject!{ _6d10 , _1d10() *  6 }
StaticGlobalDieObject!{ _7d10 , _1d10() *  7 }
StaticGlobalDieObject!{ _8d10 , _1d10() *  8 }
StaticGlobalDieObject!{ _9d10 , _1d10() *  9 }
StaticGlobalDieObject!{ _10d10, _1d10() * 10 }

StaticGlobalDieObject!{ _2d12 , _1d12() *  2 }
StaticGlobalDieObject!{ _3d12 , _1d12() *  3 }
StaticGlobalDieObject!{ _4d12 , _1d12() *  4 }
StaticGlobalDieObject!{ _5d12 , _1d12() *  5 }
StaticGlobalDieObject!{ _6d12 , _1d12() *  6 }
StaticGlobalDieObject!{ _7d12 , _1d12() *  7 }
StaticGlobalDieObject!{ _8d12 , _1d12() *  8 }
StaticGlobalDieObject!{ _9d12 , _1d12() *  9 }
StaticGlobalDieObject!{ _10d12, _1d12() * 10 }

StaticGlobalDieObject!{ _2d20 , _1d20() *  2 }
StaticGlobalDieObject!{ _3d20 , _1d20() *  3 }
StaticGlobalDieObject!{ _4d20 , _1d20() *  4 }
StaticGlobalDieObject!{ _5d20 , _1d20() *  5 }
StaticGlobalDieObject!{ _6d20 , _1d20() *  6 }
StaticGlobalDieObject!{ _7d20 , _1d20() *  7 }
StaticGlobalDieObject!{ _8d20 , _1d20() *  8 }
StaticGlobalDieObject!{ _9d20 , _1d20() *  9 }
StaticGlobalDieObject!{ _10d20, _1d20() * 10 }

#[cfg(test)]
mod tests
{
    use super::*;
    use super::IsCountValid;
    use more_asserts::{assert_gt, assert_lt, assert_le, assert_ge};

    // Check all the guarantees that any die will have.
    fn TestArbitraryDie( aName : &str, aDie : &Die )
    {
        println!();
        println!( "Testing [{}]", aName );

        // Test basic properties.
        assert!( aDie.Min() <= aDie.Max(), "Min [{}] should be <= Max [{}]", aDie.Min(), aDie.Max() );

        let mean = aDie.Mean();
        assert!( (mean >= MeanValue(aDie.Min() as f64)) && (mean <= MeanValue(aDie.Max() as f64)), "Mean value [{}] not in [{}, {}]", mean, aDie.Min(), aDie.Max() );

        assert!( IsCountValid(aDie.NumPermutations()), "Invalid number of permutations [{}]", aDie.NumPermutations() );

        // Test distribution.
        let countSum : Count = aDie.distribution
                                   .values()
                                   .inspect( |count| assert!(IsCountValid(*count), "Count [{}] is not valid", count) )
                                   .sum();

        assert!( countSum == *aDie.NumPermutations(), "Count sum [{}] should be [{}]", countSum, aDie.NumPermutations() );

        // Check rolling.
        {
            let numIterations = 100_000;

            let RollFunction = ||
            {
                aDie.Roll()
            };

            let expectedMean = aDie.Mean();
            let expectedProbabilityDistribution = aDie.BuildProbabilityDistribution();

            crate::unit_test_utils::TestRolling(RollFunction, numIterations, expectedMean, &expectedProbabilityDistribution);
        }

        {
            let roll = aDie.RollPriv( &Count::zero() );
            assert_eq!( roll, aDie.Max() );
        }

        {
            let roll = aDie.RollPriv( &(aDie.NumPermutations() - 1u32) );
            assert_eq!( roll, aDie.Min() );
        }

        // Check probability queries.
        {
            let value = aDie.Min() - 1;

            assert_eq!( aDie.CountWaysOfLT_Value(value), Count::zero() );
            assert_eq!( aDie.CountWaysOfLE_Value(value), Count::zero() );
            assert_eq!( aDie.CountWaysOfEQ_Value(value), Count::zero() );
            assert_eq!( aDie.CountWaysOfNE_Value(value), *aDie.NumPermutations() );
            assert_eq!( aDie.CountWaysOfGE_Value(value), *aDie.NumPermutations() );
            assert_eq!( aDie.CountWaysOfGT_Value(value), *aDie.NumPermutations() );
        }

        {
            let value = aDie.Min();

            assert_eq!( aDie.CountWaysOfLT_Value(value), Count::zero() );
            assert_gt!( aDie.CountWaysOfLE_Value(value), Count::zero() );
            assert_gt!( aDie.CountWaysOfEQ_Value(value), Count::zero() );
            assert_lt!( aDie.CountWaysOfNE_Value(value), *aDie.NumPermutations() );
            assert_eq!( aDie.CountWaysOfGE_Value(value), *aDie.NumPermutations() );
            assert_lt!( aDie.CountWaysOfGT_Value(value), *aDie.NumPermutations() );
        }

        {
            let value = aDie.Max();

            assert_lt!( aDie.CountWaysOfLT_Value(value), *aDie.NumPermutations() );
            assert_eq!( aDie.CountWaysOfLE_Value(value), *aDie.NumPermutations() );
            assert_gt!( aDie.CountWaysOfEQ_Value(value), Count::zero() );
            assert_lt!( aDie.CountWaysOfNE_Value(value), *aDie.NumPermutations() );
            assert_gt!( aDie.CountWaysOfGE_Value(value), Count::zero() );
            assert_eq!( aDie.CountWaysOfGT_Value(value), Count::zero() );
        }

        {
            let value = aDie.Max() + 1;

            assert_eq!( aDie.CountWaysOfLT_Value(value), *aDie.NumPermutations() );
            assert_eq!( aDie.CountWaysOfLE_Value(value), *aDie.NumPermutations() );
            assert_eq!( aDie.CountWaysOfEQ_Value(value), Count::zero() );
            assert_eq!( aDie.CountWaysOfNE_Value(value), *aDie.NumPermutations() );
            assert_eq!( aDie.CountWaysOfGE_Value(value), Count::zero() );
            assert_eq!( aDie.CountWaysOfGT_Value(value), Count::zero() );
        }

        for value in (aDie.Min() - 1) ..= (aDie.Max() + 1)
        {
            // Check counts.
            let numLT = aDie.CountWaysOfLT_Value(value);
            let numLE = aDie.CountWaysOfLE_Value(value);
            let numEQ = aDie.CountWaysOfEQ_Value(value);
            let numNE = aDie.CountWaysOfNE_Value(value);
            let numGE = aDie.CountWaysOfGE_Value(value);
            let numGT = aDie.CountWaysOfGT_Value(value);

            assert_eq!( &numEQ + &numNE, *aDie.NumPermutations() );
            assert_eq!( &numLT + &numGE, *aDie.NumPermutations() );
            assert_eq!( &numLE + &numGT, *aDie.NumPermutations() );

            assert_eq!( &numLT + &numEQ + &numGT, *aDie.NumPermutations() );

            assert_eq!( &numLT + &numEQ, numLE );
            assert_eq!( &numGT + &numEQ, numGE );

            // Check probabilities.
            let chanceLT = aDie.ChanceOfLT_Value(value);
            let chanceLE = aDie.ChanceOfLE_Value(value);
            let chanceEQ = aDie.ChanceOfEQ_Value(value);
            let chanceNE = aDie.ChanceOfNE_Value(value);
            let chanceGE = aDie.ChanceOfGE_Value(value);
            let chanceGT = aDie.ChanceOfGT_Value(value);

            assert_eq!( chanceLT, aDie.ToProbability(&numLT) );
            assert_eq!( chanceLE, aDie.ToProbability(&numLE) );
            assert_eq!( chanceEQ, aDie.ToProbability(&numEQ) );
            assert_eq!( chanceNE, aDie.ToProbability(&numNE) );
            assert_eq!( chanceGE, aDie.ToProbability(&numGE) );
            assert_eq!( chanceGT, aDie.ToProbability(&numGT) );

            // Check relative to next/previous value.
            let oneLower  = value - 1;
            let oneHigher = value + 1;

            let oneLowerNumLT = aDie.CountWaysOfLT_Value(oneLower);
            let oneLowerNumLE = aDie.CountWaysOfLE_Value(oneLower);
            let oneLowerNumGE = aDie.CountWaysOfGE_Value(oneLower);
            let oneLowerNumGT = aDie.CountWaysOfGT_Value(oneLower);

            let oneHigherNumLT = aDie.CountWaysOfLT_Value(oneHigher);
            let oneHigherNumLE = aDie.CountWaysOfLE_Value(oneHigher);
            let oneHigherNumGE = aDie.CountWaysOfGE_Value(oneHigher);
            let oneHigherNumGT = aDie.CountWaysOfGT_Value(oneHigher);

            // Should be monotonically decreasing (but not necessarily strictly decreasing).
            assert_le!( oneLowerNumLT, numLT          );
            assert_le!( oneLowerNumLE, numLE          );
            assert_le!( numLT        , oneHigherNumLT );
            assert_le!( numLE        , oneHigherNumLE );

            // Should be monotonically increasing (but not necessarily strictly increasing).
            assert_ge!( oneLowerNumGT, numGT          );
            assert_ge!( oneLowerNumGE, numGE          );
            assert_ge!( numGT        , oneHigherNumGT );
            assert_ge!( numGE        , oneHigherNumGE );
        }

        // Check probability queries.
        {
            let cumulativeDistribution = aDie.BuildCumulativeDistribution();
            let   survivalDistribution = aDie.GetSurvivalDistribution();

            AssertCountDistributionValid( &cumulativeDistribution );
            AssertCountDistributionValid(    survivalDistribution );

            let cumulativeProbabilityDistribution = aDie.BuildCumulativeProbabilityDistribution();
            let   survivalProbabilityDistribution = aDie.BuildSurvivalProbabilityDistribution();

            AssertProbabilityDistributionValid( &cumulativeProbabilityDistribution );
            AssertProbabilityDistributionValid( &  survivalProbabilityDistribution );

            assert_gt!( *GetMin(&cumulativeDistribution           ).1, Count::zero() ); assert_eq!(  GetMax(&cumulativeDistribution           ).1, aDie.NumPermutations() );
            assert_gt!( *GetMin(&cumulativeProbabilityDistribution).1, 0.0           ); assert_eq!( *GetMax(&cumulativeProbabilityDistribution).1, 1.0                    );

            assert_eq!(  GetMin( survivalDistribution           ).1, aDie.NumPermutations() ); assert_gt!( *GetMax( survivalDistribution           ).1, Count::zero() );
            assert_eq!( *GetMin(&survivalProbabilityDistribution).1, 1.0                    ); assert_gt!( *GetMax(&survivalProbabilityDistribution).1, 0.0           );

            for (value, count) in cumulativeDistribution           .iter() { assert_eq!(*count, aDie.CountWaysOfLE_Value(*value)); }
            for (value, count) in cumulativeProbabilityDistribution.iter() { assert_eq!(*count, aDie.   ChanceOfLE_Value(*value)); }

            for (value, count) in survivalDistribution           .iter() { assert_eq!(*count, aDie.CountWaysOfGE_Value(*value)); }
            for (value, count) in survivalProbabilityDistribution.iter() { assert_eq!(*count, aDie.   ChanceOfGE_Value(*value)); }
        }
    }

    // Check that the basic properties of the die are as expected.
    fn TestArbitraryDieMore( aName : &str, aDie : &Die, aMin : Value, aMax : Value, aMean : MeanValue )
    {
        // Test global properties.
        TestArbitraryDie( aName, aDie );

        // Test basic properties.
        assert_eq!( aDie.Min (), aMin  );
        assert_eq!( aDie.Max (), aMax  );
        assert_eq!( aDie.Mean(), aMean );
    }

    // Check that all properties of the die are as expected.
    fn TestArbitraryDieMost( aName : &str, aDie : &Die, aMin : Value, aMax : Value, aMean : MeanValue, aCountDistribution : &CountDistribution )
    {
        TestArbitraryDieMore( aName, aDie, aMin, aMax, aMean );

        assert_eq!(&aDie.distribution, aCountDistribution);
    }

    #[test]
    fn RollPriv()
    {
        // Uniform die.
        {
            let d = _1d6();

            assert_eq!( d.RollPriv(&Count::from(0u32)), 6 );
            assert_eq!( d.RollPriv(&Count::from(1u32)), 5 );
            assert_eq!( d.RollPriv(&Count::from(2u32)), 4 );
            assert_eq!( d.RollPriv(&Count::from(3u32)), 3 );
            assert_eq!( d.RollPriv(&Count::from(4u32)), 2 );
            assert_eq!( d.RollPriv(&Count::from(5u32)), 1 );
        }

        // Skewed die.
        {
            let d = _1d4().Advantage();

            assert_eq!( d.RollPriv(&Count::from( 0u32)), 4 );
            assert_eq!( d.RollPriv(&Count::from( 1u32)), 4 );
            assert_eq!( d.RollPriv(&Count::from( 2u32)), 4 );
            assert_eq!( d.RollPriv(&Count::from( 3u32)), 4 );
            assert_eq!( d.RollPriv(&Count::from( 4u32)), 4 );
            assert_eq!( d.RollPriv(&Count::from( 5u32)), 4 );
            assert_eq!( d.RollPriv(&Count::from( 6u32)), 4 );
            assert_eq!( d.RollPriv(&Count::from( 7u32)), 3 );
            assert_eq!( d.RollPriv(&Count::from( 8u32)), 3 );
            assert_eq!( d.RollPriv(&Count::from( 9u32)), 3 );
            assert_eq!( d.RollPriv(&Count::from(10u32)), 3 );
            assert_eq!( d.RollPriv(&Count::from(11u32)), 3 );
            assert_eq!( d.RollPriv(&Count::from(12u32)), 2 );
            assert_eq!( d.RollPriv(&Count::from(13u32)), 2 );
            assert_eq!( d.RollPriv(&Count::from(14u32)), 2 );
            assert_eq!( d.RollPriv(&Count::from(15u32)), 1 );
        }

        // Gapped die.
        {
            let d = Die::NewRaw(flatmap!
            {
                0 => Count::from(3u32),
                3 => Count::from(1u32),
                6 => Count::from(4u32),
                8 => Count::from(2u32),
            });

            assert_eq!( d.RollPriv(&Count::from( 0u32)), 8 );
            assert_eq!( d.RollPriv(&Count::from( 1u32)), 8 );
            assert_eq!( d.RollPriv(&Count::from( 2u32)), 6 );
            assert_eq!( d.RollPriv(&Count::from( 3u32)), 6 );
            assert_eq!( d.RollPriv(&Count::from( 4u32)), 6 );
            assert_eq!( d.RollPriv(&Count::from( 5u32)), 6 );
            assert_eq!( d.RollPriv(&Count::from( 6u32)), 3 );
            assert_eq!( d.RollPriv(&Count::from( 7u32)), 0 );
            assert_eq!( d.RollPriv(&Count::from( 8u32)), 0 );
            assert_eq!( d.RollPriv(&Count::from( 9u32)), 0 );
        }
    }

    #[test]
    fn RollAgainstDie()
    {
        // 1 of each die.
        {
            let die1 = _1d6();
            let die2 = _1d8();

            assert_eq!( die1.CountWaysOfLT_Die(&die2), Count::from(27u32) ); assert_eq!( die1.ChanceOfLT_Die(&die2), 27.0 / 48.0 );
            assert_eq!( die1.CountWaysOfLE_Die(&die2), Count::from(33u32) ); assert_eq!( die1.ChanceOfLE_Die(&die2), 33.0 / 48.0 );
            assert_eq!( die1.CountWaysOfEQ_Die(&die2), Count::from( 6u32) ); assert_eq!( die1.ChanceOfEQ_Die(&die2),  6.0 / 48.0 );
            assert_eq!( die1.CountWaysOfNE_Die(&die2), Count::from(42u32) ); assert_eq!( die1.ChanceOfNE_Die(&die2), 42.0 / 48.0 );
            assert_eq!( die1.CountWaysOfGE_Die(&die2), Count::from(21u32) ); assert_eq!( die1.ChanceOfGE_Die(&die2), 21.0 / 48.0 );
            assert_eq!( die1.CountWaysOfGT_Die(&die2), Count::from(15u32) ); assert_eq!( die1.ChanceOfGT_Die(&die2), 15.0 / 48.0 );

            assert_eq!( die2.CountWaysOfLT_Die(&die1), Count::from(15u32) ); assert_eq!( die2.ChanceOfLT_Die(&die1), 15.0 / 48.0 );
            assert_eq!( die2.CountWaysOfLE_Die(&die1), Count::from(21u32) ); assert_eq!( die2.ChanceOfLE_Die(&die1), 21.0 / 48.0 );
            assert_eq!( die2.CountWaysOfEQ_Die(&die1), Count::from( 6u32) ); assert_eq!( die2.ChanceOfEQ_Die(&die1),  6.0 / 48.0 );
            assert_eq!( die2.CountWaysOfNE_Die(&die1), Count::from(42u32) ); assert_eq!( die2.ChanceOfNE_Die(&die1), 42.0 / 48.0 );
            assert_eq!( die2.CountWaysOfGE_Die(&die1), Count::from(33u32) ); assert_eq!( die2.ChanceOfGE_Die(&die1), 33.0 / 48.0 );
            assert_eq!( die2.CountWaysOfGT_Die(&die1), Count::from(27u32) ); assert_eq!( die2.ChanceOfGT_Die(&die1), 27.0 / 48.0 );
        }

        // 2 of each die.
        {
            let die1 = _2d4();
            let die2 = _2d6();

            assert_eq!( die1.CountWaysOfLT_Die(&die2), Count::from(398u32) ); assert_eq!( die1.ChanceOfLT_Die(&die2), 398.0 / 576.0 );
            assert_eq!( die1.CountWaysOfLE_Die(&die2), Count::from(460u32) ); assert_eq!( die1.ChanceOfLE_Die(&die2), 460.0 / 576.0 );
            assert_eq!( die1.CountWaysOfEQ_Die(&die2), Count::from( 62u32) ); assert_eq!( die1.ChanceOfEQ_Die(&die2),  62.0 / 576.0 );
            assert_eq!( die1.CountWaysOfNE_Die(&die2), Count::from(514u32) ); assert_eq!( die1.ChanceOfNE_Die(&die2), 514.0 / 576.0 );
            assert_eq!( die1.CountWaysOfGE_Die(&die2), Count::from(178u32) ); assert_eq!( die1.ChanceOfGE_Die(&die2), 178.0 / 576.0 );
            assert_eq!( die1.CountWaysOfGT_Die(&die2), Count::from(116u32) ); assert_eq!( die1.ChanceOfGT_Die(&die2), 116.0 / 576.0 );

            assert_eq!( die2.CountWaysOfLT_Die(&die1), Count::from(116u32) ); assert_eq!( die2.ChanceOfLT_Die(&die1), 116.0 / 576.0 );
            assert_eq!( die2.CountWaysOfLE_Die(&die1), Count::from(178u32) ); assert_eq!( die2.ChanceOfLE_Die(&die1), 178.0 / 576.0 );
            assert_eq!( die2.CountWaysOfEQ_Die(&die1), Count::from( 62u32) ); assert_eq!( die2.ChanceOfEQ_Die(&die1),  62.0 / 576.0 );
            assert_eq!( die2.CountWaysOfNE_Die(&die1), Count::from(514u32) ); assert_eq!( die2.ChanceOfNE_Die(&die1), 514.0 / 576.0 );
            assert_eq!( die2.CountWaysOfGE_Die(&die1), Count::from(460u32) ); assert_eq!( die2.ChanceOfGE_Die(&die1), 460.0 / 576.0 );
            assert_eq!( die2.CountWaysOfGT_Die(&die1), Count::from(398u32) ); assert_eq!( die2.ChanceOfGT_Die(&die1), 398.0 / 576.0 );
        }

        // Check advantage.
        {
            let die1 = _1d4().Advantage();
            let die2 = _2d6();

            assert_eq!( die1.CountWaysOfLT_Die(&die2), Count::from(516u32) ); assert_eq!( die1.ChanceOfLT_Die(&die2), 516.0 / 576.0 );
            assert_eq!( die1.CountWaysOfLE_Die(&die2), Count::from(550u32) ); assert_eq!( die1.ChanceOfLE_Die(&die2), 550.0 / 576.0 );
            assert_eq!( die1.CountWaysOfEQ_Die(&die2), Count::from( 34u32) ); assert_eq!( die1.ChanceOfEQ_Die(&die2),  34.0 / 576.0 );
            assert_eq!( die1.CountWaysOfNE_Die(&die2), Count::from(542u32) ); assert_eq!( die1.ChanceOfNE_Die(&die2), 542.0 / 576.0 );
            assert_eq!( die1.CountWaysOfGE_Die(&die2), Count::from( 60u32) ); assert_eq!( die1.ChanceOfGE_Die(&die2),  60.0 / 576.0 );
            assert_eq!( die1.CountWaysOfGT_Die(&die2), Count::from( 26u32) ); assert_eq!( die1.ChanceOfGT_Die(&die2),  26.0 / 576.0 );

            assert_eq!( die2.CountWaysOfLT_Die(&die1), Count::from( 26u32) ); assert_eq!( die2.ChanceOfLT_Die(&die1),  26.0 / 576.0 );
            assert_eq!( die2.CountWaysOfLE_Die(&die1), Count::from( 60u32) ); assert_eq!( die2.ChanceOfLE_Die(&die1),  60.0 / 576.0 );
            assert_eq!( die2.CountWaysOfEQ_Die(&die1), Count::from( 34u32) ); assert_eq!( die2.ChanceOfEQ_Die(&die1),  34.0 / 576.0 );
            assert_eq!( die2.CountWaysOfNE_Die(&die1), Count::from(542u32) ); assert_eq!( die2.ChanceOfNE_Die(&die1), 542.0 / 576.0 );
            assert_eq!( die2.CountWaysOfGE_Die(&die1), Count::from(550u32) ); assert_eq!( die2.ChanceOfGE_Die(&die1), 550.0 / 576.0 );
            assert_eq!( die2.CountWaysOfGT_Die(&die1), Count::from(516u32) ); assert_eq!( die2.ChanceOfGT_Die(&die1), 516.0 / 576.0 );
        }
    }

    #[test]
    fn ReducedDie()
    {
        // Basic case.
        {
            let die = Die::NewRaw(flatmap!{ 1 => Count::from(3u32),
                                            2 => Count::from(3u32),
                                            3 => Count::from(3u32),
                                            4 => Count::from(3u32), });

            assert_eq!( *die.GetDistribution(), flatmap!
            {
                1 => Count::from(1u32),
                2 => Count::from(1u32),
                3 => Count::from(1u32),
                4 => Count::from(1u32),
            });
        }

        // Complex case.
        {
            let die = Die::NewRaw(flatmap!{ 1 => Count::from(22u32),
                                            2 => Count::from(33u32),
                                            3 => Count::from(55u32),
                                            4 => Count::from(77u32), });

            assert_eq!( *die.GetDistribution(), flatmap!
            {
                1 => Count::from(2u32),
                2 => Count::from(3u32),
                3 => Count::from(5u32),
                4 => Count::from(7u32),
            });
        }

        // Can't reduce.
        {
            let die = Die::NewRaw(flatmap!{ 1 => Count::from( 5u32),
                                            2 => Count::from( 4u32),
                                            3 => Count::from( 8u32),
                                            4 => Count::from(12u32), });

            assert_eq!( *die.GetDistribution(), flatmap!
            {
                1 => Count::from( 5u32),
                2 => Count::from( 4u32),
                3 => Count::from( 8u32),
                4 => Count::from(12u32),
            });
        }

        // Unreduced.
        {
            let die = Die::NewRawUnreduced(flatmap!{ 1 => Count::from(3u32),
                                                     2 => Count::from(3u32),
                                                     3 => Count::from(3u32),
                                                     4 => Count::from(3u32), });

            assert_eq!( *die.GetDistribution(), flatmap!
            {
                1 => Count::from(3u32),
                2 => Count::from(3u32),
                3 => Count::from(3u32),
                4 => Count::from(3u32),
            });
        }
    }

    #[test]
    fn FromValue()
    {
        // Creation from a literal.
        let _ = Die::from(     1);
        let _ = Die::from(&    1);
        let _ = Die::from(&mut 1);

        // Creation from a variable.
        let mut val = 1;
        let _ = Die::from(     val);
        let _ = Die::from(&    val);
        let _ = Die::from(&mut val);
    }

    #[test]
    fn FromRange()
    {
        // Creation from a literal.
        let _ = Die::from(1..=5);

        // Creation from a variable.
        let val = 1..=5;
        let _ = Die::from(val);
    }

    #[test]
    #[should_panic(expected = "Cannot reduce an empty CountDistribution!")]
    fn FromRange_Empty()
    {
        let _ = Die::from(1..=0);
    }

    #[test]
    fn FromStr()
    {
        // Successful parse.
        let d1 = "5d4".parse::<Die>().unwrap();
        let d2 = Die::from_str("5d4").unwrap();

        assert_eq!(d1, d2);
        assert_eq!(d1, _5d4());

        let d3 = Die::from_str("0 + 1d2 - 3d4 + 5d6 - 7d8 + 9d10 - 11").unwrap();

        assert_eq!(d3, 0 + _1d2() - _3d4() + _5d6() - _7d8() + _9d10() - 11);

        assert_eq!(Die::from_str("0").unwrap(), _1d0());
        assert_eq!(Die::from_str("6").unwrap(), _6d1());
        assert_eq!(Die::from_str("-1").unwrap(), _1dNeg1());

        // Failed parse.
        assert!( Die::from_str("d4"     ).is_err() );
        assert!( Die::from_str("0d4"    ).is_err() );
        assert!( Die::from_str("5d0"    ).is_err() );
        assert!( Die::from_str("1d4 +"  ).is_err() );
        assert!( Die::from_str("3 * 1d4").is_err() );
    }

    #[test]
    fn Deserialize()
    {
        // Authored input.
        let validStrings =
        [
            ( _1d2() - _3d4()     , r#""1d2 - 3d4""#      ),
            ( Die::from(-10)      , r#""-10""#            ),
            ( Die::from(0)        , r#""0""#              ),
            ( _3d10()             , r#""3d10""#           ),
            ( _2d6() + _1d20() - 2, r#""2d6 + 1d20 - 2""# ),
        ];

        // We already test dice parsing in many other places.
        let invalidStrings =
        [
        ];

        crate::unit_test_utils::TestDeserializeFromJSON( &validStrings, &invalidStrings );

        // Random input.
        let mut rng = rand::thread_rng();
        crate::unit_test_utils::TestGenerateRandomJSON::<_, Die>( ||{ GenerateRandomJSON_Die(&mut rng) } );

        // Parsing error.
        assert_eq!( serde_json::from_str::<Die>(r#""1000d1000""#).unwrap_err().to_string(), "\"1000\" is too large, the largest supported number is \"50\"" );
    }

    #[test]
    fn AdvantageAndDisadvantage()
    {
        // Advantage/disadvantage does not distribute.
        {
            // This is rolling 2d20 twice, and taking the highest sum.
            let die1 =_2d20().Advantage();

            // This is rolling two d20s, each with advantage, and summing them.
            let die2 = _1d20().Advantage() * 2;

            assert_ne!(die1, die2);
        }

        //
        // Linear dice are checked in other tests.
        //

        // Check weighted dice.
        {
            let weightedDie = Die::NewRaw(flatmap!{ 1 => Count::from(3u32),
                                                    2 => Count::from(2u32),
                                                    3 => Count::from(1u32), });
            // Advantage.
            {
                let calculatedAdvantageDie = weightedDie.Advantage();

                let correctAdvantageDie = Die::NewRaw(flatmap!{ 1 => Count::from( 9u32),
                                                                2 => Count::from(16u32),
                                                                3 => Count::from(11u32), });

                assert_eq!(calculatedAdvantageDie, correctAdvantageDie);
            }

            // Disadvantage.
            {
                let calculatedDisadvantageDie = weightedDie.Disadvantage();

                let correctDisadvantageDie = Die::NewRaw(flatmap!{ 1 => Count::from(27u32),
                                                                   2 => Count::from( 8u32),
                                                                   3 => Count::from( 1u32), });

                assert_eq!(calculatedDisadvantageDie, correctDisadvantageDie);
            }
        }
    }

    #[test]
    fn Operations()
    {
        // Test that all the trait implementations exist and are correct.
        {
            // Hard-coded subtraction answer, since it doesn't result in a "normal" die.
            let subRes = Die::NewRaw(flatmap!{-2 => Count::from( 1u32),
                                              -1 => Count::from( 3u32),
                                               0 => Count::from( 6u32),
                                               1 => Count::from(10u32),
                                               2 => Count::from(12u32),
                                               3 => Count::from(12u32),
                                               4 => Count::from(10u32),
                                               5 => Count::from( 6u32),
                                               6 => Count::from( 3u32),
                                               7 => Count::from( 1u32),});
            // Neg.
            {
                assert_eq!( -     _1d4(), _1dNeg4() );
                assert_eq!( -&    _1d6(), _1dNeg6() );
                assert_eq!( -&mut _1d8(), _1dNeg8() );
            }

            // Add.
            {
                assert_eq!(      _2d2()    +      _5d2()   , _7d2() ); // Die + Die
                assert_eq!(      _2d2()    + &    _5d2()   , _7d2() );
                assert_eq!(      _2d2()    + &mut _5d2()   , _7d2() );
                assert_eq!( &    _1d6()    +      _2d6()   , _3d6() );
                assert_eq!( &    _1d6()    + &    _2d6()   , _3d6() );
                assert_eq!( &    _1d6()    + &mut _2d6()   , _3d6() );
                assert_eq!( &mut _3d8()    +      _4d8()   , _7d8() );
                assert_eq!( &mut _3d8()    + &    _4d8()   , _7d8() );
                assert_eq!( &mut _3d8()    + &mut _4d8()   , _7d8() );

                assert_eq!(      _1dNeg2() +      3        , _1d2() ); // Die + Value
                assert_eq!(      _1dNeg2() + &    3        , _1d2() );
                assert_eq!(      _1dNeg2() + &mut 3        , _1d2() );
                assert_eq!( &    _1dNeg6() +      7        , _1d6() );
                assert_eq!( &    _1dNeg6() + &    7        , _1d6() );
                assert_eq!( &    _1dNeg6() + &mut 7        , _1d6() );
                assert_eq!( &mut _1dNeg8() +      9        , _1d8() );
                assert_eq!( &mut _1dNeg8() + &    9        , _1d8() );
                assert_eq!( &mut _1dNeg8() + &mut 9        , _1d8() );

                assert_eq!(      3         +      _1dNeg2(), _1d2() ); // Value + Die
                assert_eq!(      3         + &    _1dNeg2(), _1d2() );
                assert_eq!(      3         + &mut _1dNeg2(), _1d2() );
                assert_eq!( &    7         +      _1dNeg6(), _1d6() );
                assert_eq!( &    7         + &    _1dNeg6(), _1d6() );
                assert_eq!( &    7         + &mut _1dNeg6(), _1d6() );
                assert_eq!( &mut 9         +      _1dNeg8(), _1d8() );
                assert_eq!( &mut 9         + &    _1dNeg8(), _1d8() );
                assert_eq!( &mut 9         + &mut _1dNeg8(), _1d8() );
            }

            // Sub.
            {
                assert_eq!(      _2d4() -      _1d4(), subRes ); // Die - Die
                assert_eq!(      _2d4() - &    _1d4(), subRes );
                assert_eq!(      _2d4() - &mut _1d4(), subRes );
                assert_eq!( &    _2d4() -      _1d4(), subRes );
                assert_eq!( &    _2d4() - &    _1d4(), subRes );
                assert_eq!( &    _2d4() - &mut _1d4(), subRes );
                assert_eq!( &mut _2d4() -      _1d4(), subRes );
                assert_eq!( &mut _2d4() - &    _1d4(), subRes );
                assert_eq!( &mut _2d4() - &mut _1d4(), subRes );

                assert_eq!(      _1d2() -      3     , _1dNeg2() ); // Die - Value
                assert_eq!(      _1d2() - &    3     , _1dNeg2() );
                assert_eq!(      _1d2() - &mut 3     , _1dNeg2() );
                assert_eq!( &    _1d4() -      5     , _1dNeg4() );
                assert_eq!( &    _1d4() - &    5     , _1dNeg4() );
                assert_eq!( &    _1d4() - &mut 5     , _1dNeg4() );
                assert_eq!( &mut _1d6() -      7     , _1dNeg6() );
                assert_eq!( &mut _1d6() - &    7     , _1dNeg6() );
                assert_eq!( &mut _1d6() - &mut 7     , _1dNeg6() );

                assert_eq!(      3      -      _1d2(), _1d2() ); // Value - Die
                assert_eq!(      3      - &    _1d2(), _1d2() );
                assert_eq!(      3      - &mut _1d2(), _1d2() );
                assert_eq!( &    5      -      _1d4(), _1d4() );
                assert_eq!( &    5      - &    _1d4(), _1d4() );
                assert_eq!( &    5      - &mut _1d4(), _1d4() );
                assert_eq!( &mut 7      -      _1d6(), _1d6() );
                assert_eq!( &mut 7      - &    _1d6(), _1d6() );
                assert_eq!( &mut 7      - &mut _1d6(), _1d6() );
            }

            // Mul.
            {
                assert_eq!(      _1d2() *      1     , _1d2() ); // Die * usize
                assert_eq!(      _1d2() * &    1     , _1d2() );
                assert_eq!(      _1d2() * &mut 1     , _1d2() );
                assert_eq!( &    _3d4() *      3     , _9d4() );
                assert_eq!( &    _3d4() * &    3     , _9d4() );
                assert_eq!( &    _3d4() * &mut 3     , _9d4() );
                assert_eq!( &mut _2d6() *      4     , _8d6() );
                assert_eq!( &mut _2d6() * &    4     , _8d6() );
                assert_eq!( &mut _2d6() * &mut 4     , _8d6() );

                assert_eq!(      1      *      _1d2(), _1d2() ); // usize * Die
                assert_eq!(      1      * &    _1d2(), _1d2() );
                assert_eq!(      1      * &mut _1d2(), _1d2() );
                assert_eq!( &    3      *      _3d4(), _9d4() );
                assert_eq!( &    3      * &    _3d4(), _9d4() );
                assert_eq!( &    3      * &mut _3d4(), _9d4() );
                assert_eq!( &mut 4      *      _2d6(), _8d6() );
                assert_eq!( &mut 4      * &    _2d6(), _8d6() );
                assert_eq!( &mut 4      * &mut _2d6(), _8d6() );
            }

            // AddAssign.
            { let mut d = _2d2(); d +=      _5d2(); assert_eq!( d, _7d2() ); } // Die += Die
            { let mut d = _1d6(); d += &    _2d6(); assert_eq!( d, _3d6() ); }
            { let mut d = _3d8(); d += &mut _4d8(); assert_eq!( d, _7d8() ); }

            { let mut d = _1dNeg2(); d +=      3; assert_eq!( d, _1d2() ); } // Die += Value
            { let mut d = _1dNeg6(); d += &    7; assert_eq!( d, _1d6() ); }
            { let mut d = _1dNeg8(); d += &mut 9; assert_eq!( d, _1d8() ); }

            // SubAssign.
            { let mut d = _2d4(); d -=      _1d4(); assert_eq!( d, subRes ); } // Die -= Die
            { let mut d = _2d4(); d -= &    _1d4(); assert_eq!( d, subRes ); }
            { let mut d = _2d4(); d -= &mut _1d4(); assert_eq!( d, subRes ); }

            { let mut d = _1d2(); d -=      3; assert_eq!( d, _1dNeg2() ); } // Die -= Value
            { let mut d = _1d4(); d -= &    5; assert_eq!( d, _1dNeg4() ); }
            { let mut d = _1d6(); d -= &mut 7; assert_eq!( d, _1dNeg6() ); }

            // MulAssign.
            { let mut d = _1d2(); d *=      2; assert_eq!( d, _2d2() ); } // Die *= usize
            { let mut d = _2d4(); d *= &    3; assert_eq!( d, _6d4() ); }
            { let mut d = _8d6(); d *= &mut 1; assert_eq!( d, _8d6() ); }
        }

        // Test various mathematical identities.
        {
            assert_eq!( _1d2()  + _2d2() + _3d2(), _6d2()          );
            assert_eq!( _1d2()  + _2d2() + _3d2(), _3d2() + _3d2() );
            assert_eq!( 5       + _1d4() - 5     , _1d4()          );
            assert_eq!( _1d4()  - (-5)           , _1d4() + 5      );
            assert_eq!( -3      - _1d2()         , -6     + _1d2() );
            assert_eq!( (_1d2() + 1    ) * 3     , _3d2() + 3      );
            assert_eq!( (_1d2() - 1    ) * 3     , _3d2() - 3      );
        }
    }

    #[test]
    fn Clamp()
    {
        let originalDie = Die::NewRaw(flatmap!{-1 => Count::from( 1u32),
                                                0 => Count::from( 2u32),
                                                1 => Count::from( 2u32),
                                                2 => Count::from(10u32),
                                                4 => Count::from(15u32),
                                                6 => Count::from( 1u32),
                                               50 => Count::from(19u32),});
        // Clamp min.
        {
            let clampedDie = originalDie.ClampMin(1);

            let hardcodedDie = Die::NewRaw(flatmap!{ 1 => Count::from( 5u32),
                                                     2 => Count::from(10u32),
                                                     4 => Count::from(15u32),
                                                     6 => Count::from( 1u32),
                                                    50 => Count::from(19u32),});
            assert_eq!(hardcodedDie, clampedDie);
        }

        // Clamp max.
        {
            let clampedDie = originalDie.ClampMax(5);

            let hardcodedDie = Die::NewRaw(flatmap!{-1 => Count::from( 1u32),
                                                     0 => Count::from( 2u32),
                                                     1 => Count::from( 2u32),
                                                     2 => Count::from(10u32),
                                                     4 => Count::from(15u32),
                                                     5 => Count::from(20u32),});
            assert_eq!(hardcodedDie, clampedDie);
        }

        // Clamp min and max.
        {
            let clampedDie = originalDie.Clamp(1, 5);

            let hardcodedDie = Die::NewRaw(flatmap!{ 1 => Count::from( 5u32),
                                                     2 => Count::from(10u32),
                                                     4 => Count::from(15u32),
                                                     5 => Count::from(20u32),});
            assert_eq!(hardcodedDie, clampedDie);
        }
    }

    #[test]
    fn ClampToSingleValue()
    {
        let referenceDie = Die::NewRaw(flatmap!{-9 => Count::from(50u32),});

        let clampedDie   = Die::NewRaw(flatmap!{-1 => Count::from( 1u32),
                                                 0 => Count::from( 2u32),
                                                 1 => Count::from( 2u32),
                                                 2 => Count::from(10u32),
                                                 4 => Count::from(15u32),
                                                 6 => Count::from( 1u32),
                                                50 => Count::from(19u32),}).Clamp(-9, -9);

        assert_eq!(referenceDie, clampedDie);
    }

    #[test]
    fn HalfSimple()
    {
        // Due to the way we round this is not simply 1d4.
        let correctHalfD8 = Die::NewRaw(flatmap!{0 => Count::from(1u32),
                                                 1 => Count::from(2u32),
                                                 2 => Count::from(2u32),
                                                 3 => Count::from(2u32),
                                                 4 => Count::from(1u32)});
        assert_eq!(correctHalfD8, _1d8().Half());
    }

    #[test]
    fn HalfComplicated()
    {
        let referenceDie = Die::NewRaw(flatmap!{-2 => Count::from( 1u32),
                                                -1 => Count::from( 5u32),
                                                 0 => Count::from(15u32),
                                                 1 => Count::from(15u32),
                                                 2 => Count::from( 9u32),});

        let halvedDie    = Die::NewRaw(flatmap!{-4 => Count::from(1u32),
                                                -3 => Count::from(2u32),
                                                -2 => Count::from(3u32),
                                                -1 => Count::from(4u32),
                                                 0 => Count::from(5u32),
                                                 1 => Count::from(6u32),
                                                 2 => Count::from(7u32),
                                                 3 => Count::from(8u32),
                                                 4 => Count::from(9u32),}).Half();

        assert_eq!(referenceDie, halvedDie);
    }

    #[test]
    fn WeightedMergeAllSame()
    {
        // If all the dice being merged are the same then the
        // weights don't matter, it shouldn't change anything.
        for baseDie in [_1d0(), _1d6(), _2d8(), _5d12(), _10d20()].iter()
        {
            assert_eq!(baseDie, &Die::WeightedMerge( &[(baseDie, Count::from(20u32))                                                             ] ));
            assert_eq!(baseDie, &Die::WeightedMerge( &[(baseDie, Count::from( 1u32)), (baseDie, Count::from( 2u32))                              ] ));
            assert_eq!(baseDie, &Die::WeightedMerge( &[(baseDie, Count::from( 5u32)), (baseDie, Count::from(14u32)), (baseDie, Count::from(1u32))] ));
        }
    }

    #[test]
    fn WeightedMerge()
    {
        let die = Die::NewRaw(flatmap!{0 => Count::from(100u32),
                                       2 => Count::from(183u32),
                                       3 => Count::from(105u32),
                                       6 => Count::from( 12u32),});

        let mergedDie =
        {
            let die1 = Die::NewRaw( flatmap!{0 => Count::from(1u32)                        } );
            let die2 = Die::NewRaw( flatmap!{2 => Count::from(5u32), 3 => Count::from(3u32)} );
            let die3 = Die::NewRaw( flatmap!{2 => Count::from(2u32), 6 => Count::from(3u32)} );

            Die::WeightedMerge( &[(&die1, Count::from(5u32)), (&die2, Count::from(14u32)), (&die3, Count::from(1u32))] )
        };

        assert_eq!(die, mergedDie);
    }

    #[test]
    fn WeightedMergeZeroWeight()
    {
        let mergedDie = Die::WeightedMerge( &[(&_1d6(), Count::from( 0u32)),
                                              (&_1d4(), Count::from(20u32))] );

        assert_eq!( _1d4(), mergedDie );
    }

    #[test]
    #[should_panic(expected = "Can't do weighted merge with no dice!")]
    fn WeightedMergeNoDice()
    {
        let _x = Die::WeightedMerge( &[] );
    }

    #[test]
    #[should_panic(expected = "Cannot reduce an empty CountDistribution!")]
    fn WeightedMergeOneDieZeroWeight()
    {
        let _x = Die::WeightedMerge( &[(&_1d6(), Count::zero())] );
    }

    #[test]
    fn HardcodedCountWaysOf()
    {
        // I'm extra paranoid of off-by-one errors in all the Die::CountWaysOf<OP>_Value functions so this
        // is a test with a hardcoded die, just so that I can see that the actual numbers are as expected.
        //
        // Each entry has a different count to make sure we're always getting exactly the right number and
        // there is a gap (3 is missing) to make sure that is handled as expected.
        let die = Die::NewRaw( flatmap!
        {
            1 => Count::from(  1u32),
            2 => Count::from( 10u32),
            4 => Count::from(100u32),
        });

        let LT = |value : Value, count : u32| { assert_eq!( die.CountWaysOfLT_Value(value), Count::from(count) ); };
        let LE = |value : Value, count : u32| { assert_eq!( die.CountWaysOfLE_Value(value), Count::from(count) ); };
        let EQ = |value : Value, count : u32| { assert_eq!( die.CountWaysOfEQ_Value(value), Count::from(count) ); };
        let NE = |value : Value, count : u32| { assert_eq!( die.CountWaysOfNE_Value(value), Count::from(count) ); };
        let GE = |value : Value, count : u32| { assert_eq!( die.CountWaysOfGE_Value(value), Count::from(count) ); };
        let GT = |value : Value, count : u32| { assert_eq!( die.CountWaysOfGT_Value(value), Count::from(count) ); };

        LT(0,   0); LE(0,   0); EQ(0,   0); NE(0, 111); GE(0, 111); GT(0, 111);
        LT(1,   0); LE(1,   1); EQ(1,   1); NE(1, 110); GE(1, 111); GT(1, 110);
        LT(2,   1); LE(2,  11); EQ(2,  10); NE(2, 101); GE(2, 110); GT(2, 100);
        LT(3,  11); LE(3,  11); EQ(3,   0); NE(3, 111); GE(3, 100); GT(3, 100);
        LT(4,  11); LE(4, 111); EQ(4, 100); NE(4,  11); GE(4, 100); GT(4,   0);
        LT(5, 111); LE(5, 111); EQ(5,   0); NE(5, 111); GE(5,   0); GT(5,   0);

        TestArbitraryDie( "die", &die );
    }

    #[test]
    fn SimpleDice()
    {
        fn TestSimpleDie( aDie : &Die, aMax : Value )
        {
            let aName         = &format!("1d{}", aMax)[..];
            let aMin          = 1;
            let aMean         = MeanValue(((aMin + aMax) as f64) / 2.0);
            let aDistribution = (aMin..=aMax).map(|value| (value, Count::from(1u32))).collect();

            TestArbitraryDieMost( aName, aDie, aMin, aMax, aMean, &aDistribution );
        }

        TestSimpleDie( &_1d1  (),  1 );
        TestSimpleDie( &_1d2  (),  2 );
        TestSimpleDie( &_1d4  (),  4 );
        TestSimpleDie( &_1d6  (),  6 );
        TestSimpleDie( &_1d8  (),  8 );
        TestSimpleDie( &_1d10 (), 10 );
        TestSimpleDie( &_1d12 (), 12 );
        TestSimpleDie( &_1d20 (), 20 );
    }

    #[test]
    fn ZeroDie()
    {
        TestArbitraryDieMost
        (
            "1d0",
            &_1d0(),
            0,
            0,
            MeanValue(0.0),
            &flatmap!{
                0 => Count::from(1u32)
            }
        );
    }

    #[test]
    fn SimpleNonContiguousDie()
    {
        let dieDistribution = flatmap!{
            -2 => Count::from(1u32),
             0 => Count::from(1u32),
             2 => Count::from(1u32),
        };
        let die = Die::NewRaw(dieDistribution.clone());

        TestArbitraryDieMost
        (
            "non-contiguous",
            &die,
            -2,
            2,
            MeanValue(0.0),
            &dieDistribution
        );
    }

    #[test]
    fn ComplexNonContiguousDie()
    {
        let dieDistribution = flatmap!{
            0 => Count::from(10u32),
            5 => Count::from( 1u32),
            6 => Count::from( 2u32),
            7 => Count::from( 3u32),
            8 => Count::from( 2u32),
            9 => Count::from( 1u32),
        };
        let die = Die::NewRaw(dieDistribution.clone());

        TestArbitraryDieMost
        (
            "non-contiguous",
            &die,
            0,
            9,
            MeanValue(63.0 / 19.0),
            &dieDistribution
        );
    }

    #[test]
    fn NegativeSimpleDice()
    {
        fn TestNegativeSimpleDie( aDie : &Die, aMin : Value )
        {
            let aName         = &format!("1d{}", aMin)[..];
            let aMax          = -1;
            let aMean         = MeanValue(((aMin + aMax) as f64) / 2.0);
            let aDistribution = (aMin..=aMax).map(|value| (value, Count::from(1u32))).collect();

            TestArbitraryDieMost( aName, aDie, aMin, aMax, aMean, &aDistribution );
        }

        TestNegativeSimpleDie( &_1dNeg1 (), - 1 );
        TestNegativeSimpleDie( &_1dNeg2 (), - 2 );
        TestNegativeSimpleDie( &_1dNeg4 (), - 4 );
        TestNegativeSimpleDie( &_1dNeg6 (), - 6 );
        TestNegativeSimpleDie( &_1dNeg8 (), - 8 );
        TestNegativeSimpleDie( &_1dNeg10(), -10 );
        TestNegativeSimpleDie( &_1dNeg12(), -12 );
        TestNegativeSimpleDie( &_1dNeg20(), -20 );
    }

    #[test]
    fn CombinedNegativeAndPositiveDice()
    {
        println!("1d4 - 4 vs 1d-4");
        assert_eq!
        (
            Die::from( 1..=4) - 4,
            Die::from(-3..=0)
        );
    }

    #[test]
    fn SkewedSimpleDice()
    {
        // Skew high.
        TestArbitraryDieMost
        (
            "1d20_adv",
            &_1d20_adv(),
            1,
            20,
            MeanValue(13.825),
            &flatmap!{
                 1 => Count::from( 1u32),
                 2 => Count::from( 3u32),
                 3 => Count::from( 5u32),
                 4 => Count::from( 7u32),
                 5 => Count::from( 9u32),
                 6 => Count::from(11u32),
                 7 => Count::from(13u32),
                 8 => Count::from(15u32),
                 9 => Count::from(17u32),
                10 => Count::from(19u32),
                11 => Count::from(21u32),
                12 => Count::from(23u32),
                13 => Count::from(25u32),
                14 => Count::from(27u32),
                15 => Count::from(29u32),
                16 => Count::from(31u32),
                17 => Count::from(33u32),
                18 => Count::from(35u32),
                19 => Count::from(37u32),
                20 => Count::from(39u32),
            },
        );

        // Skew very high.
        TestArbitraryDieMost
        (
            "1d20_superAdv",
            &_1d20_superAdv(),
            1,
            20,
            MeanValue(15.4875),
            &flatmap!{
                1  => Count::from(   1u32),
                2  => Count::from(   7u32),
                3  => Count::from(  19u32),
                4  => Count::from(  37u32),
                5  => Count::from(  61u32),
                6  => Count::from(  91u32),
                7  => Count::from( 127u32),
                8  => Count::from( 169u32),
                9  => Count::from( 217u32),
                10 => Count::from( 271u32),
                11 => Count::from( 331u32),
                12 => Count::from( 397u32),
                13 => Count::from( 469u32),
                14 => Count::from( 547u32),
                15 => Count::from( 631u32),
                16 => Count::from( 721u32),
                17 => Count::from( 817u32),
                18 => Count::from( 919u32),
                19 => Count::from(1027u32),
                20 => Count::from(1141u32),
            },
        );

        // Skew low.
        TestArbitraryDieMost
        (
            "1d20_disadv",
            &_1d20_disadv(),
            1,
            20,
            MeanValue(7.175),
            &flatmap!{
                 1 => Count::from(39u32),
                 2 => Count::from(37u32),
                 3 => Count::from(35u32),
                 4 => Count::from(33u32),
                 5 => Count::from(31u32),
                 6 => Count::from(29u32),
                 7 => Count::from(27u32),
                 8 => Count::from(25u32),
                 9 => Count::from(23u32),
                10 => Count::from(21u32),
                11 => Count::from(19u32),
                12 => Count::from(17u32),
                13 => Count::from(15u32),
                14 => Count::from(13u32),
                15 => Count::from(11u32),
                16 => Count::from( 9u32),
                17 => Count::from( 7u32),
                18 => Count::from( 5u32),
                19 => Count::from( 3u32),
                20 => Count::from( 1u32),
            }
        );

        // Halfling class feature.
        TestArbitraryDieMost
        (
            "1d20_lucky",
            &_1d20_lucky(),
            1,
            20,
            MeanValue(10.975),
            &flatmap!{
                 1 => Count::from( 1u32),
                 2 => Count::from(21u32),
                 3 => Count::from(21u32),
                 4 => Count::from(21u32),
                 5 => Count::from(21u32),
                 6 => Count::from(21u32),
                 7 => Count::from(21u32),
                 8 => Count::from(21u32),
                 9 => Count::from(21u32),
                10 => Count::from(21u32),
                11 => Count::from(21u32),
                12 => Count::from(21u32),
                13 => Count::from(21u32),
                14 => Count::from(21u32),
                15 => Count::from(21u32),
                16 => Count::from(21u32),
                17 => Count::from(21u32),
                18 => Count::from(21u32),
                19 => Count::from(21u32),
                20 => Count::from(21u32),
            }
        );
    }

    #[test]
    fn CombinedDice()
    {
        TestArbitraryDieMost
        (
            "3d6",
            &_3d6(),
            3,
            18,
            MeanValue(10.5),
            &flatmap!{
                 3 => Count::from( 1u32),
                 4 => Count::from( 3u32),
                 5 => Count::from( 6u32),
                 6 => Count::from(10u32),
                 7 => Count::from(15u32),
                 8 => Count::from(21u32),
                 9 => Count::from(25u32),
                10 => Count::from(27u32),
                11 => Count::from(27u32),
                12 => Count::from(25u32),
                13 => Count::from(21u32),
                14 => Count::from(15u32),
                15 => Count::from(10u32),
                16 => Count::from( 6u32),
                17 => Count::from( 3u32),
                18 => Count::from( 1u32),
            }
        );

        TestArbitraryDieMost
        (
            "1d1_1d2_1d4_1d6_1d8_1d10_1d12",
            &(_1d1() + _1d2() + _1d4() + _1d6() + _1d8() + _1d10() + _1d12()),
            7,
            43,
            MeanValue(25.0),
            &flatmap!{
                 7 => Count::from(   1u32),
                 8 => Count::from(   6u32),
                 9 => Count::from(  20u32),
                10 => Count::from(  50u32),
                11 => Count::from( 104u32),
                12 => Count::from( 190u32),
                13 => Count::from( 315u32),
                14 => Count::from( 484u32),
                15 => Count::from( 699u32),
                16 => Count::from( 958u32),
                17 => Count::from(1255u32),
                18 => Count::from(1580u32),
                19 => Count::from(1919u32),
                20 => Count::from(2254u32),
                21 => Count::from(2565u32),
                22 => Count::from(2832u32),
                23 => Count::from(3037u32),
                24 => Count::from(3166u32),
                25 => Count::from(3210u32),
                26 => Count::from(3166u32),
                27 => Count::from(3037u32),
                28 => Count::from(2832u32),
                29 => Count::from(2565u32),
                30 => Count::from(2254u32),
                31 => Count::from(1919u32),
                32 => Count::from(1580u32),
                33 => Count::from(1255u32),
                34 => Count::from( 958u32),
                35 => Count::from( 699u32),
                36 => Count::from( 484u32),
                37 => Count::from( 315u32),
                38 => Count::from( 190u32),
                39 => Count::from( 104u32),
                40 => Count::from(  50u32),
                41 => Count::from(  20u32),
                42 => Count::from(   6u32),
                43 => Count::from(   1u32),
            }
        );
    }

    #[test]
    fn GreatWeaponFighting()
    {
        let shouldReroll = |value| { value == 1 || value == 2 };

        TestArbitraryDieMost
        (
            "GreatWeaponFighting",
            &(_1d6().RerollNumbers(shouldReroll) * 2),
            2,
            12,
            MeanValue(25.0 / 3.0),
            &flatmap!{
                2  => Count::from( 1u32),
                3  => Count::from( 2u32),
                4  => Count::from( 9u32),
                5  => Count::from(16u32),
                6  => Count::from(32u32),
                7  => Count::from(48u32),
                8  => Count::from(56u32),
                9  => Count::from(64u32),
                10 => Count::from(48u32),
                11 => Count::from(32u32),
                12 => Count::from(16u32),
            }
        );

        // It does not distribute.
        assert_ne!( _1d6().RerollNumbers(shouldReroll) * 2, _2d6().RerollNumbers(shouldReroll) );
    }

    //
    // These are the largest dice sets that always have a completely accurate mean value.
    // Any smaller numbers will have an accurate mean value, larger numbers may or may not depending on floating point rounding.
    //
    #[test] fn DicePrecision119d2   () { TestArbitraryDieMore("119d2" , &(_1d2()    * 119),  119,  238, MeanValue( 178.5)); }
    #[test] fn DicePrecision119dNeg2() { TestArbitraryDieMore("119d-2", &(_1dNeg2() * 119), -238, -119, MeanValue(-178.5)); }
    #[test] fn DicePrecision59d4    () { TestArbitraryDieMore("59d4"  , &(_1d4()    *  59),   59,  236, MeanValue( 147.5)); }
    #[test] fn DicePrecision46d6    () { TestArbitraryDieMore("46d6"  , &(_1d6()    *  46),   46,  276, MeanValue( 161.0)); }
    #[test] fn DicePrecision39d8    () { TestArbitraryDieMore("39d8"  , &(_1d8()    *  39),   39,  312, MeanValue( 175.5)); }
    #[test] fn DicePrecision35d10   () { TestArbitraryDieMore("35d10" , &(_1d10()   *  35),   35,  350, MeanValue( 192.5)); }
    #[test] fn DicePrecision34d12   () { TestArbitraryDieMore("34d12" , &(_1d12()   *  34),   34,  408, MeanValue( 221.0)); }
    #[test] fn DicePrecision27d20   () { TestArbitraryDieMore("27d20" , &(_1d20()   *  27),   27,  540, MeanValue( 283.5)); }

    //
    // These are the first dice sets that would have an inaccurate mean value if we weren't using big-ints.
    //
    #[test] fn DicePrecision120d2   () { TestArbitraryDieMore("120d2" , &(_1d2()    * 120),  120,  240, MeanValue( 180.0)); }
    #[test] fn DicePrecision120dNeg2() { TestArbitraryDieMore("120d-2", &(_1dNeg2() * 120), -240, -120, MeanValue(-180.0)); }
    #[test] fn DicePrecision60d4    () { TestArbitraryDieMore("60d4"  , &(_1d4()    *  60),   60,  240, MeanValue( 150.0)); }
    #[test] fn DicePrecision47d6    () { TestArbitraryDieMore("47d6"  , &(_1d6()    *  47),   47,  282, MeanValue( 164.5)); }
    #[test] fn DicePrecision40d8    () { TestArbitraryDieMore("40d8"  , &(_1d8()    *  40),   40,  320, MeanValue( 180.0)); }
    #[test] fn DicePrecision36d10   () { TestArbitraryDieMore("36d10" , &(_1d10()   *  36),   36,  360, MeanValue( 198.0)); }
    #[test] fn DicePrecision35d12   () { TestArbitraryDieMore("35d12" , &(_1d12()   *  35),   35,  420, MeanValue( 227.5)); }
    #[test] fn DicePrecision28d20   () { TestArbitraryDieMore("28d20" , &(_1d20()   *  28),   28,  560, MeanValue( 294.0)); }

    //
    // These are the first dice sets that would trigger overflows if we weren't using big-ints.
    //
    #[test] fn DicePrecision128d2   () { TestArbitraryDieMore("128d2" , &(_1d2()    * 128),  128,  256, MeanValue( 192.0)); }
    #[test] fn DicePrecision128dNeg2() { TestArbitraryDieMore("128d-2", &(_1dNeg2() * 128), -256, -128, MeanValue(-192.0)); }
    #[test] fn DicePrecision64d4    () { TestArbitraryDieMore("64d4"  , &(_1d4()    *  64),   64,  256, MeanValue( 160.0)); }
    #[test] fn DicePrecision50d6    () { TestArbitraryDieMore("50d6"  , &(_1d6()    *  50),   50,  300, MeanValue( 175.0)); }
    #[test] fn DicePrecision43d8    () { TestArbitraryDieMore("43d8"  , &(_1d8()    *  43),   43,  344, MeanValue( 193.5)); }
    #[test] fn DicePrecision39d10   () { TestArbitraryDieMore("39d10" , &(_1d10()   *  39),   39,  390, MeanValue( 214.5)); }
    #[test] fn DicePrecision36d12   () { TestArbitraryDieMore("36d12" , &(_1d12()   *  36),   36,  432, MeanValue( 234.0)); }
    #[test] fn DicePrecision30d20   () { TestArbitraryDieMore("30d20" , &(_1d20()   *  30),   30,  600, MeanValue( 315.0)); }
}
