use crate::dice::*;
use crate::distribution::*;
use crate::saving_throw::*;
use crate::check_against_dc::*;
use crate::typedefs::*;
use crate::parsing::GenerateJSON;

use strum::{EnumIter, Display, IntoEnumIterator};

use serde::{Serialize, Deserialize};
use rand::prelude::*;

#[derive(Clone, Copy, Debug, PartialEq, Eq, Serialize, Deserialize, EnumIter, Display)]
pub enum SpellType
{
    Normal              ,
    NormalAgainstEvasion,
    Cantrip             ,
    AutoHit             ,
}

fn GenerateRandomSpellTypeString<R : Rng>( rng : &mut R ) -> String
{
    let randomSpellType = SpellType::iter().choose(rng).unwrap();

    // These are for use in JSON strings, hence the embedded quotes.
    format!("\"{}\"", randomSpellType)
}

#[derive(Clone, Debug, PartialEq, Deserialize)]
#[serde(from = "DeserializeData_Spell")]
pub struct Spell
{
    passSaveDamageDie : Die,
    failSaveDamageDie : Die,

    //
    // Technically there's no reason this struct needs to store the DC directly, it
    // doesn't affect the damage dice at all so it could just be passed into all the
    // external functions (Roll, ExpectedDamage, etc...). This would allow a single
    // Spell to be reused with multiple DCs in the most efficient manner possible.
    //
    // However, that use case is not a very common one, generally calling code wants
    // to treat a Spell as a single "thing". Having to handle the DC separately is
    // cumbersome and usually results in a tuple (Spell, DC) that requires special
    // logic handling compared to an AttackSequence.
    //
    dc : DC,
}

impl Spell
{
    pub fn Roll( &self, aSavingThrow : &SavingThrow ) -> Value
    {
        if aSavingThrow.Roll(self.dc)
        {
            self.passSaveDamageDie.Roll()
        }
        else
        {
            self.failSaveDamageDie.Roll()
        }
    }

    pub fn CalculateDamageDistribution( &self, aSavingThrow : &SavingThrow ) -> Die
    {
        // TODO: This potentially calculates CountWaysToPass twice.
        let weights = &[(&self.failSaveDamageDie, aSavingThrow.CountWaysToFail(self.dc)),
                        (&self.passSaveDamageDie, aSavingThrow.CountWaysToPass(self.dc))];

        Die::WeightedMerge(weights)
    }

    pub fn ExpectedDamage( &self, aSavingThrow : &SavingThrow ) -> MeanValue
    {
        self.CalculateDamageDistribution(aSavingThrow).Mean()
    }

    pub fn New(aSpellType : SpellType, mut aDamageDie : Die, aDC : DC) -> Spell
    {
        aDamageDie = aDamageDie.ClampMin(0);

        let (passSaveDamageDie, failSaveDamageDie) = match aSpellType
        {
            SpellType::Normal               => (aDamageDie.Half() , aDamageDie       ),
            SpellType::NormalAgainstEvasion => (_1d0()            , aDamageDie.Half()),
            SpellType::Cantrip              => (_1d0()            , aDamageDie       ),
            SpellType::AutoHit              => (aDamageDie.clone(), aDamageDie       ),
        };

        Spell{ passSaveDamageDie, failSaveDamageDie, dc : aDC }
    }
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
struct DeserializeData_Spell
{
    spellType : SpellType,
    damage    : Die,
    dc        : DC,
}

impl From<DeserializeData_Spell> for Spell
{
    fn from(data : DeserializeData_Spell) -> Self
    {
        Spell::New(data.spellType, data.damage, data.dc)
    }
}

// NOTE: This should match the structure of DeserializeData_Spell.
pub fn GenerateRandomJSON_Spell<R : Rng>( rng : &mut R ) -> String
{
    GenerateJSON::BuildObject( vec!
    [
        GenerateJSON::BuildField( "spellType", Some( GenerateRandomSpellTypeString(rng) ) ),
        GenerateJSON::BuildField( "damage"   , Some( GenerateRandomDamageDie(rng)       ) ),
        GenerateJSON::BuildField( "dc"       , Some( rng.gen_range(5..25).to_string()   ) ),
    ])
}

#[cfg(test)]
mod tests
{
    use super::*;
    use num_traits::identities::Zero;

    #[test]
    fn Deserialize()
    {
        // Authored input.
        let validStrings =
        [
            ( Spell::New( SpellType::Normal              , _1d6()       , DC(  0) ), r#"{ "spellType": "Normal"              , "damage": "1d6"    , "dc":   0 }"# ),
            ( Spell::New( SpellType::Normal              , Die::from(10), DC( 10) ), r#"{ "spellType": "Normal"              , "damage": "10"     , "dc":  10 }"# ),
            ( Spell::New( SpellType::NormalAgainstEvasion, _3d4() - 1   , DC( 19) ), r#"{ "spellType": "NormalAgainstEvasion", "damage": "3d4 - 1", "dc":  19 }"# ),
            ( Spell::New( SpellType::Cantrip             , _2d12()      , DC(100) ), r#"{ "spellType": "Cantrip"             , "damage": "2d12"   , "dc": 100 }"# ),
            ( Spell::New( SpellType::AutoHit             , _2d12()      , DC(100) ), r#"{ "spellType": "AutoHit"             , "damage": "2d12"   , "dc": 100 }"# ),
        ];

        let invalidStrings =
        [
            (r#"{                                                           }"#, "missing field `spellType`"),
            (r#"{ "spellType": "Normal"                                     }"#, "missing field `damage`"),
            (r#"{ "SpellType": "Normal", "damage": "10", "dc": 10           }"#, "unknown field `SpellType`, expected one of `spellType`, `damage`, `dc`"),
            (r#"{ "spellType": "Normal", "damage": "10", "dc": 10, "foo": 1 }"#, "unknown field `foo`, expected one of `spellType`, `damage`, `dc`"),
        ];

        crate::unit_test_utils::TestDeserializeFromJSON( &validStrings, &invalidStrings );

        // Random input.
        let mut rng = rand::thread_rng();
        crate::unit_test_utils::TestGenerateRandomJSON::<_, Spell>( ||{ GenerateRandomJSON_Spell(&mut rng) } );
    }

    fn TestSpell( aSpellType : SpellType, someSpellDamageDice : Die, aSavingThrowModifier : Die )
    {
        for aSavingThrowRollStyle in RollStyle::iter()
        {
            let aSavingThrow = SavingThrow::FromDie(aSavingThrowRollStyle, aSavingThrowModifier.clone());

            for aSpellSaveDC in aSavingThrow.GetRollRange(-2, 2).map(DC)
            {
                // Calculate the distribution manually.
                let calculatedDamageProbabilityDistribution =
                {
                    let mut ret = CountDistribution::new();

                    for (sValue, sCount) in aSavingThrow.GetDie().GetDistribution().iter()
                    {
                        let passedSave = *sValue >= *aSpellSaveDC;

                        for (dValue, dCount) in someSpellDamageDice.GetDistribution().iter()
                        {
                            let numWaysToRoll = sCount * dCount;

                            let damage = match aSpellType
                            {
                                SpellType::Normal               => if passedSave { *dValue / 2 } else { *dValue     },
                                SpellType::NormalAgainstEvasion => if passedSave { 0           } else { *dValue / 2 },
                                SpellType::Cantrip              => if passedSave { 0           } else { *dValue     },
                                SpellType::AutoHit              => if passedSave { *dValue     } else { *dValue     },
                            };

                            let entry = ret.entry(damage).or_insert_with(Count::zero);
                            *entry += numWaysToRoll;
                        }
                    }

                    BuildProbabilityDistribution( &ret )
                };

                let spell = Spell::New( aSpellType, someSpellDamageDice.clone(), aSpellSaveDC );

                let expectedDistribution = spell.CalculateDamageDistribution(&aSavingThrow);

                let expectedDamage = expectedDistribution.Mean();
                let expectedProbabilityDistribution = expectedDistribution.BuildProbabilityDistribution();

                assert_eq!(expectedProbabilityDistribution, calculatedDamageProbabilityDistribution);

                // Test it a second way by emperically rolling things.
                let RollFunction = ||
                {
                    let damage = spell.Roll(&aSavingThrow);

                    assert!( damage >= 0, "{} damage shouldn't be possible!", damage );

                    damage
                };

                // Magic number. Needs to be low so that the tests don't take forever to run
                // but high enough that we don't get spurious failures due to random rolling.
                let numIterations = 50_000;

                crate::unit_test_utils::TestRolling(RollFunction, numIterations, expectedDamage, &expectedProbabilityDistribution);
            }
        }
    }

    // By having many smaller tests we can run them in parallel rather than having a single monolithic test.
    mod unrolled
    {
        use super::*;

        macro_rules! UnrollTest_SavingThrowMod
        {
            ($spellTypeName:ident, $spellType:expr, $damageDieName:ident, $damageDie:expr) =>
            {
                // Use the paste crate to be able to construct concatenated function names.
                paste::item!
                {
                    #[test] fn [< TestSpell _ $spellTypeName $damageDieName _ SavingThrow _N3____ >] () { TestSpell( $spellType, $damageDie, Die::from(-3) ); }
                    #[test] fn [< TestSpell _ $spellTypeName $damageDieName _ SavingThrow _0_____ >] () { TestSpell( $spellType, $damageDie, Die::from( 0) ); }
                    #[test] fn [< TestSpell _ $spellTypeName $damageDieName _ SavingThrow _P4____ >] () { TestSpell( $spellType, $damageDie, Die::from( 4) ); }
                    #[test] fn [< TestSpell _ $spellTypeName $damageDieName _ SavingThrow _2d4___ >] () { TestSpell( $spellType, $damageDie,  _2d4()       ); }
                    #[test] fn [< TestSpell _ $spellTypeName $damageDieName _ SavingThrow _1dNeg6 >] () { TestSpell( $spellType, $damageDie, -_1d6()       ); }
                }
            };
        }

        macro_rules! UnrollTest_DamageDie
        {
            ($spellTypeName:ident, $spellType:expr) =>
            {
                UnrollTest_SavingThrowMod!( $spellTypeName, $spellType, _1d0___, _1d0()             );
                UnrollTest_SavingThrowMod!( $spellTypeName, $spellType, _1d1___, _1d1()             );
                UnrollTest_SavingThrowMod!( $spellTypeName, $spellType, _1d2___, _1d2()             );
                UnrollTest_SavingThrowMod!( $spellTypeName, $spellType, _1d4___, _1d4()             );
                UnrollTest_SavingThrowMod!( $spellTypeName, $spellType, _2d6Adv, _2d6().Advantage() );
                UnrollTest_SavingThrowMod!( $spellTypeName, $spellType, _2d8P1_, _2d8() + 1         );
            };
        }

        UnrollTest_DamageDie!( Normal______________, SpellType::Normal               );
        UnrollTest_DamageDie!( NormalAgainstEvasion, SpellType::NormalAgainstEvasion );
        UnrollTest_DamageDie!( Cantrip_____________, SpellType::Cantrip              );
        UnrollTest_DamageDie!( AutoHit_____________, SpellType::AutoHit              );
    }

    #[test]
    fn Hardcoded()
    {
        // Just some very basic tests with hardcoded numbers as a sanity check.
        let aSavingThrow = SavingThrow::FromMod( RollStyle::Normal, SavingThrowModifier(3) );

        let Test = |aSpellType : SpellType, anExpectedDamageProbabilityDistribution : ProbabilityDistribution|
        {
            let spell = Spell::New( aSpellType, _2d4(), DC(16) );

            let calculatedDamageProbabilityDistribution = spell.CalculateDamageDistribution(&aSavingThrow).BuildProbabilityDistribution();

            assert_eq!(calculatedDamageProbabilityDistribution, anExpectedDamageProbabilityDistribution);
        };

        Test( SpellType::Normal              , flatmap!{              1 => 6./80., 2 => 17./80., 3 => 16./80., 4 => 11./80., 5 => 12./80., 6 =>  9./80., 7 =>  6./80., 8 => 3./80.} );
        Test( SpellType::NormalAgainstEvasion, flatmap!{0 => 32./80., 1 => 9./80., 2 => 21./80., 3 => 15./80., 4 =>  3./80.                                                       } );
        Test( SpellType::Cantrip             , flatmap!{0 => 32./80.,              2 =>  3./80., 3 =>  6./80., 4 =>  9./80., 5 => 12./80., 6 =>  9./80., 7 =>  6./80., 8 => 3./80.} );
        Test( SpellType::AutoHit             , flatmap!{                           2 =>  5./80., 3 => 10./80., 4 => 15./80., 5 => 20./80., 6 => 15./80., 7 => 10./80., 8 => 5./80.} );
    }
}
