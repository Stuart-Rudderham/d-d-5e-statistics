#!/bin/bash

./run-unit-tests-helper.sh                                                           \
test    `# Run deoptimized code for faster compile times.`                           \
        `# https://doc.rust-lang.org/cargo/reference/profiles.html#default-profiles` \
                                                                                     \
default `# Use the default feature set.`                                             \
        `# Needed because the --features flag requires an argument`                  \
                                                                                     \
$*      `# Pass through any user supplied arguments`
