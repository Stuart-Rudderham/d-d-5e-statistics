use crate::dice::*;
use crate::distribution::*;
use crate::attack::*;
use crate::attack_roller::*;
use crate::typedefs::*;
use crate::parsing::GenerateJSON;

use rand::prelude::*;
use serde::Deserialize;
use std::iter;
use std::ops::Add;

#[derive(Clone, Debug, PartialEq, Deserialize)]
#[serde(from = "DeserializeData_AttackSequence")]
pub struct AttackSequence
{
    attacks              : Vec<Attack>,
    oncePerTurnDamageDie : Option<Die>,
}

impl AttackSequence
{
    pub fn CalculateDamageDistribution( &self, anAC : AC ) -> Die
    {
        fn Helper( anAC : AC, anOptOncePerTurnDamageDie : Option<&Die>, attacks : &[Attack] ) -> Die
        {
            if let Some(aOncePerTurnDamageDie) = anOptOncePerTurnDamageDie
            {
                // Recursive case.
                if let Some((attack, remainingAttacks)) = attacks.split_first()
                {
                    let attackWithOPTD =
                    {
                        let mut attackClone = attack.clone();

                        attackClone.AddDamageDie(aOncePerTurnDamageDie);

                        attackClone
                    };

                    // If the first attack hits then the OPTD die isn't available anymore.
                    let remainingAttacksDamageIfAttackHits = Helper(anAC, None, remainingAttacks);

                    // TODO: This is also calculating hit/crit hit chance, even though we already do it below!
                    let attackDamageDistributionIfHit = attackWithOPTD.CalculateDamageDistributionAssumingNoMiss(anAC);

                    let damageDistributionIfAttackHits = attackDamageDistributionIfHit + remainingAttacksDamageIfAttackHits;

                    // If the first attack misses then the damage distribution is just the distribution of
                    // the remaining attacks. The OPTD die is still available for the remaining attacks.
                    let damageDistributionIfAttackMisses = Helper(anAC, Some(aOncePerTurnDamageDie), remainingAttacks);

                    // TODO: Use function to calculate all the CountWaysToX at the same time!
                    // Merge the two distributions with the appropriate weights.
                    let numWaysToMiss = attackWithOPTD.GetAttackRoller().CountWaysToMiss(anAC);

                    let numWaysToHit  = attackWithOPTD.GetAttackRoller().CountWaysToHitCritical   (    ) +
                                        attackWithOPTD.GetAttackRoller().CountWaysToHitNonCritical(anAC);

                    Die::WeightedMerge( &[(&damageDistributionIfAttackHits  , numWaysToHit ),
                                          (&damageDistributionIfAttackMisses, numWaysToMiss)] )
                }
                // Base case.
                else
                {
                    _1d0()
                }
            }
            else
            {
                // Much simpler if no once-per-turn damage, just combine all the attacks.
                attacks.iter()
                       .map(|attack| attack.CalculateDamageDistribution(anAC))
                       .fold(_1d0(), Die::add )
            }
        }

        let oncePerTurnDamageDie : Option<&Die> =
        {
            match self.oncePerTurnDamageDie
            {
                None        => None,
                Some(ref d) =>
                {
                    if *d == _1d0()
                    {
                        // The deserialize function for AttackSequence will not generally create
                        // a None once-per-turn damage die, it's much easier for clients to show
                        // a consistent UI if they treat the default case as a die doing 0 damage.
                        // So add a fast-path to optimize that common usecase.
                        None
                    }
                    else
                    {
                        Some(d)
                    }
                }
            }
        };

        Helper(anAC, oncePerTurnDamageDie, &self.attacks)
    }

    pub fn ExpectedDamage( &self, anAC : AC ) -> MeanValue
    {
        self.CalculateDamageDistribution(anAC).Mean()
    }

    pub fn Roll( &self, anAC : AC ) -> Value
    {
        let mut appliedOncePerTurnDamage = false;

        let mut GetOncePerTurnDamage = |anAttackResult : AttackResult|
        {
            if let Some(ref damageDie) = self.oncePerTurnDamageDie
            {
                if appliedOncePerTurnDamage
                {
                    0
                }
                else
                {
                    match anAttackResult
                    {
                        AttackResult::Hit         => { appliedOncePerTurnDamage = true; damageDie.Roll()                    },
                        AttackResult::CrititalHit => { appliedOncePerTurnDamage = true; damageDie.Roll() + damageDie.Roll() },
                        AttackResult::Miss        => { 0                                                                    },
                    }
                }
            }
            else
            {
                // No damage die so no damage.
                0
            }
        };

        let GetAttackDamage = |(aDamageAmount, anAttackResult) : (Value, AttackResult)|
        {
            aDamageAmount + GetOncePerTurnDamage(anAttackResult)
        };

        self.attacks.iter()
                    .map( |attack| attack.Roll(anAC) )
                    .map( GetAttackDamage )
                    .sum()
    }

    #[cfg(test)]
    pub fn GetRollRange( &self, aMinExtension : Value, aMaxExtension : Value ) -> std::ops::RangeInclusive<Value>
    {
        let attackRollRanges = self.attacks.iter().map(|x| x.GetAttackRoller().GetRollRange(aMinExtension, aMaxExtension));

        crate::unit_test_utils::GetEncompassingRollRange( attackRollRanges )
    }
}

pub fn AttackSequence( someAttacks : Vec<(Attack, usize)>, aOncePerTurnDamageDie : Option<Die> ) -> AttackSequence
{
    assert!( !someAttacks.is_empty(), "Cannot have an attack sequence with no attacks!" );

    if let Some(ref damageDie) = aOncePerTurnDamageDie
    {
        assert!( damageDie.Min() >= 0, "Cannot have damage die do negative damage!" );
    }

    let expandedAttacks = someAttacks.into_iter()
                                     .flat_map(|(attack, amount)| iter::repeat(attack).take(amount) )
                                     .collect();
    AttackSequence
    {
        attacks              : expandedAttacks       ,
        oncePerTurnDamageDie : aOncePerTurnDamageDie ,
    }
}

fn GetDefaultNumberOfAttacks() -> usize
{
    1
}

//
// There are some weird interactions between "deny_unknown_fields" and "flatten".
//
// More info here -> https://github.com/serde-rs/serde/issues/1600
//
// Based on the unit tests it doesn't seem to affect things though, unknown fields
// are still generating parsing errors as expected.
//
//#[serde(deny_unknown_fields)]
#[derive(Deserialize)]
struct DeserializeData_AttackAndNumber
{
    #[serde(flatten)]
    attack     : Attack,

    #[serde(default = "GetDefaultNumberOfAttacks")]
    numAttacks : usize,
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
struct DeserializeData_AttackSequence
{
    attacks               : Vec<DeserializeData_AttackAndNumber>,
    oncePerTurnDamageDice : Option<Die>,
}

impl From<DeserializeData_AttackSequence> for AttackSequence
{
    fn from(data : DeserializeData_AttackSequence) -> Self
    {
        AttackSequence
        (
            data.attacks.into_iter().map(|x| (x.attack, x.numAttacks)).collect(),
            data.oncePerTurnDamageDice
        )
    }
}

// NOTE: This should match the structure of DeserializeData_AttackSequence.
pub fn GenerateRandomJSON_AttackSequence<R : Rng>( rng : &mut R ) -> String
{
    let attack = GenerateJSON::BuildObject( vec!
    [
        GenerateJSON::BuildField( "attackMod"               ,                Some( GenerateRandomModifierDie(rng)     )                 ),
        GenerateJSON::BuildField( "damageDice"              ,                Some( GenerateRandomDamageDie(rng)       )                 ),
        GenerateJSON::BuildField( "overrideOnCritDamageDice", if rng.gen() { Some( GenerateRandomDamageDie(rng)       ) } else { None } ),
        GenerateJSON::BuildField( "damageMod"               ,                Some( rng.gen_range(-2..10).to_string()  )                 ),
        GenerateJSON::BuildField( "rollStyle"               , if rng.gen() { Some( GenerateRandomRollStyleString(rng) ) } else { None } ),
        GenerateJSON::BuildField( "overrideCritHitThreshold", if rng.gen() { Some( rng.gen_range(15..22).to_string()  ) } else { None } ),
        GenerateJSON::BuildField( "numAttacks"              , if rng.gen() { Some( rng.gen_range(1..4).to_string()    ) } else { None } ),
    ]);

    // Hardcode only a single attack because the JSON is a
    // single line and it would be way too long otherwise.
    let attacks = GenerateJSON::BuildArray( vec!
    [
        Some(attack),
    ]);

    GenerateJSON::BuildObject( vec!
    [
        GenerateJSON::BuildField( "attacks"              ,                Some( attacks                      )                 ),
        GenerateJSON::BuildField( "oncePerTurnDamageDice", if rng.gen() { Some( GenerateRandomDamageDie(rng) ) } else { None } ),
    ])
}

#[cfg(test)]
mod tests
{
    use super::*;
    use strum::IntoEnumIterator;
    use std::convert::TryFrom;

    #[test]
    fn Deserialize()
    {
        // Authored input.
        let data1 =
        (
            AttackSequence
            (
                vec![ (Attack( AttackRoller::FromDie( 3 + _1d4(), RollStyle::Advantage, None ), _1d10(), DamageModifier(3), Some(_1d6())), 2) ],
                None
            ),
            r#"
            {
                "attacks": [ {"attackMod": "3 + 1d4", "rollStyle": "Advantage", "damageDice": "1d10", "overrideOnCritDamageDice": "1d6", "damageMod": 3, "numAttacks": 2} ]
            }"#
        );

        let data2 =
        (
            AttackSequence
            (
                vec![ (Attack( AttackRoller::FromMod( AttackModifier(5), RollStyle::Normal      , Some(19)), _2d8(), DamageModifier( 3), None ), 1),
                      (Attack( AttackRoller::FromMod( AttackModifier(2), RollStyle::Disadvantage, None    ), _1d4(), DamageModifier(10), None ), 3) ],
                Some( _1d0() )
            ),
            r#"
            {
                "attacks": [ {"attackMod": "5", "rollStyle": "Normal"      , "damageDice": "2d8", "damageMod":  3, "overrideCritHitThreshold": 19 },
                             {"attackMod": "2", "rollStyle": "Disadvantage", "damageDice": "1d4", "damageMod": 10, "numAttacks": 3                } ],
                "oncePerTurnDamageDice": "0"
            }"#
        );

        let data3 =
        (
            AttackSequence
            (
                vec![ (Attack( AttackRoller::FromMod( AttackModifier(5), RollStyle::Normal, None ), _1d4(), DamageModifier(-1), None), 2) ],
                Some( _8d6() )
            ),
            r#"
            {
                "attacks": [{"attackMod": "5", "damageDice": "1d4", "damageMod": -1, "numAttacks": 2}],
                "oncePerTurnDamageDice": "8d6"
            }"#
        );

        let data4 =
        (
            AttackSequence
            (
                vec![ (Attack( AttackRoller::FromMod( AttackModifier(5), RollStyle::Normal, Some(24) ), _1d4(), DamageModifier(-1), None), 1) ],
                None,
            ),
            r#"
            {
                "attacks": [{"attackMod": "5", "rollStyle": "Normal", "damageDice": "1d4", "damageMod": -1, "overrideCritHitThreshold": 24}],
                "oncePerTurnDamageDice": null
            }"#
        );

        let validStrings =
        [
            data1,
            data2,
            data3,
            data4,
        ];

        let invalidStrings =
        [
            (r#"{ "attack" : [{"attackMod": "5", "rollStyle": "Normal", "damageDice": "1d4", "damageMod": -1, "overrideCritHitThreshold": 24}], "oncePerTurnDamageDice": null }"#, "unknown field `attack`"),
            (r#"{ "attacks": [{"attackMod": "5", "rollStyle": "Normal", "damageDice": "1d4", "damageMod": -1,         "critHitThreshold": 24}], "oncePerTurnDamageDice": null }"#, "unknown field `critHitThreshold`"),
            (r#"{ "attacks": [{"attackMod": "5", "RollStyle": "Normal", "damageDice": "1d4", "damageMod": -1                                }], "oncePerTurnDamageDice": null }"#, "unknown field `RollStyle`"),
            (r#"{ "attacks": [{"attackMod": "5", "rollStyle": "Normal", "damageDice": "1d4", "damageMod": -1, "NumAttacks": 2               }], "oncePerTurnDamageDice": null }"#, "unknown field `NumAttacks`"),
            (r#"{ "attacks": [{"attackMod": "5", "rollStyle": "Normal", "damageDice": "1d4", "damageMod": -1, "numAttacks": 2, "foo": 3     }], "oncePerTurnDamageDice": null }"#, "unknown field `foo`"),
            (r#"{ "attacks": [{"attackMod": "5", "rollStyle": "Normal", "damageDice": "1d4", "damageMod": -1                                }], "OncePerTurnDamageDice": null }"#, "unknown field `OncePerTurnDamageDice`, expected `attacks` or `oncePerTurnDamageDice`"),
        ];

        crate::unit_test_utils::TestDeserializeFromJSON( &validStrings, &invalidStrings );

        // Random input.
        let mut rng = rand::thread_rng();
        crate::unit_test_utils::TestGenerateRandomJSON::<_, AttackSequence>( ||{ GenerateRandomJSON_AttackSequence(&mut rng) } );
    }

    #[test]
    fn GetRollRange()
    {
        let attacks = vec!
        [
            (Attack( AttackRoller::FromMod(AttackModifier(-10), RollStyle::Advantage   , None    ), _1d4(), DamageModifier(4), None ), 1),
            (Attack( AttackRoller::FromMod(AttackModifier(  5), RollStyle::Normal      , Some(10)), _2d6(), DamageModifier(3), None ), 2),
            (Attack( AttackRoller::FromMod(AttackModifier(  0), RollStyle::Disadvantage, None    ), _3d8(), DamageModifier(2), None ), 3),
            (Attack( AttackRoller::FromDie(_1d12()            , RollStyle::Normal      , None    ), _4d2(), DamageModifier(1), None ), 4),
        ];

        let attackSequence = AttackSequence(attacks, Some(_1d0()) );

        let rollRange = attackSequence.GetRollRange(-1, 2);

        assert_eq!( rollRange, -10..=34 );
    }

    #[test]
    fn AttackSequenceNumAttacks()
    {
        let together = AttackSequence(vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Disadvantage, None    ), _1d8(), DamageModifier(3), None ), 3)], None);

        let separate = AttackSequence(vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Disadvantage, None    ), _1d8(), DamageModifier(3), None ), 1),
                                           (Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Disadvantage, Some(20)), _1d8(), DamageModifier(3), None ), 1),
                                           (Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Disadvantage, None    ), _1d8(), DamageModifier(3), None ), 1)], None);

        let rollRangeTogether = together.GetRollRange(2, 2);
        let rollRangeSeparate = separate.GetRollRange(2, 2);

        assert_eq!(rollRangeTogether, rollRangeSeparate);

        for anAC in rollRangeTogether.map(AC)
        {
            assert_eq!( together.CalculateDamageDistribution(anAC), separate.CalculateDamageDistribution(anAC) );
        }
    }

    #[test]
    fn AttackSequenceOncePerTurnDamage()
    {
        let attackRoll  = AttackSequence(vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Normal, None), _1d8(), DamageModifier(3), None ), 1)], None         );
        let oncePerTurn = AttackSequence(vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Normal, None), _1d0(), DamageModifier(3), None ), 1)], Some(_1d8()) );

        for anAC in (0..30).map(AC)
        {
            assert_eq!( attackRoll.CalculateDamageDistribution(anAC), oncePerTurn.CalculateDamageDistribution(anAC) );
        }
    }

    #[test]
    fn AttackSequenceFakeOncePerTurnDamage()
    {
        let attacks = vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Advantage, None), _2d8(), DamageModifier(3), None ), 3)];

        let   noOncePerTurnDamage = AttackSequence(attacks.clone(), None         );
        let fakeOncePerTurnDamage = AttackSequence(attacks        , Some(_1d0()) );

        let rollRangeNoOPT   =   noOncePerTurnDamage.GetRollRange(2, 2);
        let rollRangeFakeOPT = fakeOncePerTurnDamage.GetRollRange(2, 2);

        assert_eq!(rollRangeNoOPT, rollRangeFakeOPT);

        for anAC in rollRangeNoOPT.map(AC)
        {
            assert_eq!( noOncePerTurnDamage.CalculateDamageDistribution(anAC), fakeOncePerTurnDamage.CalculateDamageDistribution(anAC) );
        }
    }

    #[test]
    #[should_panic(expected = "Cannot have an attack sequence with no attacks!")]
    fn AttackSequenceNoAttack()
    {
        let _ = AttackSequence(vec![], Some(_1d8()) );
    }

    fn TestAttackSequence( anAttackModifierDie : Die, someDamageDice : Die, aDamageModifier : DamageModifier, numAttacks : usize, aOncePerTurnDamageDie : Option<Die> )
    {
        for aRollStyle in RollStyle::iter()
        {
            let attacks : Vec<_> = (0..numAttacks).map(|i|
            {
                // If we're doing more than 1 attack we don't want them to be all identical, so
                // for all the attacks after the first one we adjust it's parameters slightly.
                // For the first attack everything is 0, so there should be no change.
                let iAsValue = Value::try_from(i).unwrap();
                let d = Die::from(0..=iAsValue);

                let attackModifierAdjustment      = iAsValue - &d;
                let damageDiceAdjustment          = d;
                let overrideCriticalHitThreshold  = Some(20 - iAsValue);
                let overrideCriticalHitDamageDice = if iAsValue == 0 { None } else { Some(_1d6()) };

                let anAttackRoller = AttackRoller::FromDie( anAttackModifierDie.clone() + attackModifierAdjustment, aRollStyle, overrideCriticalHitThreshold );

                let attack = Attack( anAttackRoller, &someDamageDice + damageDiceAdjustment, aDamageModifier, overrideCriticalHitDamageDice);

                (attack, 1)
            })
            .collect();

            let attackSequence = AttackSequence(attacks, aOncePerTurnDamageDie.clone());

            for anAC in attackSequence.GetRollRange(2, 2).map(AC)
            {
                //
                // TODO: Test it by manually calculating the damage distribution like we do for all the other classes.
                //

                // Test it by emperically rolling things.
                let RollFunction = ||
                {
                    let damage = attackSequence.Roll(anAC);

                    assert!( damage >= 0, "{} damage shouldn't be possible!", damage );

                    damage
                };

                let expectedDistribution = attackSequence.CalculateDamageDistribution(anAC);

                let expectedDamage = expectedDistribution.Mean();
                let expectedProbabilityDistribution = expectedDistribution.BuildProbabilityDistribution();

                // Magic number. Needs to be low so that the tests don't take forever to run
                // but high enough that we don't get spurious failures due to random rolling.
                let numIterations = 300_000;

                crate::unit_test_utils::TestRolling(RollFunction, numIterations, expectedDamage, &expectedProbabilityDistribution);
            }
        }
    }

    // By having many smaller tests we can run them in parallel rather than having a single monolithic test.
    mod unrolled
    {
        use super::*;

        macro_rules! UnrollTest_OncePerTurnDamage
        {
            ($attackModName:ident, $attackModDie:expr, $damageName:expr, $damageDie:expr, $damageMod:expr, $numAttacks:literal) =>
            {
                // Use the paste crate to be able to construct concatenated function names.
                paste::item!
                {
                    #[test] fn [< TestAttackSequence _ AttackMod _ $attackModName _ Damage _ $damageName _ NumAttacks _ $numAttacks _ OPT_Y >] () { TestAttackSequence( $attackModDie, $damageDie, $damageMod, $numAttacks, Some(_1d4()) ); }
                    #[test] fn [< TestAttackSequence _ AttackMod _ $attackModName _ Damage _ $damageName _ NumAttacks _ $numAttacks _ OPT_N >] () { TestAttackSequence( $attackModDie, $damageDie, $damageMod, $numAttacks, None         ); }
                }
            };
        }

        macro_rules! UnrollTest_NumAttacks
        {
            ($attackModName:ident, $attackModDie:expr, $damageName:expr, $damageDie:expr, $damageMod:expr) =>
            {
                UnrollTest_OncePerTurnDamage!( $attackModName, $attackModDie, $damageName, $damageDie, $damageMod, 1 );
                UnrollTest_OncePerTurnDamage!( $attackModName, $attackModDie, $damageName, $damageDie, $damageMod, 2 );
                UnrollTest_OncePerTurnDamage!( $attackModName, $attackModDie, $damageName, $damageDie, $damageMod, 3 );
            };
        }

        macro_rules! UnrollTest_Damage
        {
            ($attackModName:ident, $attackModDie:expr) =>
            {
                UnrollTest_NumAttacks!( $attackModName, $attackModDie, Zero____, _1d0(), DamageModifier(  0) );
                UnrollTest_NumAttacks!( $attackModName, $attackModDie, PMod____, _2d4(), DamageModifier(  3) );
                UnrollTest_NumAttacks!( $attackModName, $attackModDie, NMod____, _1d6(), DamageModifier(- 1) );
                UnrollTest_NumAttacks!( $attackModName, $attackModDie, PModHuge, _1d4(), DamageModifier( 15) );
            };
        }

        UnrollTest_Damage!( P2, Die::from(2)  );
        UnrollTest_Damage!( N3, Die::from(-3) );
        UnrollTest_Damage!( D4, _1d4()        );
    }

    fn TestDamageDistribution(anAttackSequence : &AttackSequence, anAC : AC, anExpectedMeanDamage : MeanValue, anExpectedDistribution : ProbabilityDistribution )
    {
        println!("AC {:?}", anAC);

        let calculatedDamageDistribution = anAttackSequence.CalculateDamageDistribution(anAC);

        let calculatedDamageProbabilityDistribution = calculatedDamageDistribution.BuildProbabilityDistribution();

        assert_eq!(calculatedDamageProbabilityDistribution, anExpectedDistribution);

        assert_eq!(calculatedDamageDistribution.Mean(), anExpectedMeanDamage);
    }

    #[test]
    fn DamageDistributionNormal()
    {
        let attacks = vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Normal, None), _1d2(), DamageModifier(3), None ), 1),
                           (Attack( AttackRoller::FromMod(AttackModifier(6), RollStyle::Normal, None), _1d4(), DamageModifier(1), None ), 1)];

        let attackSequence = AttackSequence(attacks, None);

        TestDamageDistribution(&attackSequence, AC( 0), MeanValue(7.8  ), flatmap!{0 =>    64./25600., 2 =>  288./25600., 3 =>  292./25600., 4 =>  872./25600., 5 =>  892./25600., 6 => 2640./25600., 7 => 5320./25600., 8 => 5517./25600., 9 => 5660./25600., 10 => 3140./25600., 11 => 480./25600., 12 => 266./25600., 13 => 120./25600., 14 => 44./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC( 1), MeanValue(7.8  ), flatmap!{0 =>    64./25600., 2 =>  288./25600., 3 =>  292./25600., 4 =>  872./25600., 5 =>  892./25600., 6 => 2640./25600., 7 => 5320./25600., 8 => 5517./25600., 9 => 5660./25600., 10 => 3140./25600., 11 => 480./25600., 12 => 266./25600., 13 => 120./25600., 14 => 44./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC( 2), MeanValue(7.8  ), flatmap!{0 =>    64./25600., 2 =>  288./25600., 3 =>  292./25600., 4 =>  872./25600., 5 =>  892./25600., 6 => 2640./25600., 7 => 5320./25600., 8 => 5517./25600., 9 => 5660./25600., 10 => 3140./25600., 11 => 480./25600., 12 => 266./25600., 13 => 120./25600., 14 => 44./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC( 3), MeanValue(7.8  ), flatmap!{0 =>    64./25600., 2 =>  288./25600., 3 =>  292./25600., 4 =>  872./25600., 5 =>  892./25600., 6 => 2640./25600., 7 => 5320./25600., 8 => 5517./25600., 9 => 5660./25600., 10 => 3140./25600., 11 => 480./25600., 12 => 266./25600., 13 => 120./25600., 14 => 44./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC( 4), MeanValue(7.8  ), flatmap!{0 =>    64./25600., 2 =>  288./25600., 3 =>  292./25600., 4 =>  872./25600., 5 =>  892./25600., 6 => 2640./25600., 7 => 5320./25600., 8 => 5517./25600., 9 => 5660./25600., 10 => 3140./25600., 11 => 480./25600., 12 => 266./25600., 13 => 120./25600., 14 => 44./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC( 5), MeanValue(7.8  ), flatmap!{0 =>    64./25600., 2 =>  288./25600., 3 =>  292./25600., 4 =>  872./25600., 5 =>  892./25600., 6 => 2640./25600., 7 => 5320./25600., 8 => 5517./25600., 9 => 5660./25600., 10 => 3140./25600., 11 => 480./25600., 12 => 266./25600., 13 => 120./25600., 14 => 44./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC( 6), MeanValue(7.8  ), flatmap!{0 =>    64./25600., 2 =>  288./25600., 3 =>  292./25600., 4 =>  872./25600., 5 =>  892./25600., 6 => 2640./25600., 7 => 5320./25600., 8 => 5517./25600., 9 => 5660./25600., 10 => 3140./25600., 11 => 480./25600., 12 => 266./25600., 13 => 120./25600., 14 => 44./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC( 7), MeanValue(7.8  ), flatmap!{0 =>    64./25600., 2 =>  288./25600., 3 =>  292./25600., 4 =>  872./25600., 5 =>  892./25600., 6 => 2640./25600., 7 => 5320./25600., 8 => 5517./25600., 9 => 5660./25600., 10 => 3140./25600., 11 => 480./25600., 12 => 266./25600., 13 => 120./25600., 14 => 44./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC( 8), MeanValue(7.575), flatmap!{0 =>   128./25600., 2 =>  576./25600., 3 =>  584./25600., 4 => 1136./25600., 5 => 1160./25600., 6 => 2512./25600., 7 => 5042./25600., 8 => 5231./25600., 9 => 5366./25600., 10 => 2982./25600., 11 => 466./25600., 12 => 256./25600., 13 => 114./25600., 14 => 42./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC( 9), MeanValue(7.175), flatmap!{0 =>   384./25600., 2 =>  816./25600., 3 =>  828./25600., 4 => 1864./25600., 5 => 1908./25600., 6 => 2288./25600., 7 => 4520./25600., 8 => 4677./25600., 9 => 4800./25600., 10 => 2680./25600., 11 => 440./25600., 12 => 242./25600., 13 => 108./25600., 14 => 40./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(10), MeanValue(6.775), flatmap!{0 =>   768./25600., 2 => 1024./25600., 3 => 1040./25600., 4 => 2496./25600., 5 => 2560./25600., 6 => 2080./25600., 7 => 4030./25600., 8 => 4155./25600., 9 => 4266./25600., 10 => 2394./25600., 11 => 414./25600., 12 => 228./25600., 13 => 102./25600., 14 => 38./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(11), MeanValue(6.375), flatmap!{0 =>  1280./25600., 2 => 1200./25600., 3 => 1220./25600., 4 => 3032./25600., 5 => 3116./25600., 6 => 1888./25600., 7 => 3572./25600., 8 => 3665./25600., 9 => 3764./25600., 10 => 2124./25600., 11 => 388./25600., 12 => 214./25600., 13 =>  96./25600., 14 => 36./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(12), MeanValue(5.975), flatmap!{0 =>  1920./25600., 2 => 1344./25600., 3 => 1368./25600., 4 => 3472./25600., 5 => 3576./25600., 6 => 1712./25600., 7 => 3146./25600., 8 => 3207./25600., 9 => 3294./25600., 10 => 1870./25600., 11 => 362./25600., 12 => 200./25600., 13 =>  90./25600., 14 => 34./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(13), MeanValue(5.575), flatmap!{0 =>  2688./25600., 2 => 1456./25600., 3 => 1484./25600., 4 => 3816./25600., 5 => 3940./25600., 6 => 1552./25600., 7 => 2752./25600., 8 => 2781./25600., 9 => 2856./25600., 10 => 1632./25600., 11 => 336./25600., 12 => 186./25600., 13 =>  84./25600., 14 => 32./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(14), MeanValue(5.175), flatmap!{0 =>  3584./25600., 2 => 1536./25600., 3 => 1568./25600., 4 => 4064./25600., 5 => 4208./25600., 6 => 1408./25600., 7 => 2390./25600., 8 => 2387./25600., 9 => 2450./25600., 10 => 1410./25600., 11 => 310./25600., 12 => 172./25600., 13 =>  78./25600., 14 => 30./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(15), MeanValue(4.775), flatmap!{0 =>  4608./25600., 2 => 1584./25600., 3 => 1620./25600., 4 => 4216./25600., 5 => 4380./25600., 6 => 1280./25600., 7 => 2060./25600., 8 => 2025./25600., 9 => 2076./25600., 10 => 1204./25600., 11 => 284./25600., 12 => 158./25600., 13 =>  72./25600., 14 => 28./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(16), MeanValue(4.375), flatmap!{0 =>  5760./25600., 2 => 1600./25600., 3 => 1640./25600., 4 => 4272./25600., 5 => 4456./25600., 6 => 1168./25600., 7 => 1762./25600., 8 => 1695./25600., 9 => 1734./25600., 10 => 1014./25600., 11 => 258./25600., 12 => 144./25600., 13 =>  66./25600., 14 => 26./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(17), MeanValue(3.975), flatmap!{0 =>  7040./25600., 2 => 1584./25600., 3 => 1628./25600., 4 => 4232./25600., 5 => 4436./25600., 6 => 1072./25600., 7 => 1496./25600., 8 => 1397./25600., 9 => 1424./25600., 10 =>  840./25600., 11 => 232./25600., 12 => 130./25600., 13 =>  60./25600., 14 => 24./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(18), MeanValue(3.575), flatmap!{0 =>  8448./25600., 2 => 1536./25600., 3 => 1584./25600., 4 => 4096./25600., 5 => 4320./25600., 6 =>  992./25600., 7 => 1262./25600., 8 => 1131./25600., 9 => 1146./25600., 10 =>  682./25600., 11 => 206./25600., 12 => 116./25600., 13 =>  54./25600., 14 => 22./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(19), MeanValue(3.175), flatmap!{0 =>  9984./25600., 2 => 1456./25600., 3 => 1508./25600., 4 => 3864./25600., 5 => 4108./25600., 6 =>  928./25600., 7 => 1060./25600., 8 =>  897./25600., 9 =>  900./25600., 10 =>  540./25600., 11 => 180./25600., 12 => 102./25600., 13 =>  48./25600., 14 => 20./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(20), MeanValue(2.775), flatmap!{0 => 11648./25600., 2 => 1344./25600., 3 => 1400./25600., 4 => 3536./25600., 5 => 3800./25600., 6 =>  880./25600., 7 =>  890./25600., 8 =>  695./25600., 9 =>  686./25600., 10 =>  414./25600., 11 => 154./25600., 12 =>  88./25600., 13 =>  42./25600., 14 => 18./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(21), MeanValue(2.375), flatmap!{0 => 13440./25600., 2 => 1200./25600., 3 => 1260./25600., 4 => 3112./25600., 5 => 3396./25600., 6 =>  848./25600., 7 =>  752./25600., 8 =>  525./25600., 9 =>  504./25600., 10 =>  304./25600., 11 => 128./25600., 12 =>  74./25600., 13 =>  36./25600., 14 => 16./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(22), MeanValue(1.975), flatmap!{0 => 15360./25600., 2 => 1024./25600., 3 => 1088./25600., 4 => 2592./25600., 5 => 2896./25600., 6 =>  832./25600., 7 =>  646./25600., 8 =>  387./25600., 9 =>  354./25600., 10 =>  210./25600., 11 => 102./25600., 12 =>  60./25600., 13 =>  30./25600., 14 => 14./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(23), MeanValue(1.575), flatmap!{0 => 17408./25600., 2 =>  816./25600., 3 =>  884./25600., 4 => 1976./25600., 5 => 2300./25600., 6 =>  832./25600., 7 =>  572./25600., 8 =>  281./25600., 9 =>  236./25600., 10 =>  132./25600., 11 =>  76./25600., 12 =>  46./25600., 13 =>  24./25600., 14 => 12./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(24), MeanValue(1.175), flatmap!{0 => 19584./25600., 2 =>  576./25600., 3 =>  648./25600., 4 => 1264./25600., 5 => 1608./25600., 6 =>  848./25600., 7 =>  530./25600., 8 =>  207./25600., 9 =>  150./25600., 10 =>   70./25600., 11 =>  50./25600., 12 =>  32./25600., 13 =>  18./25600., 14 => 10./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(25), MeanValue(0.775), flatmap!{0 => 21888./25600., 2 =>  304./25600., 3 =>  380./25600., 4 =>  456./25600., 5 =>  820./25600., 6 =>  880./25600., 7 =>  520./25600., 8 =>  165./25600., 9 =>   96./25600., 10 =>   24./25600., 11 =>  24./25600., 12 =>  18./25600., 13 =>  12./25600., 14 =>  8./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(26), MeanValue(0.6  ), flatmap!{0 => 23104./25600.,                    3 =>   76./25600., 4 =>  152./25600., 5 =>  532./25600., 6 =>  912./25600., 7 =>  532./25600., 8 =>  153./25600., 9 =>   80./25600., 10 =>    8./25600., 11 =>  12./25600., 12 =>  14./25600., 13 =>  12./25600., 14 =>  8./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(27), MeanValue(0.6  ), flatmap!{0 => 23104./25600.,                    3 =>   76./25600., 4 =>  152./25600., 5 =>  532./25600., 6 =>  912./25600., 7 =>  532./25600., 8 =>  153./25600., 9 =>   80./25600., 10 =>    8./25600., 11 =>  12./25600., 12 =>  14./25600., 13 =>  12./25600., 14 =>  8./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(28), MeanValue(0.6  ), flatmap!{0 => 23104./25600.,                    3 =>   76./25600., 4 =>  152./25600., 5 =>  532./25600., 6 =>  912./25600., 7 =>  532./25600., 8 =>  153./25600., 9 =>   80./25600., 10 =>    8./25600., 11 =>  12./25600., 12 =>  14./25600., 13 =>  12./25600., 14 =>  8./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(29), MeanValue(0.6  ), flatmap!{0 => 23104./25600.,                    3 =>   76./25600., 4 =>  152./25600., 5 =>  532./25600., 6 =>  912./25600., 7 =>  532./25600., 8 =>  153./25600., 9 =>   80./25600., 10 =>    8./25600., 11 =>  12./25600., 12 =>  14./25600., 13 =>  12./25600., 14 =>  8./25600., 15 => 4./25600., 16 => 1./25600.});
        TestDamageDistribution(&attackSequence, AC(30), MeanValue(0.6  ), flatmap!{0 => 23104./25600.,                    3 =>   76./25600., 4 =>  152./25600., 5 =>  532./25600., 6 =>  912./25600., 7 =>  532./25600., 8 =>  153./25600., 9 =>   80./25600., 10 =>    8./25600., 11 =>  12./25600., 12 =>  14./25600., 13 =>  12./25600., 14 =>  8./25600., 15 => 4./25600., 16 => 1./25600.});
    }

    #[test]
    fn DamageDistributionAdvantage()
    {
        let attacks = vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Normal   , None), _1d2(), DamageModifier(3), None), 1),
                           (Attack( AttackRoller::FromMod(AttackModifier(6), RollStyle::Advantage, None), _1d4(), DamageModifier(1), None), 1)];

        let attackSequence = AttackSequence(attacks, None);

        TestDamageDistribution(&attackSequence, AC( 0), MeanValue(8.085  ), flatmap!{0 =>     64./512000., 2 =>  5760./512000., 3 =>  5916./512000., 4 =>  6648./512000., 5 =>  6820./512000., 6 => 52496./512000., 7 => 107008./512000., 8 => 112563./512000., 9 => 116772./512000., 10 => 67740./512000., 11 => 14616./512000., 12 => 9006./512000., 13 => 4680./512000., 14 => 1716./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC( 1), MeanValue(8.085  ), flatmap!{0 =>     64./512000., 2 =>  5760./512000., 3 =>  5916./512000., 4 =>  6648./512000., 5 =>  6820./512000., 6 => 52496./512000., 7 => 107008./512000., 8 => 112563./512000., 9 => 116772./512000., 10 => 67740./512000., 11 => 14616./512000., 12 => 9006./512000., 13 => 4680./512000., 14 => 1716./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC( 2), MeanValue(8.085  ), flatmap!{0 =>     64./512000., 2 =>  5760./512000., 3 =>  5916./512000., 4 =>  6648./512000., 5 =>  6820./512000., 6 => 52496./512000., 7 => 107008./512000., 8 => 112563./512000., 9 => 116772./512000., 10 => 67740./512000., 11 => 14616./512000., 12 => 9006./512000., 13 => 4680./512000., 14 => 1716./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC( 3), MeanValue(8.085  ), flatmap!{0 =>     64./512000., 2 =>  5760./512000., 3 =>  5916./512000., 4 =>  6648./512000., 5 =>  6820./512000., 6 => 52496./512000., 7 => 107008./512000., 8 => 112563./512000., 9 => 116772./512000., 10 => 67740./512000., 11 => 14616./512000., 12 => 9006./512000., 13 => 4680./512000., 14 => 1716./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC( 4), MeanValue(8.085  ), flatmap!{0 =>     64./512000., 2 =>  5760./512000., 3 =>  5916./512000., 4 =>  6648./512000., 5 =>  6820./512000., 6 => 52496./512000., 7 => 107008./512000., 8 => 112563./512000., 9 => 116772./512000., 10 => 67740./512000., 11 => 14616./512000., 12 => 9006./512000., 13 => 4680./512000., 14 => 1716./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC( 5), MeanValue(8.085  ), flatmap!{0 =>     64./512000., 2 =>  5760./512000., 3 =>  5916./512000., 4 =>  6648./512000., 5 =>  6820./512000., 6 => 52496./512000., 7 => 107008./512000., 8 => 112563./512000., 9 => 116772./512000., 10 => 67740./512000., 11 => 14616./512000., 12 => 9006./512000., 13 => 4680./512000., 14 => 1716./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC( 6), MeanValue(8.085  ), flatmap!{0 =>     64./512000., 2 =>  5760./512000., 3 =>  5916./512000., 4 =>  6648./512000., 5 =>  6820./512000., 6 => 52496./512000., 7 => 107008./512000., 8 => 112563./512000., 9 => 116772./512000., 10 => 67740./512000., 11 => 14616./512000., 12 => 9006./512000., 13 => 4680./512000., 14 => 1716./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC( 7), MeanValue(8.085  ), flatmap!{0 =>     64./512000., 2 =>  5760./512000., 3 =>  5916./512000., 4 =>  6648./512000., 5 =>  6820./512000., 6 => 52496./512000., 7 => 107008./512000., 8 => 112563./512000., 9 => 116772./512000., 10 => 67740./512000., 11 => 14616./512000., 12 => 9006./512000., 13 => 4680./512000., 14 => 1716./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC( 8), MeanValue(7.86   ), flatmap!{0 =>    128./512000., 2 => 11520./512000., 3 => 11832./512000., 4 => 12688./512000., 5 => 13016./512000., 6 => 50240./512000., 7 => 101638./512000., 8 => 106881./512000., 9 => 110778./512000., 10 => 64314./512000., 11 => 14070./512000., 12 => 8616./512000., 13 => 4446./512000., 14 => 1638./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC( 9), MeanValue(7.60875), flatmap!{0 =>    768./512000., 2 => 17136./512000., 3 => 17604./512000., 4 => 20120./512000., 5 => 20652./512000., 6 => 47696./512000., 7 =>  95536./512000., 8 => 100395./512000., 9 => 103968./512000., 10 => 60456./512000., 11 => 13488./512000., 12 => 8214./512000., 13 => 4212./512000., 14 => 1560./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(10), MeanValue(7.34   ), flatmap!{0 =>   2304./512000., 2 => 22528./512000., 3 => 23152./512000., 4 => 28096./512000., 5 => 28864./512000., 6 => 45024./512000., 7 =>  89074./512000., 8 =>  93501./512000., 9 =>  96742./512000., 10 => 56374./512000., 11 => 12882./512000., 12 => 7804./512000., 13 => 3978./512000., 14 => 1482./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(11), MeanValue(7.05375), flatmap!{0 =>   5120./512000., 2 => 27600./512000., 3 => 28380./512000., 4 => 36328./512000., 5 => 37364./512000., 6 => 42272./512000., 7 =>  82348./512000., 8 =>  86295./512000., 9 =>  89196./512000., 10 => 52116./512000., 11 => 12252./512000., 12 => 7386./512000., 13 => 3744./512000., 14 => 1404./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(12), MeanValue(6.75   ), flatmap!{0 =>   9600./512000., 2 => 32256./512000., 3 => 33192./512000., 4 => 44528./512000., 5 => 45864./512000., 6 => 39488./512000., 7 =>  75454./512000., 8 =>  78873./512000., 9 =>  81426./512000., 10 => 47730./512000., 11 => 11598./512000., 12 => 6960./512000., 13 => 3510./512000., 14 => 1326./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(13), MeanValue(6.42875), flatmap!{0 =>  16128./512000., 2 => 36400./512000., 3 => 37492./512000., 4 => 52408./512000., 5 => 54076./512000., 6 => 36720./512000., 7 =>  68488./512000., 8 =>  71331./512000., 9 =>  73528./512000., 10 => 43264./512000., 11 => 10920./512000., 12 => 6526./512000., 13 => 3276./512000., 14 => 1248./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(14), MeanValue(6.09   ), flatmap!{0 =>  25088./512000., 2 => 39936./512000., 3 => 41184./512000., 4 => 59680./512000., 5 => 61712./512000., 6 => 34016./512000., 7 =>  61546./512000., 8 =>  63765./512000., 9 =>  65598./512000., 10 => 38766./512000., 11 => 10218./512000., 12 => 6084./512000., 13 => 3042./512000., 14 => 1170./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(15), MeanValue(5.73375), flatmap!{0 =>  36864./512000., 2 => 42768./512000., 3 => 44172./512000., 4 => 66056./512000., 5 => 68484./512000., 6 => 31424./512000., 7 =>  54724./512000., 8 =>  56271./512000., 9 =>  57732./512000., 10 => 34284./512000., 11 =>  9492./512000., 12 => 5634./512000., 13 => 2808./512000., 14 => 1092./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(16), MeanValue(5.36   ), flatmap!{0 =>  51840./512000., 2 => 44800./512000., 3 => 46360./512000., 4 => 71248./512000., 5 => 74104./512000., 6 => 28992./512000., 7 =>  48118./512000., 8 =>  48945./512000., 9 =>  50026./512000., 10 => 29866./512000., 11 =>  8742./512000., 12 => 5176./512000., 13 => 2574./512000., 14 => 1014./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(17), MeanValue(4.96875), flatmap!{0 =>  70400./512000., 2 => 45936./512000., 3 => 47652./512000., 4 => 74968./512000., 5 => 78284./512000., 6 => 26768./512000., 7 =>  41824./512000., 8 =>  41883./512000., 9 =>  42576./512000., 10 => 25560./512000., 11 =>  7968./512000., 12 => 4710./512000., 13 => 2340./512000., 14 =>  936./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(18), MeanValue(4.56   ), flatmap!{0 =>  92928./512000., 2 => 46080./512000., 3 => 47952./512000., 4 => 76928./512000., 5 => 80736./512000., 6 => 24800./512000., 7 =>  35938./512000., 8 =>  35181./512000., 9 =>  35478./512000., 10 => 21414./512000., 11 =>  7170./512000., 12 => 4236./512000., 13 => 2106./512000., 14 =>  858./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(19), MeanValue(4.13375), flatmap!{0 => 119808./512000., 2 => 45136./512000., 3 => 47164./512000., 4 => 76840./512000., 5 => 81172./512000., 6 => 23136./512000., 7 =>  30556./512000., 8 =>  28935./512000., 9 =>  28828./512000., 10 => 17476./512000., 11 =>  6348./512000., 12 => 3754./512000., 13 => 1872./512000., 14 =>  780./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(20), MeanValue(3.69   ), flatmap!{0 => 151424./512000., 2 => 43008./512000., 3 => 45192./512000., 4 => 74416./512000., 5 => 79304./512000., 6 => 21824./512000., 7 =>  25774./512000., 8 =>  23241./512000., 9 =>  22722./512000., 10 => 13794./512000., 11 =>  5502./512000., 12 => 3264./512000., 13 => 1638./512000., 14 =>  702./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(21), MeanValue(3.22875), flatmap!{0 => 188160./512000., 2 => 39600./512000., 3 => 41940./512000., 4 => 69368./512000., 5 => 74844./512000., 6 => 20912./512000., 7 =>  21688./512000., 8 =>  18195./512000., 9 =>  17256./512000., 10 => 10416./512000., 11 =>  4632./512000., 12 => 2766./512000., 13 => 1404./512000., 14 =>  624./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(22), MeanValue(2.75   ), flatmap!{0 => 230400./512000., 2 => 34816./512000., 3 => 37312./512000., 4 => 61408./512000., 5 => 67504./512000., 6 => 20448./512000., 7 =>  18394./512000., 8 =>  13893./512000., 9 =>  12526./512000., 10 =>  7390./512000., 11 =>  3738./512000., 12 => 2260./512000., 13 => 1170./512000., 14 =>  546./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(23), MeanValue(2.25375), flatmap!{0 => 278528./512000., 2 => 28560./512000., 3 => 31212./512000., 4 => 50248./512000., 5 => 56996./512000., 6 => 20480./512000., 7 =>  15988./512000., 8 =>  10431./512000., 9 =>   8628./512000., 10 =>  4764./512000., 11 =>  2820./512000., 12 => 1746./512000., 13 =>  936./512000., 14 =>  468./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(24), MeanValue(1.74   ), flatmap!{0 => 332928./512000., 2 => 20736./512000., 3 => 23544./512000., 4 => 35600./512000., 5 => 43032./512000., 6 => 21056./512000., 7 =>  14566./512000., 8 =>   7905./512000., 9 =>   5658./512000., 10 =>  2586./512000., 11 =>  1878./512000., 12 => 1224./512000., 13 =>  702./512000., 14 =>  390./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(25), MeanValue(1.20875), flatmap!{0 => 393984./512000., 2 => 11248./512000., 3 => 14212./512000., 4 => 17176./512000., 5 => 25324./512000., 6 => 22224./512000., 7 =>  14224./512000., 8 =>   6411./512000., 9 =>   3712./512000., 10 =>   904./512000., 11 =>   912./512000., 12 =>  694./512000., 13 =>  468./512000., 14 =>  312./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(26), MeanValue(0.885  ), flatmap!{0 => 438976./512000.,                      3 =>  2964./512000., 4 =>  5928./512000., 5 => 14668./512000., 6 => 23408./512000., 7 =>  14668./512000., 8 =>   5967./512000., 9 =>   3120./512000., 10 =>   312./512000., 11 =>   468./512000., 12 =>  546./512000., 13 =>  468./512000., 14 =>  312./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(27), MeanValue(0.885  ), flatmap!{0 => 438976./512000.,                      3 =>  2964./512000., 4 =>  5928./512000., 5 => 14668./512000., 6 => 23408./512000., 7 =>  14668./512000., 8 =>   5967./512000., 9 =>   3120./512000., 10 =>   312./512000., 11 =>   468./512000., 12 =>  546./512000., 13 =>  468./512000., 14 =>  312./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(28), MeanValue(0.885  ), flatmap!{0 => 438976./512000.,                      3 =>  2964./512000., 4 =>  5928./512000., 5 => 14668./512000., 6 => 23408./512000., 7 =>  14668./512000., 8 =>   5967./512000., 9 =>   3120./512000., 10 =>   312./512000., 11 =>   468./512000., 12 =>  546./512000., 13 =>  468./512000., 14 =>  312./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(29), MeanValue(0.885  ), flatmap!{0 => 438976./512000.,                      3 =>  2964./512000., 4 =>  5928./512000., 5 => 14668./512000., 6 => 23408./512000., 7 =>  14668./512000., 8 =>   5967./512000., 9 =>   3120./512000., 10 =>   312./512000., 11 =>   468./512000., 12 =>  546./512000., 13 =>  468./512000., 14 =>  312./512000., 15 => 156./512000., 16 => 39./512000.});
        TestDamageDistribution(&attackSequence, AC(30), MeanValue(0.885  ), flatmap!{0 => 438976./512000.,                      3 =>  2964./512000., 4 =>  5928./512000., 5 => 14668./512000., 6 => 23408./512000., 7 =>  14668./512000., 8 =>   5967./512000., 9 =>   3120./512000., 10 =>   312./512000., 11 =>   468./512000., 12 =>  546./512000., 13 =>  468./512000., 14 =>  312./512000., 15 => 156./512000., 16 => 39./512000.});
    }

    #[test]
    fn DamageDistributionDisadvantage()
    {
        let attacks = vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Disadvantage, None), _1d2(), DamageModifier(3), None ), 1),
                           (Attack( AttackRoller::FromMod(AttackModifier(6), RollStyle::Normal      , None), _1d4(), DamageModifier(1), None ), 1)];

        let attackSequence = AttackSequence(attacks, None);

        TestDamageDistribution(&attackSequence, AC( 0), MeanValue(7.515  ), flatmap!{0 =>   2496./512000., 2 => 11232./512000., 3 => 11388./512000., 4 => 23064./512000., 5 => 23236./512000., 6 => 52496./512000., 7 => 104956./512000., 8 => 106369./512000., 9 => 107728./512000., 10 => 57176./512000., 11 => 5268./512000., 12 => 3686./512000., 13 => 2172./512000., 14 => 728./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC( 1), MeanValue(7.515  ), flatmap!{0 =>   2496./512000., 2 => 11232./512000., 3 => 11388./512000., 4 => 23064./512000., 5 => 23236./512000., 6 => 52496./512000., 7 => 104956./512000., 8 => 106369./512000., 9 => 107728./512000., 10 => 57176./512000., 11 => 5268./512000., 12 => 3686./512000., 13 => 2172./512000., 14 => 728./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC( 2), MeanValue(7.515  ), flatmap!{0 =>   2496./512000., 2 => 11232./512000., 3 => 11388./512000., 4 => 23064./512000., 5 => 23236./512000., 6 => 52496./512000., 7 => 104956./512000., 8 => 106369./512000., 9 => 107728./512000., 10 => 57176./512000., 11 => 5268./512000., 12 => 3686./512000., 13 => 2172./512000., 14 => 728./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC( 3), MeanValue(7.515  ), flatmap!{0 =>   2496./512000., 2 => 11232./512000., 3 => 11388./512000., 4 => 23064./512000., 5 => 23236./512000., 6 => 52496./512000., 7 => 104956./512000., 8 => 106369./512000., 9 => 107728./512000., 10 => 57176./512000., 11 => 5268./512000., 12 => 3686./512000., 13 => 2172./512000., 14 => 728./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC( 4), MeanValue(7.515  ), flatmap!{0 =>   2496./512000., 2 => 11232./512000., 3 => 11388./512000., 4 => 23064./512000., 5 => 23236./512000., 6 => 52496./512000., 7 => 104956./512000., 8 => 106369./512000., 9 => 107728./512000., 10 => 57176./512000., 11 => 5268./512000., 12 => 3686./512000., 13 => 2172./512000., 14 => 728./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC( 5), MeanValue(7.515  ), flatmap!{0 =>   2496./512000., 2 => 11232./512000., 3 => 11388./512000., 4 => 23064./512000., 5 => 23236./512000., 6 => 52496./512000., 7 => 104956./512000., 8 => 106369./512000., 9 => 107728./512000., 10 => 57176./512000., 11 => 5268./512000., 12 => 3686./512000., 13 => 2172./512000., 14 => 728./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC( 6), MeanValue(7.515  ), flatmap!{0 =>   2496./512000., 2 => 11232./512000., 3 => 11388./512000., 4 => 23064./512000., 5 => 23236./512000., 6 => 52496./512000., 7 => 104956./512000., 8 => 106369./512000., 9 => 107728./512000., 10 => 57176./512000., 11 => 5268./512000., 12 => 3686./512000., 13 => 2172./512000., 14 => 728./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC( 7), MeanValue(7.515  ), flatmap!{0 =>   2496./512000., 2 => 11232./512000., 3 => 11388./512000., 4 => 23064./512000., 5 => 23236./512000., 6 => 52496./512000., 7 => 104956./512000., 8 => 106369./512000., 9 => 107728./512000., 10 => 57176./512000., 11 => 5268./512000., 12 => 3686./512000., 13 => 2172./512000., 14 => 728./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC( 8), MeanValue(7.09875), flatmap!{0 =>   4864./512000., 2 => 21888./512000., 3 => 22192./512000., 4 => 32832./512000., 5 => 33152./512000., 6 => 47760./512000., 7 =>  94670./512000., 8 =>  95787./512000., 9 =>  96850./512000., 10 => 51330./512000., 11 => 4750./512000., 12 => 3316./512000., 13 => 1950./512000., 14 => 654./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC( 9), MeanValue(6.53   ), flatmap!{0 =>  14208./512000., 2 => 30192./512000., 3 => 30636./512000., 4 => 49512./512000., 5 => 49988./512000., 6 => 41008./512000., 7 =>  80344./512000., 8 =>  81157./512000., 9 =>  81936./512000., 10 => 43480./512000., 11 => 4248./512000., 12 => 2962./512000., 13 => 1740./512000., 14 => 584./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(10), MeanValue(5.98375), flatmap!{0 =>  27648./512000., 2 => 36864./512000., 3 => 37440./512000., 4 => 62496./512000., 5 => 63120./512000., 6 => 35040./512000., 7 =>  67630./512000., 8 =>  68155./512000., 9 =>  68666./512000., 10 => 36474./512000., 11 => 3774./512000., 12 => 2628./512000., 13 => 1542./512000., 14 => 518./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(11), MeanValue(5.46   ), flatmap!{0 =>  44800./512000., 2 => 42000./512000., 3 => 42700./512000., 4 => 72072./512000., 5 => 72836./512000., 6 => 29808./512000., 7 =>  56432./512000., 8 =>  56685./512000., 9 =>  56944./512000., 10 => 30264./512000., 11 => 3328./512000., 12 => 2314./512000., 13 => 1356./512000., 14 => 456./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(12), MeanValue(4.95875), flatmap!{0 =>  65280./512000., 2 => 45696./512000., 3 => 46512./512000., 4 => 78528./512000., 5 => 79424./512000., 6 => 25264./512000., 7 =>  46654./512000., 8 =>  46651./512000., 9 =>  46674./512000., 10 => 24802./512000., 11 => 2910./512000., 12 => 2020./512000., 13 => 1182./512000., 14 => 398./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(13), MeanValue(4.48   ), flatmap!{0 =>  88704./512000., 2 => 48048./512000., 3 => 48972./512000., 4 => 82152./512000., 5 => 83172./512000., 6 => 21360./512000., 7 =>  38200./512000., 8 =>  37957./512000., 9 =>  37760./512000., 10 => 20040./512000., 11 => 2520./512000., 12 => 1746./512000., 13 => 1020./512000., 14 => 344./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(14), MeanValue(4.02375), flatmap!{0 => 114688./512000., 2 => 49152./512000., 3 => 50176./512000., 4 => 83232./512000., 5 => 84368./512000., 6 => 18048./512000., 7 =>  30974./512000., 8 =>  30507./512000., 9 =>  30106./512000., 10 => 15930./512000., 11 => 2158./512000., 12 => 1492./512000., 13 =>  870./512000., 14 => 294./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(15), MeanValue(3.59   ), flatmap!{0 => 142848./512000., 2 => 49104./512000., 3 => 50220./512000., 4 => 82056./512000., 5 => 83300./512000., 6 => 15280./512000., 7 =>  24880./512000., 8 =>  24205./512000., 9 =>  23616./512000., 10 => 12424./512000., 11 => 1824./512000., 12 => 1258./512000., 13 =>  732./512000., 14 => 248./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(16), MeanValue(3.17875), flatmap!{0 => 172800./512000., 2 => 48000./512000., 3 => 49200./512000., 4 => 78912./512000., 5 => 80256./512000., 6 => 13008./512000., 7 =>  19822./512000., 8 =>  18955./512000., 9 =>  18194./512000., 10 =>  9474./512000., 11 => 1518./512000., 12 => 1044./512000., 13 =>  606./512000., 14 => 206./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(17), MeanValue(2.79   ), flatmap!{0 => 204160./512000., 2 => 45936./512000., 3 => 47212./512000., 4 => 74088./512000., 5 => 75524./512000., 6 => 11184./512000., 7 =>  15704./512000., 8 =>  14661./512000., 9 =>  13744./512000., 10 =>  7032./512000., 11 => 1240./512000., 12 =>  850./512000., 13 =>  492./512000., 14 => 168./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(18), MeanValue(2.42375), flatmap!{0 => 236544./512000., 2 => 43008./512000., 3 => 44352./512000., 4 => 67872./512000., 5 => 69392./512000., 6 =>  9760./512000., 7 =>  12430./512000., 8 =>  11227./512000., 9 =>  10170./512000., 10 =>  5050./512000., 11 =>  990./512000., 12 =>  676./512000., 13 =>  390./512000., 14 => 134./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(19), MeanValue(2.08   ), flatmap!{0 => 269568./512000., 2 => 39312./512000., 3 => 40716./512000., 4 => 60552./512000., 5 => 62148./512000., 6 =>  8688./512000., 7 =>   9904./512000., 8 =>   8557./512000., 9 =>   7376./512000., 10 =>  3480./512000., 11 =>  768./512000., 12 =>  522./512000., 13 =>  300./512000., 14 => 104./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(20), MeanValue(1.75875), flatmap!{0 => 302848./512000., 2 => 34944./512000., 3 => 36400./512000., 4 => 52416./512000., 5 => 54080./512000., 6 =>  7920./512000., 7 =>   8030./512000., 8 =>   6555./512000., 9 =>   5266./512000., 10 =>  2274./512000., 11 =>  574./512000., 12 =>  388./512000., 13 =>  222./512000., 14 =>  78./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(21), MeanValue(1.46   ), flatmap!{0 => 336000./512000., 2 => 30000./512000., 3 => 31500./512000., 4 => 43752./512000., 5 => 45476./512000., 6 =>  7408./512000., 7 =>   6712./512000., 8 =>   5125./512000., 9 =>   3744./512000., 10 =>  1384./512000., 11 =>  408./512000., 12 =>  274./512000., 13 =>  156./512000., 14 =>  56./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(22), MeanValue(1.18375), flatmap!{0 => 368640./512000., 2 => 24576./512000., 3 => 26112./512000., 4 => 34848./512000., 5 => 36624./512000., 6 =>  7104./512000., 7 =>   5854./512000., 8 =>   4171./512000., 9 =>   2714./512000., 10 =>   762./512000., 11 =>  270./512000., 12 =>  180./512000., 13 =>  102./512000., 14 =>  38./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(23), MeanValue(0.93   ), flatmap!{0 => 400384./512000., 2 => 18768./512000., 3 => 20332./512000., 4 => 25992./512000., 5 => 27812./512000., 6 =>  6960./512000., 7 =>   5360./512000., 8 =>   3597./512000., 9 =>   2080./512000., 10 =>   360./512000., 11 =>  160./512000., 12 =>  106./512000., 13 =>   60./512000., 14 =>  24./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(24), MeanValue(0.69875), flatmap!{0 => 430848./512000., 2 => 12672./512000., 3 => 14256./512000., 4 => 17472./512000., 5 => 19328./512000., 6 =>  6928./512000., 7 =>   5134./512000., 8 =>   3307./512000., 9 =>   1746./512000., 10 =>   130./512000., 11 =>   78./512000., 12 =>   52./512000., 13 =>   30./512000., 14 =>  14./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(25), MeanValue(0.49   ), flatmap!{0 => 459648./512000., 2 =>  6384./512000., 3 =>  7980./512000., 4 =>  9576./512000., 5 => 11460./512000., 6 =>  6960./512000., 7 =>   5080./512000., 8 =>   3205./512000., 9 =>   1616./512000., 10 =>    24./512000., 11 =>   24./512000., 12 =>   18./512000., 13 =>   12./512000., 14 =>   8./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(26), MeanValue(0.315  ), flatmap!{0 => 485184./512000.,                      3 =>  1596./512000., 4 =>  3192./512000., 5 =>  5092./512000., 6 =>  6992./512000., 7 =>   5092./512000., 8 =>   3193./512000., 9 =>   1600./512000., 10 =>     8./512000., 11 =>   12./512000., 12 =>   14./512000., 13 =>   12./512000., 14 =>   8./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(27), MeanValue(0.315  ), flatmap!{0 => 485184./512000.,                      3 =>  1596./512000., 4 =>  3192./512000., 5 =>  5092./512000., 6 =>  6992./512000., 7 =>   5092./512000., 8 =>   3193./512000., 9 =>   1600./512000., 10 =>     8./512000., 11 =>   12./512000., 12 =>   14./512000., 13 =>   12./512000., 14 =>   8./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(28), MeanValue(0.315  ), flatmap!{0 => 485184./512000.,                      3 =>  1596./512000., 4 =>  3192./512000., 5 =>  5092./512000., 6 =>  6992./512000., 7 =>   5092./512000., 8 =>   3193./512000., 9 =>   1600./512000., 10 =>     8./512000., 11 =>   12./512000., 12 =>   14./512000., 13 =>   12./512000., 14 =>   8./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(29), MeanValue(0.315  ), flatmap!{0 => 485184./512000.,                      3 =>  1596./512000., 4 =>  3192./512000., 5 =>  5092./512000., 6 =>  6992./512000., 7 =>   5092./512000., 8 =>   3193./512000., 9 =>   1600./512000., 10 =>     8./512000., 11 =>   12./512000., 12 =>   14./512000., 13 =>   12./512000., 14 =>   8./512000., 15 => 4./512000., 16 => 1./512000.});
        TestDamageDistribution(&attackSequence, AC(30), MeanValue(0.315  ), flatmap!{0 => 485184./512000.,                      3 =>  1596./512000., 4 =>  3192./512000., 5 =>  5092./512000., 6 =>  6992./512000., 7 =>   5092./512000., 8 =>   3193./512000., 9 =>   1600./512000., 10 =>     8./512000., 11 =>   12./512000., 12 =>   14./512000., 13 =>   12./512000., 14 =>   8./512000., 15 => 4./512000., 16 => 1./512000.});
    }

    #[test]
    fn DamageDistributionOncePerTurnNormal()
    {
        let attacks = vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Normal, None), _1d2(), DamageModifier(3), None), 1),
                           (Attack( AttackRoller::FromMod(AttackModifier(6), RollStyle::Normal, None), _1d4(), DamageModifier(1), None), 1)];

        let attackSequence = AttackSequence(attacks, Some(_1d2()));

        TestDamageDistribution(&attackSequence, AC( 0), MeanValue(9.375  ), flatmap!{0 =>   256./102400., 3 =>  576./102400., 4 => 1152./102400., 5 =>  2308./102400., 6 =>  3472./102400., 7 =>  6960./102400., 8 => 15736./102400., 9 => 21248./102400., 10 => 21785./102400., 11 => 17262./102400., 12 => 7305./102400., 13 => 1980./102400., 14 => 1414./102400., 15 => 700./102400., 16 => 190./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC( 1), MeanValue(9.375  ), flatmap!{0 =>   256./102400., 3 =>  576./102400., 4 => 1152./102400., 5 =>  2308./102400., 6 =>  3472./102400., 7 =>  6960./102400., 8 => 15736./102400., 9 => 21248./102400., 10 => 21785./102400., 11 => 17262./102400., 12 => 7305./102400., 13 => 1980./102400., 14 => 1414./102400., 15 => 700./102400., 16 => 190./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC( 2), MeanValue(9.375  ), flatmap!{0 =>   256./102400., 3 =>  576./102400., 4 => 1152./102400., 5 =>  2308./102400., 6 =>  3472./102400., 7 =>  6960./102400., 8 => 15736./102400., 9 => 21248./102400., 10 => 21785./102400., 11 => 17262./102400., 12 => 7305./102400., 13 => 1980./102400., 14 => 1414./102400., 15 => 700./102400., 16 => 190./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC( 3), MeanValue(9.375  ), flatmap!{0 =>   256./102400., 3 =>  576./102400., 4 => 1152./102400., 5 =>  2308./102400., 6 =>  3472./102400., 7 =>  6960./102400., 8 => 15736./102400., 9 => 21248./102400., 10 => 21785./102400., 11 => 17262./102400., 12 => 7305./102400., 13 => 1980./102400., 14 => 1414./102400., 15 => 700./102400., 16 => 190./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC( 4), MeanValue(9.375  ), flatmap!{0 =>   256./102400., 3 =>  576./102400., 4 => 1152./102400., 5 =>  2308./102400., 6 =>  3472./102400., 7 =>  6960./102400., 8 => 15736./102400., 9 => 21248./102400., 10 => 21785./102400., 11 => 17262./102400., 12 => 7305./102400., 13 => 1980./102400., 14 => 1414./102400., 15 => 700./102400., 16 => 190./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC( 5), MeanValue(9.375  ), flatmap!{0 =>   256./102400., 3 =>  576./102400., 4 => 1152./102400., 5 =>  2308./102400., 6 =>  3472./102400., 7 =>  6960./102400., 8 => 15736./102400., 9 => 21248./102400., 10 => 21785./102400., 11 => 17262./102400., 12 => 7305./102400., 13 => 1980./102400., 14 => 1414./102400., 15 => 700./102400., 16 => 190./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC( 6), MeanValue(9.375  ), flatmap!{0 =>   256./102400., 3 =>  576./102400., 4 => 1152./102400., 5 =>  2308./102400., 6 =>  3472./102400., 7 =>  6960./102400., 8 => 15736./102400., 9 => 21248./102400., 10 => 21785./102400., 11 => 17262./102400., 12 => 7305./102400., 13 => 1980./102400., 14 => 1414./102400., 15 => 700./102400., 16 => 190./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC( 7), MeanValue(9.375  ), flatmap!{0 =>   256./102400., 3 =>  576./102400., 4 => 1152./102400., 5 =>  2308./102400., 6 =>  3472./102400., 7 =>  6960./102400., 8 => 15736./102400., 9 => 21248./102400., 10 => 21785./102400., 11 => 17262./102400., 12 => 7305./102400., 13 => 1980./102400., 14 => 1414./102400., 15 => 700./102400., 16 => 190./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC( 8), MeanValue(9.15   ), flatmap!{0 =>   512./102400., 3 => 1152./102400., 4 => 2304./102400., 5 =>  3400./102400., 6 =>  4512./102400., 7 =>  7216./102400., 8 => 14916./102400., 9 => 20136./102400., 10 => 20649./102400., 11 => 16382./102400., 12 => 6977./102400., 13 => 1936./102400., 14 => 1382./102400., 15 => 684./102400., 16 => 186./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC( 9), MeanValue(8.73875), flatmap!{0 =>  1536./102400., 3 => 1632./102400., 4 => 3264./102400., 5 =>  5324./102400., 6 =>  7408./102400., 7 =>  8160./102400., 8 => 13392./102400., 9 => 18092./102400., 10 => 18533./102400., 11 => 14706./102400., 12 => 6333./102400., 13 => 1832./102400., 14 => 1306./102400., 15 => 648./102400., 16 => 178./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(10), MeanValue(8.32   ), flatmap!{0 =>  3072./102400., 3 => 2048./102400., 4 => 4096./102400., 5 =>  6992./102400., 6 =>  9920./102400., 7 =>  8944./102400., 8 => 11964./102400., 9 => 16176./102400., 10 => 16545./102400., 11 => 13126./102400., 12 => 5721./102400., 13 => 1728./102400., 14 => 1230./102400., 15 => 612./102400., 16 => 170./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(11), MeanValue(7.89375), flatmap!{0 =>  5120./102400., 3 => 2400./102400., 4 => 4800./102400., 5 =>  8404./102400., 6 => 12048./102400., 7 =>  9568./102400., 8 => 10632./102400., 9 => 14388./102400., 10 => 14685./102400., 11 => 11642./102400., 12 => 5141./102400., 13 => 1624./102400., 14 => 1154./102400., 15 => 576./102400., 16 => 162./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(12), MeanValue(7.46   ), flatmap!{0 =>  7680./102400., 3 => 2688./102400., 4 => 5376./102400., 5 =>  9560./102400., 6 => 13792./102400., 7 => 10032./102400., 8 =>  9396./102400., 9 => 12728./102400., 10 => 12953./102400., 11 => 10254./102400., 12 => 4593./102400., 13 => 1520./102400., 14 => 1078./102400., 15 => 540./102400., 16 => 154./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(13), MeanValue(7.01875), flatmap!{0 => 10752./102400., 3 => 2912./102400., 4 => 5824./102400., 5 => 10460./102400., 6 => 15152./102400., 7 => 10336./102400., 8 =>  8256./102400., 9 => 11196./102400., 10 => 11349./102400., 11 =>  8962./102400., 12 => 4077./102400., 13 => 1416./102400., 14 => 1002./102400., 15 => 504./102400., 16 => 146./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(14), MeanValue(6.57   ), flatmap!{0 => 14336./102400., 3 => 3072./102400., 4 => 6144./102400., 5 => 11104./102400., 6 => 16128./102400., 7 => 10480./102400., 8 =>  7212./102400., 9 =>  9792./102400., 10 =>  9873./102400., 11 =>  7766./102400., 12 => 3593./102400., 13 => 1312./102400., 14 =>  926./102400., 15 => 468./102400., 16 => 138./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(15), MeanValue(6.11375), flatmap!{0 => 18432./102400., 3 => 3168./102400., 4 => 6336./102400., 5 => 11492./102400., 6 => 16720./102400., 7 => 10464./102400., 8 =>  6264./102400., 9 =>  8516./102400., 10 =>  8525./102400., 11 =>  6666./102400., 12 => 3141./102400., 13 => 1208./102400., 14 =>  850./102400., 15 => 432./102400., 16 => 130./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(16), MeanValue(5.65   ), flatmap!{0 => 23040./102400., 3 => 3200./102400., 4 => 6400./102400., 5 => 11624./102400., 6 => 16928./102400., 7 => 10288./102400., 8 =>  5412./102400., 9 =>  7368./102400., 10 =>  7305./102400., 11 =>  5662./102400., 12 => 2721./102400., 13 => 1104./102400., 14 =>  774./102400., 15 => 396./102400., 16 => 122./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(17), MeanValue(5.17875), flatmap!{0 => 28160./102400., 3 => 3168./102400., 4 => 6336./102400., 5 => 11500./102400., 6 => 16752./102400., 7 =>  9952./102400., 8 =>  4656./102400., 9 =>  6348./102400., 10 =>  6213./102400., 11 =>  4754./102400., 12 => 2333./102400., 13 => 1000./102400., 14 =>  698./102400., 15 => 360./102400., 16 => 114./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(18), MeanValue(4.7    ), flatmap!{0 => 33792./102400., 3 => 3072./102400., 4 => 6144./102400., 5 => 11120./102400., 6 => 16192./102400., 7 =>  9456./102400., 8 =>  3996./102400., 9 =>  5456./102400., 10 =>  5249./102400., 11 =>  3942./102400., 12 => 1977./102400., 13 =>  896./102400., 14 =>  622./102400., 15 => 324./102400., 16 => 106./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(19), MeanValue(4.21375), flatmap!{0 => 39936./102400., 3 => 2912./102400., 4 => 5824./102400., 5 => 10484./102400., 6 => 15248./102400., 7 =>  8800./102400., 8 =>  3432./102400., 9 =>  4692./102400., 10 =>  4413./102400., 11 =>  3226./102400., 12 => 1653./102400., 13 =>  792./102400., 14 =>  546./102400., 15 => 288./102400., 16 =>  98./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(20), MeanValue(3.72   ), flatmap!{0 => 46592./102400., 3 => 2688./102400., 4 => 5376./102400., 5 =>  9592./102400., 6 => 13920./102400., 7 =>  7984./102400., 8 =>  2964./102400., 9 =>  4056./102400., 10 =>  3705./102400., 11 =>  2606./102400., 12 => 1361./102400., 13 =>  688./102400., 14 =>  470./102400., 15 => 252./102400., 16 =>  90./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(21), MeanValue(3.21875), flatmap!{0 => 53760./102400., 3 => 2400./102400., 4 => 4800./102400., 5 =>  8444./102400., 6 => 12208./102400., 7 =>  7008./102400., 8 =>  2592./102400., 9 =>  3548./102400., 10 =>  3125./102400., 11 =>  2082./102400., 12 => 1101./102400., 13 =>  584./102400., 14 =>  394./102400., 15 => 216./102400., 16 =>  82./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(22), MeanValue(2.71   ), flatmap!{0 => 61440./102400., 3 => 2048./102400., 4 => 4096./102400., 5 =>  7040./102400., 6 => 10112./102400., 7 =>  5872./102400., 8 =>  2316./102400., 9 =>  3168./102400., 10 =>  2673./102400., 11 =>  1654./102400., 12 =>  873./102400., 13 =>  480./102400., 14 =>  318./102400., 15 => 180./102400., 16 =>  74./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(23), MeanValue(2.19375), flatmap!{0 => 69632./102400., 3 => 1632./102400., 4 => 3264./102400., 5 =>  5380./102400., 6 =>  7632./102400., 7 =>  4576./102400., 8 =>  2136./102400., 9 =>  2916./102400., 10 =>  2349./102400., 11 =>  1322./102400., 12 =>  677./102400., 13 =>  376./102400., 14 =>  242./102400., 15 => 144./102400., 16 =>  66./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(24), MeanValue(1.67   ), flatmap!{0 => 78336./102400., 3 => 1152./102400., 4 => 2304./102400., 5 =>  3464./102400., 6 =>  4768./102400., 7 =>  3120./102400., 8 =>  2052./102400., 9 =>  2792./102400., 10 =>  2153./102400., 11 =>  1086./102400., 12 =>  513./102400., 13 =>  272./102400., 14 =>  166./102400., 15 => 108./102400., 16 =>  58./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(25), MeanValue(1.13875), flatmap!{0 => 87552./102400., 3 =>  608./102400., 4 => 1216./102400., 5 =>  1292./102400., 6 =>  1520./102400., 7 =>  1504./102400., 8 =>  2064./102400., 9 =>  2796./102400., 10 =>  2085./102400., 11 =>   946./102400., 12 =>  381./102400., 13 =>  168./102400., 14 =>   90./102400., 15 =>  72./102400., 16 =>  50./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(26), MeanValue(0.8925 ), flatmap!{0 => 92416./102400.,                                         5 =>    76./102400., 6 =>   304./102400., 7 =>   912./102400., 8 =>  2128./102400., 9 =>  2888./102400., 10 =>  2129./102400., 11 =>   918./102400., 12 =>  321./102400., 13 =>  108./102400., 14 =>   46./102400., 15 =>  52./102400., 16 =>  46./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(27), MeanValue(0.8925 ), flatmap!{0 => 92416./102400.,                                         5 =>    76./102400., 6 =>   304./102400., 7 =>   912./102400., 8 =>  2128./102400., 9 =>  2888./102400., 10 =>  2129./102400., 11 =>   918./102400., 12 =>  321./102400., 13 =>  108./102400., 14 =>   46./102400., 15 =>  52./102400., 16 =>  46./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(28), MeanValue(0.8925 ), flatmap!{0 => 92416./102400.,                                         5 =>    76./102400., 6 =>   304./102400., 7 =>   912./102400., 8 =>  2128./102400., 9 =>  2888./102400., 10 =>  2129./102400., 11 =>   918./102400., 12 =>  321./102400., 13 =>  108./102400., 14 =>   46./102400., 15 =>  52./102400., 16 =>  46./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(29), MeanValue(0.8925 ), flatmap!{0 => 92416./102400.,                                         5 =>    76./102400., 6 =>   304./102400., 7 =>   912./102400., 8 =>  2128./102400., 9 =>  2888./102400., 10 =>  2129./102400., 11 =>   918./102400., 12 =>  321./102400., 13 =>  108./102400., 14 =>   46./102400., 15 =>  52./102400., 16 =>  46./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
        TestDamageDistribution(&attackSequence, AC(30), MeanValue(0.8925 ), flatmap!{0 => 92416./102400.,                                         5 =>    76./102400., 6 =>   304./102400., 7 =>   912./102400., 8 =>  2128./102400., 9 =>  2888./102400., 10 =>  2129./102400., 11 =>   918./102400., 12 =>  321./102400., 13 =>  108./102400., 14 =>   46./102400., 15 =>  52./102400., 16 =>  46./102400., 17 => 32./102400., 18 => 17./102400., 19 => 6./102400., 20 => 1./102400.});
    }

    #[test]
    fn DamageDistributionOncePerTurnAdvantage()
    {
        let attacks = vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Normal   , None), _1d2(), DamageModifier(3), None ), 1),
                           (Attack( AttackRoller::FromMod(AttackModifier(6), RollStyle::Advantage, None), _1d4(), DamageModifier(1), None ), 1)];

        let attackSequence = AttackSequence(attacks, Some(_1d2()));

        TestDamageDistribution(&attackSequence, AC( 0), MeanValue(9.667125 ), flatmap!{0 =>     256./2048000., 3 => 11520./2048000., 4 =>  23040./2048000., 5 =>  24348./2048000., 6 =>  25968./2048000., 7 => 117616./2048000., 8 => 315784./2048000., 9 => 429672./2048000., 10 => 446359./2048000., 11 => 362074./2048000., 12 => 165879./2048000., 13 => 56700./2048000., 14 => 40098./2048000., 15 => 20460./2048000., 16 => 6042./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC( 1), MeanValue(9.667125 ), flatmap!{0 =>     256./2048000., 3 => 11520./2048000., 4 =>  23040./2048000., 5 =>  24348./2048000., 6 =>  25968./2048000., 7 => 117616./2048000., 8 => 315784./2048000., 9 => 429672./2048000., 10 => 446359./2048000., 11 => 362074./2048000., 12 => 165879./2048000., 13 => 56700./2048000., 14 => 40098./2048000., 15 => 20460./2048000., 16 => 6042./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC( 2), MeanValue(9.667125 ), flatmap!{0 =>     256./2048000., 3 => 11520./2048000., 4 =>  23040./2048000., 5 =>  24348./2048000., 6 =>  25968./2048000., 7 => 117616./2048000., 8 => 315784./2048000., 9 => 429672./2048000., 10 => 446359./2048000., 11 => 362074./2048000., 12 => 165879./2048000., 13 => 56700./2048000., 14 => 40098./2048000., 15 => 20460./2048000., 16 => 6042./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC( 3), MeanValue(9.667125 ), flatmap!{0 =>     256./2048000., 3 => 11520./2048000., 4 =>  23040./2048000., 5 =>  24348./2048000., 6 =>  25968./2048000., 7 => 117616./2048000., 8 => 315784./2048000., 9 => 429672./2048000., 10 => 446359./2048000., 11 => 362074./2048000., 12 => 165879./2048000., 13 => 56700./2048000., 14 => 40098./2048000., 15 => 20460./2048000., 16 => 6042./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC( 4), MeanValue(9.667125 ), flatmap!{0 =>     256./2048000., 3 => 11520./2048000., 4 =>  23040./2048000., 5 =>  24348./2048000., 6 =>  25968./2048000., 7 => 117616./2048000., 8 => 315784./2048000., 9 => 429672./2048000., 10 => 446359./2048000., 11 => 362074./2048000., 12 => 165879./2048000., 13 => 56700./2048000., 14 => 40098./2048000., 15 => 20460./2048000., 16 => 6042./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC( 5), MeanValue(9.667125 ), flatmap!{0 =>     256./2048000., 3 => 11520./2048000., 4 =>  23040./2048000., 5 =>  24348./2048000., 6 =>  25968./2048000., 7 => 117616./2048000., 8 => 315784./2048000., 9 => 429672./2048000., 10 => 446359./2048000., 11 => 362074./2048000., 12 => 165879./2048000., 13 => 56700./2048000., 14 => 40098./2048000., 15 => 20460./2048000., 16 => 6042./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC( 6), MeanValue(9.667125 ), flatmap!{0 =>     256./2048000., 3 => 11520./2048000., 4 =>  23040./2048000., 5 =>  24348./2048000., 6 =>  25968./2048000., 7 => 117616./2048000., 8 => 315784./2048000., 9 => 429672./2048000., 10 => 446359./2048000., 11 => 362074./2048000., 12 => 165879./2048000., 13 => 56700./2048000., 14 => 40098./2048000., 15 => 20460./2048000., 16 => 6042./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC( 7), MeanValue(9.667125 ), flatmap!{0 =>     256./2048000., 3 => 11520./2048000., 4 =>  23040./2048000., 5 =>  24348./2048000., 6 =>  25968./2048000., 7 => 117616./2048000., 8 => 315784./2048000., 9 => 429672./2048000., 10 => 446359./2048000., 11 => 362074./2048000., 12 => 165879./2048000., 13 => 56700./2048000., 14 => 40098./2048000., 15 => 20460./2048000., 16 => 6042./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC( 8), MeanValue(9.44925  ), flatmap!{0 =>     512./2048000., 3 => 23040./2048000., 4 =>  46080./2048000., 5 =>  47480./2048000., 6 =>  49504./2048000., 7 => 124560./2048000., 8 => 300220./2048000., 9 => 408192./2048000., 10 => 423943./2048000., 11 => 344170./2048000., 12 => 158559./2048000., 13 => 54984./2048000., 14 => 38850./2048000., 15 => 19836./2048000., 16 => 5886./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC( 9), MeanValue(9.2034375), flatmap!{0 =>    3072./2048000., 3 => 34272./2048000., 4 =>  68544./2048000., 5 =>  73108./2048000., 6 =>  78608./2048000., 7 => 133568./2048000., 8 => 282544./2048000., 9 => 383916./2048000., 10 => 398587./2048000., 11 => 323878./2048000., 12 => 150291./2048000., 13 => 53088./2048000., 14 => 37470./2048000., 15 => 19152./2048000., 16 => 5718./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(10), MeanValue(8.9375   ), flatmap!{0 =>    9216./2048000., 3 => 45056./2048000., 4 =>  90112./2048000., 5 =>  99376./2048000., 6 => 109888./2048000., 7 => 143312./2048000., 8 => 263844./2048000., 9 => 358288./2048000., 10 => 371783./2048000., 11 => 302378./2048000., 12 => 141519./2048000., 13 => 51072./2048000., 14 => 36002./2048000., 15 => 18428./2048000., 16 => 5542./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(11), MeanValue(8.6503125), flatmap!{0 =>   20480./2048000., 3 => 55200./2048000., 4 => 110400./2048000., 5 => 125516./2048000., 6 => 142192./2048000., 7 => 153312./2048000., 8 => 244408./2048000., 9 => 331692./2048000., 10 => 343915./2048000., 11 => 279958./2048000., 12 => 132339./2048000., 13 => 48936./2048000., 14 => 34446./2048000., 15 => 17664./2048000., 16 => 5358./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(12), MeanValue(8.34075  ), flatmap!{0 =>   38400./2048000., 3 => 64512./2048000., 4 => 129024./2048000., 5 => 150760./2048000., 6 => 174368./2048000., 7 => 163088./2048000., 8 => 224524./2048000., 9 => 304512./2048000., 10 => 315367./2048000., 11 => 256906./2048000., 12 => 122847./2048000., 13 => 46680./2048000., 14 => 32802./2048000., 15 => 16860./2048000., 16 => 5166./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(13), MeanValue(8.0076875), flatmap!{0 =>   64512./2048000., 3 => 72800./2048000., 4 => 145600./2048000., 5 => 174340./2048000., 6 => 205264./2048000., 7 => 172160./2048000., 8 => 204480./2048000., 9 => 277132./2048000., 10 => 286523./2048000., 11 => 233510./2048000., 12 => 113139./2048000., 13 => 44304./2048000., 14 => 31070./2048000., 15 => 16016./2048000., 16 => 4966./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(14), MeanValue(7.65     ), flatmap!{0 =>  100352./2048000., 3 => 79872./2048000., 4 => 159744./2048000., 5 => 195488./2048000., 6 => 233728./2048000., 7 => 180048./2048000., 8 => 184564./2048000., 9 => 249936./2048000., 10 => 257767./2048000., 11 => 210058./2048000., 12 => 103311./2048000., 13 => 41808./2048000., 14 => 29250./2048000., 15 => 15132./2048000., 16 => 4758./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(15), MeanValue(7.2665625), flatmap!{0 =>  147456./2048000., 3 => 85536./2048000., 4 => 171072./2048000., 5 => 213436./2048000., 6 => 258608./2048000., 7 => 186272./2048000., 8 => 165064./2048000., 9 => 223308./2048000., 10 => 229483./2048000., 11 => 186838./2048000., 12 =>  93459./2048000., 13 => 39192./2048000., 14 => 27342./2048000., 15 => 14208./2048000., 16 => 4542./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(16), MeanValue(6.85625  ), flatmap!{0 =>  207360./2048000., 3 => 89600./2048000., 4 => 179200./2048000., 5 => 227416./2048000., 6 => 278752./2048000., 7 => 190352./2048000., 8 => 146268./2048000., 9 => 197632./2048000., 10 => 202055./2048000., 11 => 164138./2048000., 12 =>  83679./2048000., 13 => 36456./2048000., 14 => 25346./2048000., 15 => 13244./2048000., 16 => 4318./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(17), MeanValue(6.4179375), flatmap!{0 =>  281600./2048000., 3 => 91872./2048000., 4 => 183744./2048000., 5 => 236660./2048000., 6 => 293008./2048000., 7 => 191808./2048000., 8 => 128464./2048000., 9 => 173292./2048000., 10 => 175867./2048000., 11 => 142246./2048000., 12 =>  74067./2048000., 13 => 33600./2048000., 14 => 23262./2048000., 15 => 12240./2048000., 16 => 4086./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(18), MeanValue(5.9505   ), flatmap!{0 =>  371712./2048000., 3 => 92160./2048000., 4 => 184320./2048000., 5 => 240400./2048000., 6 => 300224./2048000., 7 => 190160./2048000., 8 => 111940./2048000., 9 => 150672./2048000., 10 => 151303./2048000., 11 => 121450./2048000., 12 =>  64719./2048000., 13 => 30624./2048000., 14 => 21090./2048000., 15 => 11196./2048000., 16 => 3846./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(19), MeanValue(5.4528125), flatmap!{0 =>  479232./2048000., 3 => 90272./2048000., 4 => 180544./2048000., 5 => 237868./2048000., 6 => 299248./2048000., 7 => 184928./2048000., 8 =>  96984./2048000., 9 => 130156./2048000., 10 => 128747./2048000., 11 => 102038./2048000., 12 =>  55731./2048000., 13 => 27528./2048000., 14 => 18830./2048000., 15 => 10112./2048000., 16 => 3598./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(20), MeanValue(4.92375  ), flatmap!{0 =>  605696./2048000., 3 => 86016./2048000., 4 => 172032./2048000., 5 => 228296./2048000., 6 => 288928./2048000., 7 => 175632./2048000., 8 =>  83884./2048000., 9 => 112128./2048000., 10 => 108583./2048000., 11 =>  84298./2048000., 12 =>  47199./2048000., 13 => 24312./2048000., 14 => 16482./2048000., 15 =>  8988./2048000., 16 => 3342./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(21), MeanValue(4.3621875), flatmap!{0 =>  752640./2048000., 3 => 79200./2048000., 4 => 158400./2048000., 5 => 210916./2048000., 6 => 268112./2048000., 7 => 161792./2048000., 8 =>  72928./2048000., 9 =>  96972./2048000., 10 =>  91195./2048000., 11 =>  68518./2048000., 12 =>  39219./2048000., 13 => 20976./2048000., 14 => 14046./2048000., 15 =>  7824./2048000., 16 => 3078./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(22), MeanValue(3.767    ), flatmap!{0 =>  921600./2048000., 3 => 69632./2048000., 4 => 139264./2048000., 5 => 184960./2048000., 6 => 235648./2048000., 7 => 142928./2048000., 8 =>  64404./2048000., 9 =>  85072./2048000., 10 =>  76967./2048000., 11 =>  54986./2048000., 12 =>  31887./2048000., 13 => 17520./2048000., 14 => 11522./2048000., 15 =>  6620./2048000., 16 => 2806./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(23), MeanValue(3.1370625), flatmap!{0 => 1114112./2048000., 3 => 57120./2048000., 4 => 114240./2048000., 5 => 149660./2048000., 6 => 190384./2048000., 7 => 118560./2048000., 8 =>  58600./2048000., 9 =>  76812./2048000., 10 =>  66283./2048000., 11 =>  43990./2048000., 12 =>  25299./2048000., 13 => 13944./2048000., 14 =>  8910./2048000., 15 =>  5376./2048000., 16 => 2526./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(24), MeanValue(2.47125  ), flatmap!{0 => 1331712./2048000., 3 => 41472./2048000., 4 =>  82944./2048000., 5 => 104248./2048000., 6 => 131168./2048000., 7 =>  88208./2048000., 8 =>  55804./2048000., 9 =>  72576./2048000., 10 =>  59527./2048000., 11 =>  35818./2048000., 12 =>  19551./2048000., 13 => 10248./2048000., 14 =>  6210./2048000., 15 =>  4092./2048000., 16 => 2238./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(25), MeanValue(1.7684375), flatmap!{0 => 1575936./2048000., 3 => 22496./2048000., 4 =>  44992./2048000., 5 =>  47956./2048000., 6 =>  56848./2048000., 7 =>  51392./2048000., 8 =>  56304./2048000., 9 =>  72748./2048000., 10 =>  57083./2048000., 11 =>  30758./2048000., 12 =>  14739./2048000., 13 =>  6432./2048000., 14 =>  3422./2048000., 15 =>  2768./2048000., 16 => 1942./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(26), MeanValue(1.312875 ), flatmap!{0 => 1755904./2048000.,                                              5 =>   2964./2048000., 6 =>  11856./2048000., 7 =>  29488./2048000., 8 =>  58672./2048000., 9 =>  76152./2048000., 10 =>  58711./2048000., 11 =>  29722./2048000., 12 =>  12519./2048000., 13 =>  4212./2048000., 14 =>  1794./2048000., 15 =>  2028./2048000., 16 => 1794./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(27), MeanValue(1.312875 ), flatmap!{0 => 1755904./2048000.,                                              5 =>   2964./2048000., 6 =>  11856./2048000., 7 =>  29488./2048000., 8 =>  58672./2048000., 9 =>  76152./2048000., 10 =>  58711./2048000., 11 =>  29722./2048000., 12 =>  12519./2048000., 13 =>  4212./2048000., 14 =>  1794./2048000., 15 =>  2028./2048000., 16 => 1794./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(28), MeanValue(1.312875 ), flatmap!{0 => 1755904./2048000.,                                              5 =>   2964./2048000., 6 =>  11856./2048000., 7 =>  29488./2048000., 8 =>  58672./2048000., 9 =>  76152./2048000., 10 =>  58711./2048000., 11 =>  29722./2048000., 12 =>  12519./2048000., 13 =>  4212./2048000., 14 =>  1794./2048000., 15 =>  2028./2048000., 16 => 1794./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(29), MeanValue(1.312875 ), flatmap!{0 => 1755904./2048000.,                                              5 =>   2964./2048000., 6 =>  11856./2048000., 7 =>  29488./2048000., 8 =>  58672./2048000., 9 =>  76152./2048000., 10 =>  58711./2048000., 11 =>  29722./2048000., 12 =>  12519./2048000., 13 =>  4212./2048000., 14 =>  1794./2048000., 15 =>  2028./2048000., 16 => 1794./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
        TestDamageDistribution(&attackSequence, AC(30), MeanValue(1.312875 ), flatmap!{0 => 1755904./2048000.,                                              5 =>   2964./2048000., 6 =>  11856./2048000., 7 =>  29488./2048000., 8 =>  58672./2048000., 9 =>  76152./2048000., 10 =>  58711./2048000., 11 =>  29722./2048000., 12 =>  12519./2048000., 13 =>  4212./2048000., 14 =>  1794./2048000., 15 =>  2028./2048000., 16 => 1794./2048000., 17 => 1248./2048000., 18 => 663./2048000., 19 => 234./2048000., 20 => 39./2048000.});
    }

    #[test]
    fn DamageDistributionOncePerTurnDisadvantage()
    {
        let attacks = vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Disadvantage, None), _1d2(), DamageModifier(3), None), 1),
                           (Attack( AttackRoller::FromMod(AttackModifier(6), RollStyle::Normal      , None), _1d4(), DamageModifier(1), None), 1)];

        let attackSequence = AttackSequence(attacks, Some(_1d2()));

        TestDamageDistribution(&attackSequence, AC( 0), MeanValue(9.01875  ), flatmap!{0 =>    9984./2048000., 3 => 22464./2048000., 4 =>  44928./2048000., 5 =>  68124./2048000., 6 =>  91632./2048000., 7 => 150448./2048000., 8 => 314416./2048000., 9 => 422832./2048000., 10 => 428537./2048000., 11 => 330382./2048000., 12 => 125561./2048000., 13 => 18548./2048000., 14 => 12358./2048000., 15 => 6172./2048000., 16 => 1558./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC( 1), MeanValue(9.01875  ), flatmap!{0 =>    9984./2048000., 3 => 22464./2048000., 4 =>  44928./2048000., 5 =>  68124./2048000., 6 =>  91632./2048000., 7 => 150448./2048000., 8 => 314416./2048000., 9 => 422832./2048000., 10 => 428537./2048000., 11 => 330382./2048000., 12 => 125561./2048000., 13 => 18548./2048000., 14 => 12358./2048000., 15 => 6172./2048000., 16 => 1558./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC( 2), MeanValue(9.01875  ), flatmap!{0 =>    9984./2048000., 3 => 22464./2048000., 4 =>  44928./2048000., 5 =>  68124./2048000., 6 =>  91632./2048000., 7 => 150448./2048000., 8 => 314416./2048000., 9 => 422832./2048000., 10 => 428537./2048000., 11 => 330382./2048000., 12 => 125561./2048000., 13 => 18548./2048000., 14 => 12358./2048000., 15 => 6172./2048000., 16 => 1558./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC( 3), MeanValue(9.01875  ), flatmap!{0 =>    9984./2048000., 3 => 22464./2048000., 4 =>  44928./2048000., 5 =>  68124./2048000., 6 =>  91632./2048000., 7 => 150448./2048000., 8 => 314416./2048000., 9 => 422832./2048000., 10 => 428537./2048000., 11 => 330382./2048000., 12 => 125561./2048000., 13 => 18548./2048000., 14 => 12358./2048000., 15 => 6172./2048000., 16 => 1558./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC( 4), MeanValue(9.01875  ), flatmap!{0 =>    9984./2048000., 3 => 22464./2048000., 4 =>  44928./2048000., 5 =>  68124./2048000., 6 =>  91632./2048000., 7 => 150448./2048000., 8 => 314416./2048000., 9 => 422832./2048000., 10 => 428537./2048000., 11 => 330382./2048000., 12 => 125561./2048000., 13 => 18548./2048000., 14 => 12358./2048000., 15 => 6172./2048000., 16 => 1558./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC( 5), MeanValue(9.01875  ), flatmap!{0 =>    9984./2048000., 3 => 22464./2048000., 4 =>  44928./2048000., 5 =>  68124./2048000., 6 =>  91632./2048000., 7 => 150448./2048000., 8 => 314416./2048000., 9 => 422832./2048000., 10 => 428537./2048000., 11 => 330382./2048000., 12 => 125561./2048000., 13 => 18548./2048000., 14 => 12358./2048000., 15 => 6172./2048000., 16 => 1558./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC( 6), MeanValue(9.01875  ), flatmap!{0 =>    9984./2048000., 3 => 22464./2048000., 4 =>  44928./2048000., 5 =>  68124./2048000., 6 =>  91632./2048000., 7 => 150448./2048000., 8 => 314416./2048000., 9 => 422832./2048000., 10 => 428537./2048000., 11 => 330382./2048000., 12 => 125561./2048000., 13 => 18548./2048000., 14 => 12358./2048000., 15 => 6172./2048000., 16 => 1558./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC( 7), MeanValue(9.01875  ), flatmap!{0 =>    9984./2048000., 3 => 22464./2048000., 4 =>  44928./2048000., 5 =>  68124./2048000., 6 =>  91632./2048000., 7 => 150448./2048000., 8 => 314416./2048000., 9 => 422832./2048000., 10 => 428537./2048000., 11 => 330382./2048000., 12 => 125561./2048000., 13 => 18548./2048000., 14 => 12358./2048000., 15 => 6172./2048000., 16 => 1558./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC( 8), MeanValue(8.6025   ), flatmap!{0 =>   19456./2048000., 3 => 43776./2048000., 4 =>  87552./2048000., 5 => 108528./2048000., 6 => 130112./2048000., 7 => 159920./2048000., 8 => 284076./2048000., 9 => 381688./2048000., 10 => 386505./2048000., 11 => 297822./2048000., 12 => 113425./2048000., 13 => 16920./2048000., 14 => 11174./2048000., 15 => 5580./2048000., 16 => 1410./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC( 9), MeanValue(8.0129375), flatmap!{0 =>   56832./2048000., 3 => 60384./2048000., 4 => 120768./2048000., 5 => 158076./2048000., 6 => 196272./2048000., 7 => 179168./2048000., 8 => 241616./2048000., 9 => 324428./2048000., 10 => 328357./2048000., 11 => 253170./2048000., 12 =>  97277./2048000., 13 => 15320./2048000., 14 => 10010./2048000., 15 => 5000./2048000., 16 => 1266./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(10), MeanValue(7.4335   ), flatmap!{0 =>  110592./2048000., 3 => 73728./2048000., 4 => 147456./2048000., 5 => 196992./2048000., 6 => 247680./2048000., 7 => 192624./2048000., 8 => 203964./2048000., 9 => 273616./2048000., 10 => 276705./2048000., 11 => 213446./2048000., 12 =>  82841./2048000., 13 => 13808./2048000., 14 =>  8910./2048000., 15 => 4452./2048000., 16 => 1130./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(11), MeanValue(6.8653125), flatmap!{0 =>  179200./2048000., 3 => 84000./2048000., 4 => 168000./2048000., 5 => 226044./2048000., 6 => 285488./2048000., 7 => 200768./2048000., 8 => 170832./2048000., 9 => 228868./2048000., 10 => 231165./2048000., 11 => 178362./2048000., 12 =>  70021./2048000., 13 => 12384./2048000., 14 =>  7874./2048000., 15 => 3936./2048000., 16 => 1002./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(12), MeanValue(6.3095   ), flatmap!{0 =>  261120./2048000., 3 => 91392./2048000., 4 => 182784./2048000., 5 => 246000./2048000., 6 => 310848./2048000., 7 => 204080./2048000., 8 => 141932./2048000., 9 => 189800./2048000., 10 => 191353./2048000., 11 => 147630./2048000., 12 =>  58721./2048000., 13 => 11048./2048000., 14 =>  6902./2048000., 15 => 3452./2048000., 16 =>  882./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(13), MeanValue(5.7671875), flatmap!{0 =>  354816./2048000., 3 => 96096./2048000., 4 => 192192./2048000., 5 => 257628./2048000., 6 => 324912./2048000., 7 => 203040./2048000., 8 => 116976./2048000., 9 => 156028./2048000., 10 => 156885./2048000., 11 => 120962./2048000., 12 =>  48845./2048000., 13 =>  9800./2048000., 14 =>  5994./2048000., 15 => 3000./2048000., 16 =>  770./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(14), MeanValue(5.2395   ), flatmap!{0 =>  458752./2048000., 3 => 98304./2048000., 4 => 196608./2048000., 5 => 261696./2048000., 6 => 328832./2048000., 7 => 198128./2048000., 8 =>  95676./2048000., 9 => 127168./2048000., 10 => 127377./2048000., 11 =>  98070./2048000., 12 =>  40297./2048000., 13 =>  8640./2048000., 14 =>  5150./2048000., 15 => 2580./2048000., 16 =>  666./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(15), MeanValue(4.7275625), flatmap!{0 =>  571392./2048000., 3 => 98208./2048000., 4 => 196416./2048000., 5 => 258972./2048000., 6 => 323760./2048000., 7 => 189824./2048000., 8 =>  77744./2048000., 9 => 102836./2048000., 10 => 102445./2048000., 11 =>  78666./2048000., 12 =>  32981./2048000., 13 =>  7568./2048000., 14 =>  4370./2048000., 15 => 2192./2048000., 16 =>  570./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(16), MeanValue(4.2325   ), flatmap!{0 =>  691200./2048000., 3 => 96000./2048000., 4 => 192000./2048000., 5 => 250224./2048000., 6 => 310848./2048000., 7 => 178608./2048000., 8 =>  62892./2048000., 9 =>  82648./2048000., 10 =>  81705./2048000., 11 =>  62462./2048000., 12 =>  26801./2048000., 13 =>  6584./2048000., 14 =>  3654./2048000., 15 => 1836./2048000., 16 =>  482./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(17), MeanValue(3.7554375), flatmap!{0 =>  816640./2048000., 3 => 91872./2048000., 4 => 183744./2048000., 5 => 236220./2048000., 6 => 291248./2048000., 7 => 164960./2048000., 8 =>  50832./2048000., 9 =>  66220./2048000., 10 =>  64773./2048000., 11 =>  49170./2048000., 12 =>  21661./2048000., 13 =>  5688./2048000., 14 =>  3002./2048000., 15 => 1512./2048000., 16 =>  402./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(18), MeanValue(3.2975   ), flatmap!{0 =>  946176./2048000., 3 => 86016./2048000., 4 => 172032./2048000., 5 => 217728./2048000., 6 => 266112./2048000., 7 => 149360./2048000., 8 =>  41276./2048000., 9 =>  53168./2048000., 10 =>  51265./2048000., 11 =>  38502./2048000., 12 =>  17465./2048000., 13 =>  4880./2048000., 14 =>  2414./2048000., 15 => 1220./2048000., 16 =>  330./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(19), MeanValue(2.8598125), flatmap!{0 => 1078272./2048000., 3 => 78624./2048000., 4 => 157248./2048000., 5 => 195516./2048000., 6 => 236592./2048000., 7 => 132288./2048000., 8 =>  33936./2048000., 9 =>  43108./2048000., 10 =>  40797./2048000., 11 =>  30170./2048000., 12 =>  14117./2048000., 13 =>  4160./2048000., 14 =>  1890./2048000., 15 =>  960./2048000., 16 =>  266./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(20), MeanValue(2.4435   ), flatmap!{0 => 1211392./2048000., 3 => 69888./2048000., 4 => 139776./2048000., 5 => 170352./2048000., 6 => 203840./2048000., 7 => 114224./2048000., 8 =>  28524./2048000., 9 =>  35656./2048000., 10 =>  32985./2048000., 11 =>  23886./2048000., 12 =>  11521./2048000., 13 =>  3528./2048000., 14 =>  1430./2048000., 15 =>  732./2048000., 16 =>  210./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(21), MeanValue(2.0496875), flatmap!{0 => 1344000./2048000., 3 => 60000./2048000., 4 => 120000./2048000., 5 => 143004./2048000., 6 => 169008./2048000., 7 =>  95648./2048000., 8 =>  24752./2048000., 9 =>  30428./2048000., 10 =>  27445./2048000., 11 =>  19362./2048000., 12 =>   9581./2048000., 13 =>  2984./2048000., 14 =>  1034./2048000., 15 =>  536./2048000., 16 =>  162./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(22), MeanValue(1.6795   ), flatmap!{0 => 1474560./2048000., 3 => 49152./2048000., 4 =>  98304./2048000., 5 => 114240./2048000., 6 => 133248./2048000., 7 =>  77040./2048000., 8 =>  22332./2048000., 9 =>  27040./2048000., 10 =>  23793./2048000., 11 =>  16310./2048000., 12 =>   8201./2048000., 13 =>  2528./2048000., 14 =>   702./2048000., 15 =>  372./2048000., 16 =>  122./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(23), MeanValue(1.3340625), flatmap!{0 => 1601536./2048000., 3 => 37536./2048000., 4 =>  75072./2048000., 5 =>  84828./2048000., 6 =>  97712./2048000., 7 =>  58880./2048000., 8 =>  20976./2048000., 9 =>  25108./2048000., 10 =>  21645./2048000., 11 =>  14442./2048000., 12 =>   7285./2048000., 13 =>  2160./2048000., 14 =>   434./2048000., 15 =>  240./2048000., 16 =>   90./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(24), MeanValue(1.0145   ), flatmap!{0 => 1723392./2048000., 3 => 25344./2048000., 4 =>  50688./2048000., 5 =>  55536./2048000., 6 =>  63552./2048000., 7 =>  41648./2048000., 8 =>  20396./2048000., 9 =>  24248./2048000., 10 =>  20617./2048000., 11 =>  13470./2048000., 12 =>   6737./2048000., 13 =>  1880./2048000., 14 =>   230./2048000., 15 =>  140./2048000., 16 =>   66./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(25), MeanValue(0.7219375), flatmap!{0 => 1838592./2048000., 3 => 12768./2048000., 4 =>  25536./2048000., 5 =>  27132./2048000., 6 =>  31920./2048000., 7 =>  25824./2048000., 8 =>  20304./2048000., 9 =>  24076./2048000., 10 =>  20325./2048000., 11 =>  13106./2048000., 12 =>   6461./2048000., 13 =>  1688./2048000., 14 =>    90./2048000., 15 =>   72./2048000., 16 =>   50./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(26), MeanValue(0.472125 ), flatmap!{0 => 1940736./2048000.,                                              5 =>   1596./2048000., 6 =>   6384./2048000., 7 =>  13072./2048000., 8 =>  20368./2048000., 9 =>  24168./2048000., 10 =>  20369./2048000., 11 =>  13078./2048000., 12 =>   6401./2048000., 13 =>  1628./2048000., 14 =>    46./2048000., 15 =>   52./2048000., 16 =>   46./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(27), MeanValue(0.472125 ), flatmap!{0 => 1940736./2048000.,                                              5 =>   1596./2048000., 6 =>   6384./2048000., 7 =>  13072./2048000., 8 =>  20368./2048000., 9 =>  24168./2048000., 10 =>  20369./2048000., 11 =>  13078./2048000., 12 =>   6401./2048000., 13 =>  1628./2048000., 14 =>    46./2048000., 15 =>   52./2048000., 16 =>   46./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(28), MeanValue(0.472125 ), flatmap!{0 => 1940736./2048000.,                                              5 =>   1596./2048000., 6 =>   6384./2048000., 7 =>  13072./2048000., 8 =>  20368./2048000., 9 =>  24168./2048000., 10 =>  20369./2048000., 11 =>  13078./2048000., 12 =>   6401./2048000., 13 =>  1628./2048000., 14 =>    46./2048000., 15 =>   52./2048000., 16 =>   46./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(29), MeanValue(0.472125 ), flatmap!{0 => 1940736./2048000.,                                              5 =>   1596./2048000., 6 =>   6384./2048000., 7 =>  13072./2048000., 8 =>  20368./2048000., 9 =>  24168./2048000., 10 =>  20369./2048000., 11 =>  13078./2048000., 12 =>   6401./2048000., 13 =>  1628./2048000., 14 =>    46./2048000., 15 =>   52./2048000., 16 =>   46./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
        TestDamageDistribution(&attackSequence, AC(30), MeanValue(0.472125 ), flatmap!{0 => 1940736./2048000.,                                              5 =>   1596./2048000., 6 =>   6384./2048000., 7 =>  13072./2048000., 8 =>  20368./2048000., 9 =>  24168./2048000., 10 =>  20369./2048000., 11 =>  13078./2048000., 12 =>   6401./2048000., 13 =>  1628./2048000., 14 =>    46./2048000., 15 =>   52./2048000., 16 =>   46./2048000., 17 => 32./2048000., 18 => 17./2048000., 19 => 6./2048000., 20 => 1./2048000.});
    }
}
