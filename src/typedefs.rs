use crate::distribution::Value;
use serde::{Serialize, Deserialize};
use derive_more::{Display, Deref};

#[derive(Debug, Display, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Deref, Serialize, Deserialize)] pub struct AC                 (pub Value);
#[derive(Debug, Display, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Deref, Serialize, Deserialize)] pub struct DC                 (pub Value);
#[derive(Debug, Display, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Deref, Serialize, Deserialize)] pub struct SavingThrowModifier(pub Value);
#[derive(Debug, Display, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Deref, Serialize, Deserialize)] pub struct AttackModifier     (pub Value);
#[derive(Debug, Display, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Deref, Serialize, Deserialize)] pub struct DamageModifier     (pub Value);
#[derive(Debug, Display, Copy, Clone, PartialEq,     PartialOrd,            Deref, Serialize, Deserialize)] pub struct MeanValue          (pub f64);

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn Serialize()
    {
        assert_eq!( serde_json::to_string( &AC                 (-1  ) ).unwrap(), "-1"   );
        assert_eq!( serde_json::to_string( &DC                 ( 0  ) ).unwrap(),  "0"   );
        assert_eq!( serde_json::to_string( &SavingThrowModifier( 1  ) ).unwrap(),  "1"   );
        assert_eq!( serde_json::to_string( &AttackModifier     ( 2  ) ).unwrap(),  "2"   );
        assert_eq!( serde_json::to_string( &DamageModifier     ( 3  ) ).unwrap(),  "3"   );
        assert_eq!( serde_json::to_string( &MeanValue          ( 4.0) ).unwrap(),  "4.0" );
    }

    #[test]
    fn Deserialize()
    {
        assert_eq!( AC                 (-1  ), serde_json::from_str("-1"  ).unwrap() );
        assert_eq!( DC                 ( 0  ), serde_json::from_str( "0"  ).unwrap() );
        assert_eq!( SavingThrowModifier( 1  ), serde_json::from_str( "1"  ).unwrap() );
        assert_eq!( AttackModifier     ( 2  ), serde_json::from_str( "2"  ).unwrap() );
        assert_eq!( DamageModifier     ( 3  ), serde_json::from_str( "3"  ).unwrap() );
        assert_eq!( MeanValue          ( 4.0), serde_json::from_str( "4.0").unwrap() );
    }
}
