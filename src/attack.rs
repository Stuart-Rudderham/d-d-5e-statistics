use crate::dice::*;
use crate::distribution::*;
use crate::typedefs::*;
use crate::attack_roller::*;
use serde::Deserialize;

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
struct DeserializeData_Attack
{
    #[serde(flatten)] attackRoller             : AttackRoller,
                      damageDice               : Die,
                      damageMod                : DamageModifier,
                      overrideOnCritDamageDice : Option<Die>,
}

impl From<DeserializeData_Attack> for Attack
{
    fn from(data : DeserializeData_Attack) -> Self
    {
        Attack(data.attackRoller, data.damageDice, data.damageMod, data.overrideOnCritDamageDice)
    }
}

#[derive(Clone, Debug, PartialEq, Deserialize)]
#[serde(from = "DeserializeData_Attack")]
pub struct Attack
{
    attackRoller  : AttackRoller,
    damageDie     : Die, // Damage modifier is merged into the die.
    critDamageDie : Die, // Damage modifier is merged into the die, but only once.
}

impl Attack
{
    pub fn Roll( &self, anAC : AC ) -> (Value, AttackResult)
    {
        match self.attackRoller.Roll(anAC)
        {
            AttackResult::Hit         => (self.    damageDie.Roll(), AttackResult::Hit        ),
            AttackResult::CrititalHit => (self.critDamageDie.Roll(), AttackResult::CrititalHit),
            AttackResult::Miss        => (0                        , AttackResult::Miss       ),
        }
    }

    pub fn AddDamageModifier( &mut self, aDamageModifier : DamageModifier )
    {
        self.    damageDie += *aDamageModifier;
        self.critDamageDie += *aDamageModifier; // Since it's a damage modifier it's *not* doubled by a crit.
    }

    pub fn AddDamageDie( &mut self, aDamageDie : &Die )
    {
        self.    damageDie += aDamageDie;
        self.critDamageDie += aDamageDie; // Since it's a damage die it *is* doubled by a crit.
        self.critDamageDie += aDamageDie;
    }

    pub fn GetAttackRoller( &self ) -> &AttackRoller
    {
        &self.attackRoller
    }

    pub fn CalculateDamageDistributionAssumingNoMiss( &self, anAC : AC ) -> Die
    {
        let weights = &[(&self.damageDie    , self.attackRoller.CountWaysToHitNonCritical(anAC)),
                        (&self.critDamageDie, self.attackRoller.CountWaysToHitCritical   (    ))];

        Die::WeightedMerge(weights)
    }

    pub fn CalculateDamageDistribution( &self, anAC : AC ) -> Die
    {
        // TODO: Optimize, this calls CountWaysToHitNonCritical() and CountWaysToHitCritical() twice internally!
        let weights = &[(&_1d0()            , self.attackRoller.CountWaysToMiss          (anAC)),
                        (&self.damageDie    , self.attackRoller.CountWaysToHitNonCritical(anAC)),
                        (&self.critDamageDie, self.attackRoller.CountWaysToHitCritical   (    ))];

        Die::WeightedMerge(weights)
    }

    pub fn ExpectedDamage( &self, anAC : AC ) -> MeanValue
    {
        self.CalculateDamageDistribution(anAC).Mean()
    }
}

pub fn GetCritDamageDie( aNonCritDamageDie : &Die, anOverrideCritDamageDie : Option<Die> ) -> Die
{
    // Double damage dice by default.
    anOverrideCritDamageDie.unwrap_or_else(|| 2 * aNonCritDamageDie)
}

pub fn Attack( anAttackRoller : AttackRoller, mut aDamageDie : Die, aDamageModifier : DamageModifier, anOverrideCritDamageDie : Option<Die> ) -> Attack
{
    let mut critDamageDie = GetCritDamageDie(&aDamageDie, anOverrideCritDamageDie);

    // Doing negative damage makes no sense, how would the double crit damage dice work in that case?
    assert!( aDamageDie   .Min() >= 0, "Cannot have non-crit damage die do negative damage!" );
    assert!( critDamageDie.Min() >= 0, "Cannot have crit damage die do negative damage!" );

       aDamageDie += *aDamageModifier;
    critDamageDie += *aDamageModifier;

    // Can't do less than 0 damage on an attack.
       aDamageDie =    aDamageDie.ClampMin(0);
    critDamageDie = critDamageDie.ClampMin(0);

    Attack
    {
        attackRoller  : anAttackRoller,
        damageDie     : aDamageDie,
        critDamageDie,
    }
}

#[cfg(test)]
mod tests
{
    use super::*;
    use num_traits::identities::Zero;
    use strum::IntoEnumIterator;
    use more_asserts::*;

    #[test]
    fn AddDamageModifier()
    {
        let     attack1 = Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Advantage, None), _2d2(), DamageModifier(3), None );
        let mut attack2 = Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Advantage, None), _2d2(), DamageModifier(1), None );

        attack2.AddDamageModifier( DamageModifier(2) );

        assert_eq!(attack1, attack2);
    }

    #[test]
    fn AddDamageDie()
    {
        let     attack1 = Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Normal, None), _2d2(), DamageModifier(3), None );
        let mut attack2 = Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Normal, None), _1d2(), DamageModifier(3), None );

        attack2.AddDamageDie(&_1d2());

        assert_eq!(attack1, attack2);
    }

    #[test]
    fn OverrideCritDamageDice()
    {
        let attack1 = Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Normal, None), _2d2(), DamageModifier(3), None         );
        let attack2 = Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Normal, None), _2d2(), DamageModifier(3), Some(_4d2()) );

        assert_eq!(attack1, attack2);
    }

    #[test]
    fn Deserialize()
    {
        let validStrings =
        [
            ( Attack( AttackRoller::FromMod( AttackModifier(5) , RollStyle::Normal        , Some(20) ), _1d4() , DamageModifier(-3), None         ), r#"{ "attackMod": "5"        ,                                "overrideCritHitThreshold": 20, "damageDice": "1d4" ,                                    "damageMod": -3 }"# ),
            ( Attack( AttackRoller::FromMod( AttackModifier(5) , RollStyle::Normal        , None     ), _1d4() , DamageModifier(-3), None         ), r#"{ "attackMod": "5"        , "rollStyle": "Normal"        ,                                 "damageDice": "1d4" ,                                    "damageMod": -3 }"# ),
            ( Attack( AttackRoller::FromDie( _1d4()            , RollStyle::Advantage     , None     ), _2d8() , DamageModifier( 0), Some(_1d6()) ), r#"{ "attackMod": "1d4"      , "rollStyle": "Advantage"     ,                                 "damageDice": "2d8" , "overrideOnCritDamageDice": "1d6", "damageMod":  0 }"# ),
            ( Attack( AttackRoller::FromDie( _1d10() - 10      , RollStyle::Disadvantage  , Some(14) ), _1d0() , DamageModifier( 5), Some(_4d2()) ), r#"{ "attackMod": "1d10 - 10", "rollStyle": "Disadvantage"  , "overrideCritHitThreshold": 14, "damageDice": "0"   , "overrideOnCritDamageDice": "4d2", "damageMod":  5 }"# ),
            ( Attack( AttackRoller::FromMod( AttackModifier(-1), RollStyle::SuperAdvantage, None     ), _5d10(), DamageModifier(20), None         ), r#"{ "attackMod": "-1"       , "rollStyle": "SuperAdvantage",                                 "damageDice": "5d10",                                    "damageMod": 20 }"# ),
        ];

        let invalidStrings =
        [
            (r#"{                                                                                  }"#, "missing field `damageDice`"),
            (r#"{ "attackModifier": "10",                    "damageDice": "1d4" , "damageMod": -3 }"#, "missing field `attackMod`"),
            (r#"{ "attackModifier": "10", "attackMod": "10", "damageDice": "1d4" , "damageMod": -3 }"#, "unknown field `attackModifier`"),
            (r#"{                         "attackMod":  10 , "damageDice": "1d4" , "damageMod": -3 }"#, "invalid type: integer `10`, expected a string"),
        ];

        crate::unit_test_utils::TestDeserializeFromJSON( &validStrings, &invalidStrings );
    }

    fn TestAttack( anAttackModifierDie : Die, anOverrideCriticalHitThreshold : Option<Value>, someDamageDice : Die, aDamageModifier : DamageModifier, anOverrideCritDamageDie : Option<Die> )
    {
        let someCritDamageDice = GetCritDamageDie(&someDamageDice, anOverrideCritDamageDie.clone());

        let critHitThreshold = GetCriticalHitThreshold(anOverrideCriticalHitThreshold);

        for aRollStyle in RollStyle::iter()
        {
            let anAttackRoller = AttackRoller::FromDie( anAttackModifierDie.clone(), aRollStyle, anOverrideCriticalHitThreshold );

            let attack = Attack( anAttackRoller.clone(), someDamageDice.clone(), aDamageModifier, anOverrideCritDamageDie.clone() );

            for anAC in anAttackRoller.GetRollRange(-2, 2).map(AC)
            {
                // Calculate the distribution manually.
                let calculatedDamageProbabilityDistribution =
                {
                    let mut ret = CountDistribution::new();

                    // Critical hits.
                    {
                        let numWaysToHitCritical = anAttackRoller.CountWaysToHitCritical();

                        if critHitThreshold <= 20
                        {
                            assert_gt!( numWaysToHitCritical, Count::zero() );
                        }

                        if numWaysToHitCritical > Count::zero()
                        {
                            // Since we're just tracking the raw number of counts we need to "rescale" each
                            // type of attack result (hit, miss, crit hit) so that they all have the same
                            // denominator, otherwise things wouldn't be weighted properly.
                            let scaleFactor = someDamageDice.NumPermutations();

                            for (damageValue, damageCount) in someCritDamageDice.GetDistribution().iter()
                            {
                                let damage = std::cmp::max(damageValue + *aDamageModifier, 0);

                                *ret.entry(damage).or_insert_with(Count::zero) += &numWaysToHitCritical * damageCount * scaleFactor;
                            }
                        }
                    }

                    // Non-critical hits.
                    {
                        let numWaysToHitNonCritical = anAttackRoller.CountWaysToHitNonCritical(anAC);

                        if numWaysToHitNonCritical > Count::zero()
                        {
                            // Since we're just tracking the raw number of counts we need to "rescale" each
                            // type of attack result (hit, miss, crit hit) so that they all have the same
                            // denominator, otherwise things wouldn't be weighted properly.
                            let scaleFactor = someCritDamageDice.NumPermutations();

                            for (damageValue, damageCount) in someDamageDice.GetDistribution().iter()
                            {
                                let damage = std::cmp::max(damageValue + *aDamageModifier, 0);

                                *ret.entry(damage).or_insert_with(Count::zero) += &numWaysToHitNonCritical * damageCount * scaleFactor;
                            }
                        }
                    }

                    // Misses.
                    {
                        // Since we're just tracking the raw number of counts we need to "rescale" each
                        // type of attack result (hit, miss, crit hit) so that they all have the same
                        // denominator, otherwise things wouldn't be weighted properly.
                        let scaleFactor = someDamageDice.NumPermutations() * someCritDamageDice.NumPermutations();

                        let damage = 0;

                        *ret.entry(damage).or_insert_with(Count::zero) += anAttackRoller.CountWaysToMiss(anAC) * scaleFactor;
                    }

                    BuildProbabilityDistribution( &ret )
                };

                // Compare it with the closed form version, they should be identical.
                let expectedDamageCountDistribution = attack.CalculateDamageDistribution(anAC);

                let expectedDamageProbabilityDistribution = expectedDamageCountDistribution.BuildProbabilityDistribution();

                assert_eq!( calculatedDamageProbabilityDistribution, expectedDamageProbabilityDistribution );

                // Test it a second way by emperically rolling things.
                let RollFunction = ||
                {
                    let damage = attack.Roll(anAC).0;

                    assert!( damage >= 0, "{} damage shouldn't be possible!", damage );

                    damage
                };

                // Magic number. Needs to be low so that the tests don't take forever to run
                // but high enough that we don't get spurious failures due to random rolling.
                let numIterations = 75_000;

                crate::unit_test_utils::TestRolling(RollFunction, numIterations, attack.ExpectedDamage(anAC), &expectedDamageProbabilityDistribution);
            }
        }
    }

    // By having many smaller tests we can run them in parallel rather than having a single monolithic test.
    mod unrolled
    {
        use super::*;

        macro_rules! UnrollTest_CritDamageDie
        {
            ($attackModName:ident, $attackMod:expr, $overrideCritRangeName:ident, $overrideCritRange:expr, $damageDieName:ident, $damageDie:expr, $damageModName:ident, $damageMod:expr) =>
            {
                // Use the paste crate to be able to construct concatenated function names.
                paste::item!
                {
                    #[test] fn [< TestAttack_AttackMod $attackModName _ Crit $overrideCritRangeName _ DamageDie $damageDieName _ DamageMod _ $damageModName _ CritDamageDie_Yes >] () { TestAttack( $attackMod, $overrideCritRange, $damageDie, $damageMod, Some(_2d6()) ); }
                    #[test] fn [< TestAttack_AttackMod $attackModName _ Crit $overrideCritRangeName _ DamageDie $damageDieName _ DamageMod _ $damageModName _ CritDamageDie__No >] () { TestAttack( $attackMod, $overrideCritRange, $damageDie, $damageMod, None         ); }
                }
            };
        }

        macro_rules! UnrollTest_AttackMod
        {
            ($damageDieName:ident, $damageDie:expr, $damageModName:ident, $damageMod:expr) =>
            {
                UnrollTest_CritDamageDie!( _N3____, Die::from(-3), _21, Some(21), $damageDieName, $damageDie, $damageModName, $damageMod );
                UnrollTest_CritDamageDie!( _0_____, Die::from( 0), _17, Some(17), $damageDieName, $damageDie, $damageModName, $damageMod );
                UnrollTest_CritDamageDie!( _P4____, Die::from( 4), _20, None    , $damageDieName, $damageDie, $damageModName, $damageMod );
                UnrollTest_CritDamageDie!( _2d4___, _2d4()       , _20, None    , $damageDieName, $damageDie, $damageModName, $damageMod );
                UnrollTest_CritDamageDie!( _1dNeg6, -_1d6()      , __2, Some(2) , $damageDieName, $damageDie, $damageModName, $damageMod );
            };
        }

        macro_rules! UnrollTest_DamageMod
        {
            ($damageDieName:ident, $damageDie:expr) =>
            {
                UnrollTest_AttackMod!( $damageDieName, $damageDie, P0, DamageModifier( 0) );
                UnrollTest_AttackMod!( $damageDieName, $damageDie, P1, DamageModifier( 1) );
                UnrollTest_AttackMod!( $damageDieName, $damageDie, N1, DamageModifier(-1) );
                UnrollTest_AttackMod!( $damageDieName, $damageDie, P2, DamageModifier( 2) );
                UnrollTest_AttackMod!( $damageDieName, $damageDie, N2, DamageModifier(-2) );
                UnrollTest_AttackMod!( $damageDieName, $damageDie, P5, DamageModifier( 5) );
                UnrollTest_AttackMod!( $damageDieName, $damageDie, N5, DamageModifier(-5) );
            };
        }

        UnrollTest_DamageMod!( _1d0, _1d0()                                                         );
        UnrollTest_DamageMod!( _1d1, _1d1()                                                         );
        UnrollTest_DamageMod!( _1d2, _1d2()                                                         );
        UnrollTest_DamageMod!( _1d4, _1d4()                                                         );
        UnrollTest_DamageMod!( _GWF, 2 * _1d6().RerollNumbers(|value| { value == 1 || value == 2 }) );
        UnrollTest_DamageMod!( _3d4, _3d4()                                                         );
    }

    #[test]
    fn Hardcoded()
    {
        // Just some very basic tests with hardcoded numbers as a sanity check.
        let Test = |anAttackRoller : AttackRoller, anOverrideCritDamageDie : Option<Die>, anExpectedDamageProbabilityDistribution : ProbabilityDistribution|
        {
            let anAC = AC(16);

            let anAttack = Attack( anAttackRoller, _2d2(), DamageModifier(3), anOverrideCritDamageDie );

            let calculatedDamageProbabilityDistribution = anAttack.CalculateDamageDistribution(anAC).BuildProbabilityDistribution();

            assert_eq!(calculatedDamageProbabilityDistribution, anExpectedDamageProbabilityDistribution);
        };

        Test( AttackRoller::FromMod(AttackModifier( 0), RollStyle::Normal        , None    ), None        , flatmap!{ 0 =>   240./  320.,               5 =>   16./  320., 6 =>   32./  320., 7 =>   17./  320., 8 =>    4./  320., 9 =>    6./  320., 10 =>    4./  320., 11 =>   1./  320., } );
        Test( AttackRoller::FromDie( 1 + _1d2()       , RollStyle::Advantage     , None    ), None        , flatmap!{ 0 =>  5008./12800.,               5 => 1636./12800., 6 => 3272./12800., 7 => 1714./12800., 8 =>  312./12800., 9 =>  468./12800., 10 =>  312./12800., 11 =>  78./12800., } );
        Test( AttackRoller::FromDie(-1 - _1d2()       , RollStyle::Disadvantage  , None    ), None        , flatmap!{ 0 => 12592./12800.,               5 =>   44./12800., 6 =>   88./12800., 7 =>   46./12800., 8 =>    8./12800., 9 =>   12./12800., 10 =>    8./12800., 11 =>   2./12800., } );
        Test( AttackRoller::FromMod(AttackModifier( 3), RollStyle::SuperAdvantage, Some(15)), None        , flatmap!{ 0 =>  3456./16000.,               5 =>  508./16000., 6 => 1016./16000., 7 => 1165./16000., 8 => 2628./16000., 9 => 3942./16000., 10 => 2628./16000., 11 => 657./16000., } );
        Test( AttackRoller::FromMod(AttackModifier(-2), RollStyle::Normal        , None    ), Some(_1d4()), flatmap!{ 0 =>   272./  320., 4 => 4./320., 5 =>   12./  320., 6 =>   20./  320., 7 =>   12./  320.                                                                               } );
    }
}
