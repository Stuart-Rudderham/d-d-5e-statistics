#![allow(non_snake_case)]

#[macro_use]
extern crate criterion;

#[macro_use]
extern crate boolean_enums;

use criterion::Criterion;
use criterion::black_box;

extern crate dnd5e;
use dnd5e::attack::*;
use dnd5e::attack_roller::*;
use dnd5e::attack_sequence::*;
use dnd5e::check_against_dc::*;
use dnd5e::dice::*;
use dnd5e::group_check::*;
use dnd5e::saving_throw::*;
use dnd5e::spell::*;
use dnd5e::typedefs::*;

boolean_enums::gen_boolean_enum!(ShouldTestBasic);
boolean_enums::gen_boolean_enum!(ShouldTestQuery);

fn DieBenchmark(c: &mut Criterion)
{
    macro_rules! CreateDieBenchmark
    {
        ($bencher:expr, $die:expr, $shouldTestBasic:expr, $shoudTestQuery:expr) =>
        {
            // So far we've always wanted to benchmark the creation function.
            if true
            {
                // Some $die expressions are functions that return a cached value, so we only care about testing the performance of that.
                $bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::new" ), |b| {               b.iter(|| black_box($die    )) } );
            }

            if $shouldTestBasic.into()
            {
                // Basic functionality.
                $bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::roll"), |b| { let d = $die; b.iter(|| black_box(d.Roll())) } );
                $bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::mean"), |b| { let d = $die; b.iter(|| black_box(d.Mean())) } );
            }

            if $shoudTestQuery.into()
            {
                // Query against Value.
                $bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::LT_value" ), |b| { let d = $die; let value = (d.Min() + d.Max()) / 2; b.iter(|| d.CountWaysOfLT_Value(value) ); } );
                $bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::LE_value" ), |b| { let d = $die; let value = (d.Min() + d.Max()) / 2; b.iter(|| d.CountWaysOfLE_Value(value) ); } );
                $bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::EQ_value" ), |b| { let d = $die; let value = (d.Min() + d.Max()) / 2; b.iter(|| d.CountWaysOfEQ_Value(value) ); } );
                $bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::NE_value" ), |b| { let d = $die; let value = (d.Min() + d.Max()) / 2; b.iter(|| d.CountWaysOfNE_Value(value) ); } );
                $bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::GE_value" ), |b| { let d = $die; let value = (d.Min() + d.Max()) / 2; b.iter(|| d.CountWaysOfGE_Value(value) ); } );
                $bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::GT_value" ), |b| { let d = $die; let value = (d.Min() + d.Max()) / 2; b.iter(|| d.CountWaysOfGT_Value(value) ); } );

                // Query vs Die.
                // Currently disabled as they are called pretty rarely, so their performace isn't critical.
              //$bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::LT_die" ), |b| { let d = $die; b.iter(|| d.CountWaysOfLT_Die(&d) ); } );
              //$bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::LE_die" ), |b| { let d = $die; b.iter(|| d.CountWaysOfLE_Die(&d) ); } );
              //$bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::EQ_die" ), |b| { let d = $die; b.iter(|| d.CountWaysOfEQ_Die(&d) ); } );
              //$bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::NE_die" ), |b| { let d = $die; b.iter(|| d.CountWaysOfNE_Die(&d) ); } );
              //$bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::GE_die" ), |b| { let d = $die; b.iter(|| d.CountWaysOfGE_Die(&d) ); } );
              //$bencher.bench_function( concat!("Die::\"", stringify!($die), "\"::GT_die" ), |b| { let d = $die; b.iter(|| d.CountWaysOfGT_Die(&d) ); } );
            }
        }
    }

    // Test cached die functions.
    CreateDieBenchmark!( c, _1d1()                            , ShouldTestBasic::No , ShouldTestQuery::No  ); // d1
    CreateDieBenchmark!( c, _10d1()                           , ShouldTestBasic::No , ShouldTestQuery::No  );
    CreateDieBenchmark!( c, _1d6()                            , ShouldTestBasic::No , ShouldTestQuery::No  ); // d6
    CreateDieBenchmark!( c, _10d6()                           , ShouldTestBasic::No , ShouldTestQuery::No  );
    CreateDieBenchmark!( c, _1d20()                           , ShouldTestBasic::No , ShouldTestQuery::No  ); // d20
    CreateDieBenchmark!( c, _10d20()                          , ShouldTestBasic::No , ShouldTestQuery::No  );
    CreateDieBenchmark!( c, _1d20_adv()                       , ShouldTestBasic::No , ShouldTestQuery::No  ); // special d20

    // Test non-cached die functions.
    CreateDieBenchmark!( c, Die::from(1..=1)                  , ShouldTestBasic::Yes, ShouldTestQuery::Yes ); // d1
    CreateDieBenchmark!( c, Die::from(1..=1) * 10             , ShouldTestBasic::Yes, ShouldTestQuery::Yes );
    CreateDieBenchmark!( c, Die::from(1..=6)                  , ShouldTestBasic::Yes, ShouldTestQuery::Yes ); // d6
    CreateDieBenchmark!( c, Die::from(1..=6) * 10             , ShouldTestBasic::Yes, ShouldTestQuery::Yes );
    CreateDieBenchmark!( c, Die::from(1..=12)                 , ShouldTestBasic::Yes, ShouldTestQuery::Yes ); // d12
    CreateDieBenchmark!( c, Die::from(1..=12) * 10            , ShouldTestBasic::Yes, ShouldTestQuery::Yes );
    CreateDieBenchmark!( c, Die::from(1..=20)                 , ShouldTestBasic::Yes, ShouldTestQuery::Yes ); // d20
    CreateDieBenchmark!( c, Die::from(1..=20) * 10            , ShouldTestBasic::Yes, ShouldTestQuery::Yes );
    CreateDieBenchmark!( c, Die::from(1..=10_000)             , ShouldTestBasic::Yes, ShouldTestQuery::Yes ); // d10,000
    CreateDieBenchmark!( c, Die::from(1..=20).Advantage()     , ShouldTestBasic::No , ShouldTestQuery::No  ); // special d20
    CreateDieBenchmark!( c, Die::from(1..=20).Disadvantage()  , ShouldTestBasic::No , ShouldTestQuery::No  );
    CreateDieBenchmark!( c, Die::from(1..=20).SuperAdvantage(), ShouldTestBasic::No , ShouldTestQuery::No  );
    CreateDieBenchmark!( c, Die::from(1..=20) + 4             , ShouldTestBasic::No , ShouldTestQuery::No  ); // Addition
    CreateDieBenchmark!( c, Die::from(1..=20) + 1 + 1 + 1 + 1 , ShouldTestBasic::No , ShouldTestQuery::No  );
}

fn SpellBenchmark(c: &mut Criterion)
{
    macro_rules! CreateSpellBenchmark
    {
        ($bencher:expr, $name:ident, $spell:expr, $save:expr) =>
        {
            // Basic functionality.
            $bencher.bench_function( concat!("Spell::\"", stringify!($name), "\"::new" ), |b| {                                  b.iter(|| black_box($spell                )) } );
            $bencher.bench_function( concat!("Spell::\"", stringify!($name), "\"::roll"), |b| { let sp = $spell; let st = $save; b.iter(|| black_box(sp.Roll          (&st))) } );
            $bencher.bench_function( concat!("Spell::\"", stringify!($name), "\"::mean"), |b| { let sp = $spell; let st = $save; b.iter(|| black_box(sp.ExpectedDamage(&st))) } );
        }
    }

    CreateSpellBenchmark!( c, Fireball_N , Spell::New(SpellType::Normal              , _8d6(), DC(10)), SavingThrow::FromMod(RollStyle::Normal, SavingThrowModifier(3)) );
    CreateSpellBenchmark!( c, Fireball_EV, Spell::New(SpellType::NormalAgainstEvasion, _8d6(), DC(10)), SavingThrow::FromMod(RollStyle::Normal, SavingThrowModifier(3)) );
    CreateSpellBenchmark!( c, Fireball_C , Spell::New(SpellType::Cantrip             , _8d6(), DC(10)), SavingThrow::FromMod(RollStyle::Normal, SavingThrowModifier(3)) );
}

fn AttackSequenceBenchmark(c: &mut Criterion)
{
    macro_rules! CreateAttackSequenceBenchmark
    {
        ($bencher:expr, $name:ident, $attack_seq:expr) =>
        {
            // Basic functionality.
            $bencher.bench_function( concat!("AttackSequence::\"", stringify!($name), "\"::new" ), |b| {                      b.iter(|| black_box($attack_seq             )) } );
            $bencher.bench_function( concat!("AttackSequence::\"", stringify!($name), "\"::roll"), |b| { let a = $attack_seq; b.iter(|| black_box(a.Roll          (AC(10)))) } );
            $bencher.bench_function( concat!("AttackSequence::\"", stringify!($name), "\"::mean"), |b| { let a = $attack_seq; b.iter(|| black_box(a.ExpectedDamage(AC(10)))) } );
        }
    }

    CreateAttackSequenceBenchmark!( c, OneAttack     , AttackSequence(vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Normal, None), _1d8(), DamageModifier(3), None ), 1)], None        ) );
    CreateAttackSequenceBenchmark!( c, OneSneakAttack, AttackSequence(vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Normal, None), _1d8(), DamageModifier(3), None ), 1)], Some(_5d6())) );
    CreateAttackSequenceBenchmark!( c, TwoAttacks    , AttackSequence(vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Normal, None), _1d8(), DamageModifier(3), None ), 2)], None        ) );
    CreateAttackSequenceBenchmark!( c, TwoSneakAttack, AttackSequence(vec![(Attack( AttackRoller::FromMod(AttackModifier(5), RollStyle::Normal, None), _1d8(), DamageModifier(3), None ), 2)], Some(_5d6())) );
}

fn SavingThrowBenchmark(c: &mut Criterion)
{
    macro_rules! CreateSavingThrowBenchmark
    {
        ($bencher:expr, $name:ident, $saving_throw:expr) =>
        {
            // Basic functionality.
            $bencher.bench_function( concat!("SavingThrow::\"", stringify!($name), "\"::roll"   ), |b| { let st = $saving_throw; b.iter(|| black_box(st.Roll                        (DC(10)))) } );
            $bencher.bench_function( concat!("SavingThrow::\"", stringify!($name), "\"::distrib"), |b| { let st = $saving_throw; b.iter(|| black_box(st.CalculateSuccessDistribution(DC(10)))) } );
            $bencher.bench_function( concat!("SavingThrow::\"", stringify!($name), "\"::contest"), |b| { let st = $saving_throw; b.iter(|| black_box(st.CountWaysToWinContest       (&st   ))) } );
        }
    }

    CreateSavingThrowBenchmark!( c, StaticMod, SavingThrow::FromMod( RollStyle::Normal   , SavingThrowModifier(-1) ) );
    CreateSavingThrowBenchmark!( c, Bless    , SavingThrow::FromDie( RollStyle::Normal   , _1d4() + 3              ) );
    CreateSavingThrowBenchmark!( c, Advantage, SavingThrow::FromMod( RollStyle::Advantage, SavingThrowModifier(3)  ) );
}

fn GroupCheckBenchmark(c: &mut Criterion)
{
    macro_rules! CreateGroupCheckBenchmark
    {
        ($bencher:expr, $name:ident, $saving_throws:expr) =>
        {
            // Basic functionality.
            $bencher.bench_function( concat!("GroupCheck::\"", stringify!($name), "\"::roll_maj"   ), |b| { let gc = GroupCheck($saving_throws, PassCriteria::MajorityPass); b.iter(|| black_box(gc.Roll                        (DC(10)))) } );
            $bencher.bench_function( concat!("GroupCheck::\"", stringify!($name), "\"::roll_avg"   ), |b| { let gc = GroupCheck($saving_throws, PassCriteria::AverageRoll ); b.iter(|| black_box(gc.Roll                        (DC(10)))) } );
            $bencher.bench_function( concat!("GroupCheck::\"", stringify!($name), "\"::distrib_maj"), |b| { let gc = GroupCheck($saving_throws, PassCriteria::MajorityPass); b.iter(|| black_box(gc.CalculateSuccessDistribution(DC(10)))) } );
            $bencher.bench_function( concat!("GroupCheck::\"", stringify!($name), "\"::distrib_avg"), |b| { let gc = GroupCheck($saving_throws, PassCriteria::AverageRoll ); b.iter(|| black_box(gc.CalculateSuccessDistribution(DC(10)))) } );
        }
    }

    let savingThrows = vec!
    [
        SavingThrow::FromMod( RollStyle::Normal      , SavingThrowModifier(0) ),
        SavingThrow::FromMod( RollStyle::Advantage   , SavingThrowModifier(1) ),
        SavingThrow::FromMod( RollStyle::Disadvantage, SavingThrowModifier(2) ),
        SavingThrow::FromMod( RollStyle::Normal      , SavingThrowModifier(3) ),
        SavingThrow::FromMod( RollStyle::Normal      , SavingThrowModifier(4) ),
    ];

    CreateGroupCheckBenchmark!( c, PartyOf1, savingThrows[0..1].to_vec() );
    CreateGroupCheckBenchmark!( c, PartyOf3, savingThrows[0..3].to_vec() );
    CreateGroupCheckBenchmark!( c, PartyOf5, savingThrows[0..5].to_vec() );
}

fn GenerateRandomJsonBenchmark(c: &mut Criterion)
{
    use rand::SeedableRng;

    macro_rules! CreateRandomJsonBenchmark
    {
        ($bencher:expr, $name:ident, $genFn:expr) =>
        {
            $bencher.bench_function( concat!("RandomJson::\"", stringify!($name), "\"::Generate"   ), |b| { let mut rng = rand::thread_rng(); b.iter(|| black_box($genFn(&mut rng))) } );
            $bencher.bench_function( concat!("RandomJson::\"", stringify!($name), "\"::Deserialize"), |b|
            {
                // Use a fixed seed so that we're always deserializing the same input.
                // Otherwise it could change between benchmarks runs which makes comparisons useless.
                let mut rng = rand::rngs::StdRng::seed_from_u64(1234);
                let input = $genFn(&mut rng);

                b.iter(|| black_box( serde_json::from_str::<$name>(&input).unwrap() ))
            });
        }
    }

    CreateRandomJsonBenchmark!( c, Spell         , GenerateRandomJSON_Spell          );
    CreateRandomJsonBenchmark!( c, Die           , GenerateRandomJSON_Die            );
    CreateRandomJsonBenchmark!( c, SavingThrow   , GenerateRandomJSON_SavingThrow    );
    CreateRandomJsonBenchmark!( c, GroupCheck    , GenerateRandomJSON_GroupCheck     );
    CreateRandomJsonBenchmark!( c, AttackSequence, GenerateRandomJSON_AttackSequence );
}

criterion_group!
(
    benches,
    DieBenchmark,
    SpellBenchmark,
    AttackSequenceBenchmark,
    SavingThrowBenchmark,
    GroupCheckBenchmark,
    GenerateRandomJsonBenchmark
);

criterion_main!(benches);
