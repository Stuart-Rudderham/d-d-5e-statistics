use crate::dice::*;
use crate::distribution::*;
use crate::typedefs::*;

pub trait CheckAgainstDC
{
    fn Roll( &self, aDC : DC ) -> bool;

    fn NumPermutations(&self) -> Count;

    fn CountWaysToPass(&self, aDC : DC) -> Count;

    // TODO: Return struct with both WaysToPass and WaysToFail as an optimization
    fn CountWaysToFail(&self, aDC : DC) -> Count
    {
        self.NumPermutations() - self.CountWaysToPass(aDC)
    }

    fn ChanceToPass( &self, aDC : DC ) -> Probability { CountRatioToProbability(&self.CountWaysToPass(aDC), &self.NumPermutations()) }
    fn ChanceToFail( &self, aDC : DC ) -> Probability { CountRatioToProbability(&self.CountWaysToFail(aDC), &self.NumPermutations()) }

    // This API is a little strange for the binary pass/fail that the
    // check has but it's consistent with other classes which is useful.
    fn CalculateSuccessDistribution(&self, aDC : DC) -> Die
    {
        let weights = &[(&_1d0(), self.CountWaysToFail(aDC)),
                        (&_1d1(), self.CountWaysToPass(aDC))];

        Die::WeightedMerge(weights)
    }

    #[cfg(test)]
    fn GetRollRange( &self, aMinExtension : Value, aMaxExtension : Value ) -> std::ops::RangeInclusive<Value>;
}
