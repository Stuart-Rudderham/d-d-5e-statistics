use crate::dice::*;
use crate::distribution::*;
use crate::typedefs::*;
use crate::check_against_dc::*;
use crate::parsing::GenerateJSON;

use std::borrow::Borrow;

use serde::Deserialize;

use rand::Rng;

#[derive(Clone, Debug, PartialEq, Deserialize)]
#[serde(from = "DeserializeData_SavingThrow")]
pub struct SavingThrow
{
    die : Die,
}

impl CheckAgainstDC for SavingThrow
{
    fn Roll( &self, aDC : DC ) -> bool
    {
        self.RollValue() >= *aDC
    }

    fn NumPermutations(&self) -> Count
    {
        // Not 100% efficient since we're making an unnecessary
        // copy but that's what the interface forces us to do.
        self.die.NumPermutations().clone()
    }

    fn CountWaysToPass(&self, aDC : DC) -> Count
    {
        self.die.CountWaysOfGE_Value(*aDC)
    }

    #[cfg(test)]
    fn GetRollRange( &self, aMinExtension : Value, aMaxExtension : Value ) -> std::ops::RangeInclusive<Value>
    {
        let min = self.die.Min() + aMinExtension;
        let max = self.die.Max() + aMaxExtension;

        assert!(min <= max);

        min..=max
    }
}

impl SavingThrow
{
    pub fn RollValue( &self ) -> Value
    {
        self.die.Roll()
    }

    // Expose the underlying Die to the world.
    pub fn GetDie(&self) -> &Die
    {
        &self.die
    }

    pub fn  CountWaysToWinContest(&self, aSavingThrow : &SavingThrow) -> Count       { self.die.CountWaysOfGT_Die(&aSavingThrow.die) }
    pub fn     ChanceToWinContest(&self, aSavingThrow : &SavingThrow) -> Probability { self.die.ChanceOfGT_Die   (&aSavingThrow.die) }

    pub fn CountWaysToLoseContest(&self, aSavingThrow : &SavingThrow) -> Count       { self.die.CountWaysOfLT_Die(&aSavingThrow.die) }
    pub fn    ChanceToLoseContest(&self, aSavingThrow : &SavingThrow) -> Probability { self.die.ChanceOfLT_Die   (&aSavingThrow.die) }

    pub fn CountWaysToTieContest(&self, aSavingThrow : &SavingThrow) -> Count       { self.die.CountWaysOfEQ_Die(&aSavingThrow.die) }
    pub fn    ChanceToTieContest(&self, aSavingThrow : &SavingThrow) -> Probability { self.die.ChanceOfEQ_Die   (&aSavingThrow.die) }

    pub fn FromMod( aRollStyle : RollStyle, aModifierValue : SavingThrowModifier ) -> SavingThrow
    {
        SavingThrow::FromDie(aRollStyle, Die::from(*aModifierValue) )
    }

    pub fn FromDie<DieRef : Borrow<Die>>( aRollStyle : RollStyle, aModifierDie : DieRef ) -> SavingThrow
    {
        SavingThrow
        {
            die : _1d20_roll_style(aRollStyle) + aModifierDie.borrow()
        }
    }
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
struct DeserializeData_SavingThrow
{
    #[serde(default)]
    rollStyle : RollStyle,

    modifier  : Die,
}

impl From<DeserializeData_SavingThrow> for SavingThrow
{
    fn from(data : DeserializeData_SavingThrow) -> Self
    {
        SavingThrow::FromDie(data.rollStyle, data.modifier)
    }
}

// NOTE: This should match the structure of DeserializeData_SavingThrow.
pub fn GenerateRandomJSON_SavingThrow<R : Rng>( rng : &mut R ) -> String
{
    let rollStyle = if rng.gen()
    {
        Some( GenerateRandomRollStyleString(rng) )
    }
    else
    {
        None
    };

    GenerateJSON::BuildObject( vec!
    [
        GenerateJSON::BuildField( "modifier"  , Some(GenerateRandomModifierDie(rng)) ),
        GenerateJSON::BuildField( "rollStyle" , rollStyle                            ),
    ])
}

#[cfg(test)]
mod tests
{
    use super::*;
    use itertools::iproduct;
    use num_traits::identities::Zero;
    use strum::IntoEnumIterator;

    #[test]
    fn Deserialize()
    {
        // Authored input.
        let validStrings =
        [
            ( SavingThrow::FromDie( RollStyle::Normal      , _1d6() + 3              ), r#"{ "modifier": "1d6 + 3", "rollStyle": "Normal"       }"# ),
            ( SavingThrow::FromMod( RollStyle::Normal      , SavingThrowModifier(1)  ), r#"{ "modifier": "1"                                    }"# ),
            ( SavingThrow::FromDie( RollStyle::Advantage   , _1d4()                  ), r#"{ "modifier": "1d4"    , "rollStyle": "Advantage"    }"# ),
            ( SavingThrow::FromMod( RollStyle::Disadvantage, SavingThrowModifier(-1) ), r#"{ "modifier": "-1"     , "rollStyle": "Disadvantage" }"# ),
        ];

        let invalidStrings =
        [
            (r#"{                                                        }"#, "missing field `modifier`"),
            (r#"{ "modifier": "1d6 + 3", "RollStyle": "Normal"           }"#, "unknown field `RollStyle`, expected `rollStyle` or `modifier`"),
            (r#"{ "Modifier": "1d6 + 3"                                  }"#, "unknown field `Modifier`, expected `rollStyle` or `modifier`"),
            (r#"{ "modifier": "1d6 + 3", "modifier": "1d6 + 3"           }"#, "duplicate field `modifier`"),
            (r#"{ "modifier": "1d6 + 3", "rollStyle": "Normal", "foo": 1 }"#, "unknown field `foo`"),
        ];

        crate::unit_test_utils::TestDeserializeFromJSON( &validStrings, &invalidStrings );

        // Random input.
        let mut rng = rand::thread_rng();
        crate::unit_test_utils::TestGenerateRandomJSON::<_, SavingThrow>( ||{ GenerateRandomJSON_SavingThrow(&mut rng) } );
    }

    #[test]
    fn GetRollRange()
    {
        for aModifierDie in &[Die::from(0), Die::from(-5), Die::from(5), _1d4(), -_1d6(), 5 + _1d4(), 10 - _1d6()]
        {
            for aRollStyle in RollStyle::iter()
            {
                for &aMinRangeExtension in [-2, 0, 2].iter()
                {
                    for &aMaxRangeExtension in [-2, 0, 2].iter()
                    {
                        let anExpectedRollRange = (1 + aMinRangeExtension + aModifierDie.Min())..=(20 + aMaxRangeExtension + aModifierDie.Max());

                        let rollRange = SavingThrow::FromDie(aRollStyle, aModifierDie.clone()).GetRollRange(aMinRangeExtension, aMaxRangeExtension);

                        assert_eq!( rollRange, anExpectedRollRange );
                    }
                }
            }
        }
    }

    fn TestSavingThrow( aSavingThrowModifier : Die )
    {
        for aSavingThrowRollStyle in RollStyle::iter()
        {
            let aSavingThrow = SavingThrow::FromDie(aSavingThrowRollStyle, aSavingThrowModifier.clone());

            for aDC in aSavingThrow.GetRollRange(-2, 2).map(DC)
            {
                // Calculate the distribution manually.
                let calculatedSuccessProbabilityDistribution =
                {
                    let mut ret = CountDistribution::new();

                    for (sValue, sCount) in aSavingThrow.GetDie().GetDistribution().iter()
                    {
                        let passedSave = *sValue >= *aDC;

                        let distributionEntry = if passedSave { 1 } else { 0 };

                        let entry = ret.entry(distributionEntry).or_insert_with(Count::zero);
                        *entry += sCount;
                    }

                    BuildProbabilityDistribution( &ret )
                };

                // Compare it with the closed form version, they should be identical.
                let expectedSuccessCountDistribution = aSavingThrow.CalculateSuccessDistribution(aDC);

                let expectedSuccessProbabilityDistribution = expectedSuccessCountDistribution.BuildProbabilityDistribution();

                assert_eq!( calculatedSuccessProbabilityDistribution, expectedSuccessProbabilityDistribution );

                // Test it a second way by emperically rolling things.
                let RollFunction = ||
                {
                    if aSavingThrow.Roll(aDC) { 1 } else { 0 }
                };

                // Magic number. Needs to be low so that the tests don't take forever to run
                // but high enough that we don't get spurious failures due to random rolling.
                let numIterations = 50_000;

                crate::unit_test_utils::TestRolling(RollFunction, numIterations, expectedSuccessCountDistribution.Mean(), &expectedSuccessProbabilityDistribution);
            }
        }
    }

    // Split the saving throw modifiers across different tests to make it more parallel.
    #[test] fn TestSavingThrow_PositiveMod         () { TestSavingThrow( Die::from(  3) ); }
    #[test] fn TestSavingThrow_NegativeMod         () { TestSavingThrow( Die::from(- 5) ); }
    #[test] fn TestSavingThrow_ReallyPositiveMod   () { TestSavingThrow( Die::from( 30) ); }
    #[test] fn TestSavingThrow_ReallyNegativeMod   () { TestSavingThrow( Die::from(-29) ); }
    #[test] fn TestSavingThrow_PositiveDieMod      () { TestSavingThrow( _1d6()         ); }
    #[test] fn TestSavingThrow_PositiveDoubleDieMod() { TestSavingThrow( _2d4()         ); }
    #[test] fn TestSavingThrow_NegativeDieMod      () { TestSavingThrow( _1dNeg4()      ); }

    #[test]
    fn Hardcoded()
    {
        // Just some very basic tests with hardcoded numbers as a sanity check.
        let Test = |aRollStyle : RollStyle, anExpectedSuccessProbabilityDistribution : ProbabilityDistribution|
        {
            let dc = DC(14);

            let savingThrow = SavingThrow::FromMod( aRollStyle, SavingThrowModifier(5) );

            let calculatedSuccessProbabilityDistribution = savingThrow.CalculateSuccessDistribution(dc).BuildProbabilityDistribution();

            assert_eq!(calculatedSuccessProbabilityDistribution, anExpectedSuccessProbabilityDistribution);
        };

        Test( RollStyle::Normal      , flatmap!{0 => 160./400., 1 => 240./400.} );
        Test( RollStyle::Advantage   , flatmap!{0 =>  64./400., 1 => 336./400.} );
        Test( RollStyle::Disadvantage, flatmap!{0 => 256./400., 1 => 144./400.} );
    }

    #[test]
    fn Contests()
    {
        let savingThrowModifiers =
        [
            Die::from(-5),
            Die::from( 0),
            Die::from( 7),
            _1d4(),
            _1dNeg4(),
        ];

        let savingThrows : Vec<_> = iproduct!(RollStyle::iter(), savingThrowModifiers.iter()).map(|(rollStyle, modifier)| SavingThrow::FromDie(rollStyle, modifier) ).collect();

        for savingThrow1 in &savingThrows
        {
            assert_eq!( savingThrow1.ChanceToWinContest(savingThrow1), (1.0 - savingThrow1.ChanceToTieContest(savingThrow1)) / 2.0 );

            for savingThrow2 in &savingThrows
            {
                assert_eq!( savingThrow1.ChanceToWinContest (savingThrow2), savingThrow2.ChanceToLoseContest(savingThrow1) );
                assert_eq!( savingThrow1.ChanceToLoseContest(savingThrow2), savingThrow2.ChanceToWinContest (savingThrow1) );
                assert_eq!( savingThrow1.ChanceToTieContest (savingThrow2), savingThrow2.ChanceToTieContest (savingThrow1) );
            }
        }
    }
}
