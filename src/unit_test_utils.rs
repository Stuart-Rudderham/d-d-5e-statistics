use crate::distribution::*;
use crate::typedefs::*;
use serde::Deserialize;
use num_traits::identities::Zero;
use num_traits::cast::ToPrimitive;

// Check this at runtime so that we still get compile time checks for disabled code.
// Otherwise it might be possible to break code and not realize if we don't compile
// that cfg for a while.
#[cfg(test)]
pub fn ShouldRunSlowUnitTests() -> bool
{
    cfg!(feature = "run-slow-unit-tests")
}

#[cfg(test)]
pub fn TestRolling<F>(rollFunc : F, numIterations : u32, anExpectedMeanValue : MeanValue, anExpectedProbabilityDistribution : &ProbabilityDistribution ) where F: Fn() -> Value
{
    if !ShouldRunSlowUnitTests()
    {
        // Generally we test tens or hundreds of thousands of rolls which takes a while.
        return;
    }

    let (rolledProbabilityDistribution, rolledMeanValue) =
    {
        let mut rolledCountDistribution = CountDistribution::new();

        let mut total = num_bigint::BigInt::zero();

        for _ in 0..numIterations
        {
            let roll = rollFunc();

            total += roll;

            let entry = rolledCountDistribution.entry(roll).or_insert_with(Count::zero);
            *entry += 1u32;
        }

        let numInterationsAsCount = Count::from(numIterations);

        let rolledMeanValue = total.to_f64().unwrap() / numInterationsAsCount.to_f64().unwrap();

        let rolledProbabilityDistribution = BuildProbabilityDistribution( &rolledCountDistribution );

        (rolledProbabilityDistribution, rolledMeanValue)
    };

    // Make sure we didn't roll any impossible numbers.
    for value in rolledProbabilityDistribution.keys()
    {
        assert!
        (
            anExpectedProbabilityDistribution.contains_key(value),
            "Rolled impossible value {:?}\nRolled   {:?}\nExpected {:?}",
            value,
            rolledProbabilityDistribution,
            anExpectedProbabilityDistribution
        );
    }

    for (value, expectedProbability) in anExpectedProbabilityDistribution
    {
        // If we didn't roll a value then it happened with probability 0.
        let zero = &0.0;
        let rolledProbability = rolledProbabilityDistribution.get(value).unwrap_or(zero);

        let diff = ( rolledProbability - expectedProbability ).abs();

        assert!( diff < 0.01, "Rolled probability [{}] differs too much from expected probability [{}] for value [{}]", rolledProbability, expectedProbability, value );
    }

    // For small numbers we want to compare the absolute difference to make sure it is
    // sufficiently small. But this small tolerance doesn't work for big numbers, so
    // also use a relative difference.
    let absoluteDiff = ( rolledMeanValue - *anExpectedMeanValue ).abs();
    let absoluteDiffIsOk = absoluteDiff < 0.1;

    let ratioDiff = ( rolledMeanValue / *anExpectedMeanValue ).abs();
    let ratioDiffIsOk = ratioDiff >= 0.99 && ratioDiff <= 1.01; // +-1%

    assert!( absoluteDiffIsOk || ratioDiffIsOk, "Rolled mean [{}] differs too much from expected mean [{}]", rolledMeanValue, anExpectedMeanValue );
}

#[cfg(test)]
pub fn TestDeserializeFromJSON<T, S>( itemsToCheck : &[(T, S)], invalidStringsWithExpectedErrors : &[(&str, &str)] )
    where T: ::std::fmt::Debug + PartialEq + serde::de::DeserializeOwned + Clone
        , S: AsRef<str>
{
    assert!( !itemsToCheck.is_empty() );

    // Test deserializing as part of a larger struct.
    #[derive(Deserialize, PartialEq, Debug)]
    struct TestStruct<X>
    {
        item     : X,
        itemList : Vec<X>,
    }

    let mut itemList         = vec![];
    let mut itemListStr      = String::new();
    let mut isFirstIteration = true;

    for (expectedItem, string) in itemsToCheck
    {
        // Test deserializing individual object from standalone JSON.
        println!("Input string: {:?}", string.as_ref());

        let deserializedItem : T = serde_json::from_str(string.as_ref()).unwrap();
        assert_eq!( *expectedItem, deserializedItem );

        // Build up list.
        itemList.push(expectedItem.clone());

        if !isFirstIteration
        {
            itemListStr += ", ";
        }
        else
        {
            isFirstIteration = false;
        }

        itemListStr += string.as_ref();
    }

    let firstEntry = itemsToCheck.first().unwrap();

    let testStructText = format!(r#"{{ "item": {}, "itemList": [{}] }}"#, firstEntry.1.as_ref(), itemListStr);

    let deserializedTestStruct : TestStruct<T> = serde_json::from_str(&testStructText).unwrap();

    let expectedTestStruct = TestStruct::<T>
    {
        item : firstEntry.0.clone(), itemList
    };

    assert_eq!(expectedTestStruct, deserializedTestStruct);

    // Check to make sure these strings are *not* valid.
    for (string, expectedErrorMessageFragment) in invalidStringsWithExpectedErrors
    {
        let errorMessage = serde_json::from_str::<T>(string).unwrap_err().to_string();

        // Otherwise the .find() call below will always succeed.
        assert!( !expectedErrorMessageFragment.is_empty() );

        assert!( errorMessage.contains(expectedErrorMessageFragment), "Error Message: {}\nExpected: {}", errorMessage, expectedErrorMessageFragment );
    }
}

#[cfg(test)]
pub fn TestGenerateRandomJSON<F, T>( mut generationFunc : F )
    where F: FnMut() -> String
        , T: serde::de::DeserializeOwned
{
    // Hardcoded, arbitrary number of iterations.
    let numIterations = if ShouldRunSlowUnitTests()
    {
        10_000
    }
    else
    {
        10
    };

    for _ in 0..=numIterations
    {
        let input = generationFunc();

        let deserializedItem = serde_json::from_str::<T>(&input);

        // We don't have a way to randomly generate the same object of type T
        // to make sure that the deserialization worked the way we expected
        // so just check to make sure that it deserialized in some valid T.
        assert!( deserializedItem.is_ok(), "Input:\n\n{}\n\n", input);
    }
}

#[cfg(test)]
pub fn GetEncompassingRollRange<I>( vals: I ) -> std::ops::RangeInclusive<Value>
    where I: Iterator<Item = std::ops::RangeInclusive<Value>>
{
    vals.reduce(|a, b|
    {
        let newMin = *std::cmp::min( a.start(), b.start() );
        let newMax = *std::cmp::max( a.end  (), b.end  () );

        assert!(newMin <= newMax);

        newMin..=newMax
    })
    .expect("Must provide non-empty iterator!")
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn TestGetEncompassingRollRange()
    {
        let ranges = vec!
        [
            5..=10,
            6..=9,
            7..=15,
            3..=17,
            0..=1,
            20..=30,
        ];

        // The encompassing range should only ever grow as we add more ranges
        assert_eq!( GetEncompassingRollRange( ranges[0..=0].iter().cloned() ), 5..=10 );
        assert_eq!( GetEncompassingRollRange( ranges[0..=1].iter().cloned() ), 5..=10 );
        assert_eq!( GetEncompassingRollRange( ranges[0..=2].iter().cloned() ), 5..=15 );
        assert_eq!( GetEncompassingRollRange( ranges[0..=3].iter().cloned() ), 3..=17 );
        assert_eq!( GetEncompassingRollRange( ranges[0..=4].iter().cloned() ), 0..=17 );
        assert_eq!( GetEncompassingRollRange( ranges[0..=5].iter().cloned() ), 0..=30 );
    }
}
