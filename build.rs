fn main()
{
    // Only rebuild if the parser format has changed.
    // https://doc.rust-lang.org/cargo/reference/build-scripts.html#change-detection
    println!("cargo:rerun-if-changed=src/parsing.lalrpop");

    // Needed to generate all the parsing code.
    // http://lalrpop.github.io/lalrpop/tutorial/001_adding_lalrpop.html
    lalrpop::process_root().unwrap();
}
