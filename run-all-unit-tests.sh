#!/bin/bash

./run-unit-tests-helper.sh                                                                          \
bench               `# Run optimized code for faster test execution but slower compile times.`      \
                    `# https://doc.rust-lang.org/cargo/reference/profiles.html#default-profiles`    \
                                                                                                    \
run-slow-unit-tests `# Enable all the slow running tests which are normally disabled by default.`   \
                                                                                                    \
$*                  `# Pass through any user supplied arguments`
